define([
	'app',
	'marionette',
	'text!templates/socialmedia.html',
	'text!templates/leaderboard.html',
	"codebird",
	"isotope",
	"instafeed"
	], function (App, Marionette, SocialTemplate, LeaderboardTemplate, Isotope, Codebird) {
	
	var SocialView = Marionette.ItemView.extend({

		tagName: 'div',

		attributes: {
			id: "social"
		},

		events: {
			"click #sitefeedbtn": 	"sitefeed", 
			"click #fbfeedbtn": 	"facebookfeed", 
			"click #twitterfeedbtn":"twitterfeed", 
			"click #instafeedbtn": 	"instafeed"
		},

		onRender: function () {
			var that = this;

			this.siteRss();
			this.fbRss();
			this.codeBird();//Twitter RSS
			this.instafeedRss();
		},

		onShow: function() {
			var that = this;
			$(window).resize( function() {
				that.windowResize();
			})
		},

		onClose: function () {
			$(window).unbind();
		},

		template: function(data) {
			return _.template(SocialTemplate);
		},

		windowResize: function() {
			if ( $(window).width() >= 800) {
				App.Router.navigate('leaderboard', {trigger: true, replace: true})
			}
		},

		sitefeed: function() {
			$("#fbfeed").hide();
			$("#instafeed").hide();
        	$("#twitterfeed").hide();
        	$("#sitefeed").show();
		},

		siteRss: function() {
				var rssfeedsite = new google.feeds.Feed("http://rbwsmedia.redbull.com/rss/xf/en");
			    rssfeedsite.setNumEntries(8); // how many posts to pull
			    rssfeedsite.load(showSiteEntries);

			    function showSiteEntries(siteresult){
			        if(!siteresult.error)
			        {
			            var feeds = siteresult.feed.entries;
			            var rsssiteoutput = "";
			            for(var i=0; i<feeds.length; i++)
			            {               
			                rsssiteoutput += '<li>' + 
			                '<div class="img-wrap">' + '<span class="icon-group">' + '</span>' + '</div>' + 
			                '<div class="text">' + '<p>' + '<a href="' + feeds[i].link + '">' + feeds[i].title  + '</a>' +  '</p>' + '</div>'
			                '</li>';
			                // trouble getting the image url inserted                           
			            }
			            document.getElementById("sitefeed").innerHTML = rsssiteoutput;
			        }
			    }
		},

		facebookfeed: function() {
			$("#fbfeed").show();
	        $("#sitefeed").hide();
	        $("#twitterfeed").hide();
	        $("#instafeed").hide();
		},

		fbRss: function () {

			FB.api(
			    "/21470270055/posts?limit=7",
			    function (response) {
			      if (response && !response.error) {

			        var feeds = response.data;

			        var rssfboutput = "";

			        
			        for(var i=0; i<feeds.length; i++)

			    		{
			    			
			    			if ( feeds[i].type === "video" ) {
				        		rssfboutput += '<li>' + '<a href="' + feeds[i].link + '">' + feeds[i].message + '</a>' + '</li>';
				        	} if ( feeds[i].type === "photo" ) {
				    			var pic = feeds[i].picture;
				    			var res = pic.replace("_n.jpg", "_b.jpg");
				    			var res = pic.replace("_s.jpg", "_b.jpg");

				        		rssfboutput += '<li>' + '<a href="' + feeds[i].link + '">' + feeds[i].message + '</a>' + '<br />' + '<img src= " ' + res + ' " >' + '</li>';	
				        	}                               
			            }
			            document.getElementById("fbfeed").innerHTML = rssfboutput;
			      }
			    }
			);
		},

		twitterfeed: function() {
			$("#fbfeed").hide();
	        $("#sitefeed").hide();
	        $("#instafeed").hide();
	        $("#twitterfeed").show();
		},

		codeBird: function() {

			var cb = Codebird;

			cb.__call(
			    "oauth2_token",
			    {},
			    function (reply) {
			        var bearer_token = reply.access_token;
			        cb.setBearerToken(bearer_token);

			        cb.__call(
		                "search_tweets",
		                {"q": "#redbullxfighters"},
		                function (reply) {

		                    var twitterFeed = reply.statuses;
		                    var TwitterOutput = "";

		                    for(var i=0; i<7; i++)
		                    {
		                    	var tweetTime = new Date(twitterFeed[i].created_at).getTime();
			                    var dt = new Date().getTime();

			                    var diff = (dt - tweetTime) / 1000 / 60 /60;

			                    var ago = "hr ago";

			                    if (diff > 24){
			                    	var diff = diff / 24; 
			                    	ago = "days ago";
			                    	if (diff = 1) {
			                    		ago = "day ago";
			                    	}
			                    }
			                    	var diff = Math.floor(diff) + " " + ago;

		                    	TwitterOutput += '<li>' + '<div class="img-wrap">' + 
		                    	'<a href="' + "https://twitter.com/" + twitterFeed[i].user.screen_name  + '" target="_blank">' + '<img src="' + twitterFeed[i].user.profile_image_url + '">' + '</a>' + 
		                    	'</div>' + '<div class="text"> ' + '<p>' + 
		                    	'<a href="' + "https://twitter.com/" + twitterFeed[i].user.screen_name + '" target="_blank">' + '<span>' + '@' + twitterFeed[i].user.screen_name + '</span>' + '</a>' + '<span class="timeago">' + diff + '</span>' + '<br/>' +
		                    	twitterFeed[i].text + '</p>'  + '</div>' + '</li>';
		                    }

		                    

			        		document.getElementById("twitterfeed").innerHTML = TwitterOutput;
		                }
		            );
			    }
			);
		},

		instafeed: function() {
			$("#fbfeed").hide();
	        $("#sitefeed").hide();
	        $("#twitterfeed").hide();
	        $("#instafeed").show();
		},

		instafeedRss: function() {
			var feed = new Instafeed({
		        get: 'tagged',
		        tagName: 'xfighters',
		        clientId: '2d1f5416eebd424c8689e5d00c90a4a9',//Mike's API ID 
		        limit: 8,
			  	sortBy: 'most-liked',
			  template: '<a href="{{link}}" target="_blank"><img src="{{image}}" /><div class="likes"><p>&hearts; {{likes}}</p></div></a>'
			});
		    feed.run();
		},

		refresh: function() {
			
		}

	});

	return SocialView;
})