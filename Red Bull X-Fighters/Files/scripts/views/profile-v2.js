define([
	"app",
	'marionette',
	"facebook",
	"models/user",
	"models/play", 
	'text!templates/profile-v2.html',
	"codebird"
	], function (App, Marionette, FB, UserModel, PlayModel, ProfileTemplate, Codebird) {
	
	var ProfileView = Marionette.ItemView.extend({

		tagName: 'div',

		attributes: {
			id: "profile"
		},


		events: {
			"click #user-fb-login": 		"facebookLogin",
			"click #user-twitter-login": 	"twitterLogin",
			"click #share-facebook" : 		"shareFacebook",
			"click #play-again-profile": 			"playGame",
			"click #share-Twitter" : 		"shareTwitter"
		},

		template: function (data) {
			return _.template(ProfileTemplate, data,{"variable": "data"});
		},

		serializeData: function () {
			var entries = (this.getLevel().length > 1) ? this.getLevel() : "0" + this.getLevel();
			var name = (UserModel.get('XFightersID') !== null) ? UserModel.get('FirstName') + " " + UserModel.get('LastName') : this.getTitle(this.getLevel());
			var total = (UserModel.get('Total') === null) ?  0 : UserModel.get('Total');
		
			var data = {
				points: total,
				name: name,
				entries: entries,
				picture: UserModel.get('SelfieUrl'),
				badge: this.getBadge(),
				title: this.getTitle(this.getLevel()),
				nextTitle: this.getTitle(this.getLevel() + 1),
			}

			return data;
		},

		onShow: function () {
			var that = this;

			this.resetCss();
			setTimeout(function() {
				that.loadCover();
				that.loadSpeedo();
			}, 10)
			
			
			$("#nav-user-btn").addClass("active");
			this.initNeedle();
			this.animateNeedle();
			this.startOrigin();
		},

		resetCss: function () {
			$('.cover-image').css('height', '0px');
		},

		addScore: function (score) {
			var that = this;

			//if level up - level up
			if ( that.getNewPoints(score).overflow > 0 ) {
				that.startLevelUp(score);
			} else {
				//add points, save and animate
				UserModel.set('Total', that.getNewPoints(score).newPoints);
				that.animateNeedle();
			};

			that.addSharePoints(score);
		},

		getNewPoints: function (score) {
			var newPoints = UserModel.get('Total') + score;
			var total = this.nextLevelTotal(this.getLevel());
			var overflow = 0;
			var userTotal = UserModel.get('Total') + score;

			if ( newPoints > total ) {
				overflow = newPoints - total;
				newPoints = total;
			}

			return {newPoints: newPoints, overflow: overflow, total: userTotal};
		},


		startLevelUp: function (score) {
			var that = this;
			var details = this.getNewPoints(score);

			//animate to end
			UserModel.set("Total", this.nextLevelTotal() - 1);
			this.animateNeedle();

			//show level up screen
			var levelUpElement = $("<div id='pop-up-container'><div class='pop-up' id='level-up'><div class='title'><img src='img/leveled-up.jpg' /></div><div><img class='badge' /></div><button id='level-up-close'>Thanks!</button></div></div>");
			var el = levelUpElement.eq(0);

			el.find('.badge').bind('load', function() {
				setTimeout(function () {
					var bg = $("<div class='pop-up-bg'></div>");
					$('body').append(el);
					$('body').append(bg);
					$('#pop-up-container').css({"overflow": "auto"})
					that.levelUp(details);
				}, 3000)
			});

			el.find('.badge').attr("src", "img/badges/" + (this.getLevel() + 1) + "-large.png");
		},

		levelUp: function (details) {
			var that = this;

			//move needle to beginning once modal shows
			setTimeout(function(){
				UserModel.set("Total", that.nextLevelTotal() + 1);
				that.initNeedle();
			}, 900)
			
			//activate
			$('#pop-up-container').fadeIn(400, function(){
				$(this).addClass('active');
				$('.pop-up-bg').addClass('active');
				$("#game").css({"position":"fixed" , "min-height":"800px"});
			});

			//On Close
			$('#level-up-close').click(function(){

				//remove
				$("#pop-up-container").fadeOut(400, function(){
					$(this).remove();
					$('.pop-up-bg').remove();
				});
				$('.pop-up-bg').removeClass('active');

				$("#game").css({"position":"relative"});

				//set score & animate
				UserModel.set("Total", details.total);
				that.animateNeedle();

				//Update profile for new level
				var entries = (that.getLevel().length > 1) ? that.getLevel() : "0" + that.getLevel();
				$('#speedo-top img').attr("src", that.getBadge());
				$("#entries").text(entries);
				$('#current-title').html(that.getTitle(that.getLevel()))
				$('#next-title').html(that.getTitle(that.getLevel() + 1))
			})
			
		},

		initNeedle: function () {
			$('.needle').remove();

			var srotate = "rotate(-120deg)";
			var needle = $("<img />");
			needle.css({"transform" : srotate});

			needle.bind("load", function () {
				$(".speedo-top-wrap").append(needle);
			});
			needle.addClass("needle").attr("id", "needle").attr("src", "img/score/needle.png");
		},

		getLevel: function () {
			var points = UserModel.get('Total');
			if (points === null || points === undefined) return 0;
			return Math.floor(((-25 + Math.sqrt(5 * (125 + points))) / 50));
		},

		getBadge: function (size) {
			var badge = this.getLevel();

			if (size !== "large") {
				var badgeSrc = "img/badges/" + badge + ".png";
			} else {
				var badgeSrc = "img/badges/" + badge + "-large.png";
			}
			
			return badgeSrc;
		},

		prevLevelTotal: function () {
			var level = this.getLevel();
			return 500 * Math.pow((level - 1), 2) + (500 * (level - 1));
		},

		nextLevelTotal: function () {
			var level = this.getLevel();
			return 500 * Math.pow((level + 1), 2) + (500 * (level + 1));
		},

		currentLevelTotal: function () {
			var level = this.getLevel();
			return 500 * Math.pow((level), 2) + (500 * (level));
		},

		rotation: function (points) {
			var thisLevelPoints = points - this.currentLevelTotal();
			var rotation = thisLevelPoints /  (this.nextLevelTotal() - this.currentLevelTotal()) * 240;
			return rotation - 120;
		},


		animateNeedle: function () {
			var points = UserModel.get('Total');
			var srotate = "rotate(" + this.rotation(points) + "deg)";
			setTimeout(function() {
				$("#needle").css("transform", srotate);
				var score = PlayModel.get("score");
				var total = UserModel.get('Total');
				var countUp = setInterval(function(){
					var current = parseInt($('.speedo-score span').eq(0).text());
					if (current < total){
						$('.speedo-score span').text(current + 1);
					} else {
						clearInterval(countUp);
					}
				}, 2000 / score)
			}, 1400);
		},


		getTitle: function (level) {
			if (level > 9) return "<strong>FMX</strong> Warrior";
			switch (level) {
				case 0:
					return "<strong>FMX</strong> Spectator";
					break;
				case 1:
					return "<strong>FMX</strong> Follower";
					break;
				case 2:
					return "<strong>FMX</strong> Fan boy";
					break;
				case 3:
					return "<strong>FMX</strong> Buff";
					break;
				case 4:
					return "<strong>FMX</strong> Groupie";
					break;
				case 5:
					return "<strong>FMX</strong> Junkie";
					break;
				case 6:
					return "<strong>FMX</strong> Nerd";
					break;
				case 7:
					return "<strong>FMX</strong> Expert";
					break;
				case 8:
					return "<strong>FMX</strong> Fundi";
					break;
				case 9:
					return "<strong>FMX</strong> Warrior";
					break;
				default :
					return "<strong>FMX</strong> Spectator";
					break;
			}
		},

		shareFacebook: function () {
			var that = this;
			var body = "I’ve reached ’" + this.getTitle(that.getLevel()) + "’ in the Red Bull X-Fighters Trivia Game. I could be on my way to watch it live in Pretoria on the 23 of August!";
			body = body.replace("<strong>", "").replace("</strong>", "");

			if( navigator.userAgent.match('CriOS') ){
				that.addScore(250);
			    window.open("http://www.facebook.com/dialog/share?app_id=400635373410198&display=popup&redirect_uri=http://www.redbullxfighters.com/en_ZA/trivia&href=http://www.redbullxfighters.com/en_ZA/trivia&message=" + body, '', null);
			}
			else{
				FB.login(function(response) {
					if (response.authResponse) {
						
							FB.ui({
								method: 'share',
							 	message: body, 
							 	href: App.config.shareUrl 
							}, function(response) {
								if (!response.error) {
									that.addScore(250);
								}
							});
						
					}
				});
			}
		},

		shareTwitter: function () {
			var that = this;

			//if IE 9 - no twitter support
			if (window.browserSupport !== undefined && window.browserSupport === 9) {
				that.noTwitterSupport();
				return;
			} 
			
			
			//if we have your twitter details
			if(UserModel.get("TwitterID") && UserModel.get("TwitterToken") && UserModel.get("TwitterSecret")) {

				//create tweet
				var body = "I’ve reached ’" + this.getTitle(UserModel.get('level')) + "’ in the Red Bull X-Fighters Trivia Game. #xfighters http://win.gs/TGXYoV";
				body = body.replace("<strong>", "").replace("</strong>", "");
				
				//post tweet
				Codebird.__call( 
					"statuses_update",
					{
						"status": body
					},
					function (reply) {
					     if (reply.httpstatus === 200) {
					    	that.addScore(250);
					    } else if (reply.errors[0].code === 220){
					    	$('.twitterbtn').html('<img src="img/white-loader.gif" />');
							UserModel.twitter("login");
					    } else {
					    	console.error("Tweeting error", reply);
					    }
					}
				);
			} else {
				$('#share-Twitter').html('<img src="img/white-loader.gif" />')
				UserModel.twitter("login");
			}
		},

		addSharePoints: function (score) {
			var that = this;
			$.ajax({
				type: "POST",
				url: "api/result",
				data: { 
				    "XFightersID": UserModel.get("XFightersID"), 
				    "Result": score
				},
				success: function(data){
						that.animateNeedle();
				},
				error: function (data) {
				}
			});
		},

		loadCover: function () {
			var that = this;
			var image;

			if (UserModel.cover === null || UserModel.cover === undefined){
				image = window.location.href.replace(window.location.hash, "") + "img/cover.jpg";
			} else {
				image = UserModel.cover;
			}
			
		    var img = $('<img />');
			img.bind('load', function() {
			     $('.cover-image').css({'background-image': "url('" + image + "')"});
			     $('.cover-image').css('height', '');
			     setTimeout(function(){
			     	$('.profile-picture').css('opacity', '1');
			     }, 1000)

			     setTimeout(function(){
			     	$(window).resize()
			     }, 2000);
			});
			img.attr('src', image);
		},

		loadSpeedo: function () {
			var image = window.location.href.replace(window.location.hash, "") + "img/score/speedo.jpg";
		    img = $('<img />');
			img.bind('load', function() {
			     $('#speedo').css({'background-image': "url('" + image + "')"});
			     $('#speedo').css('opacity', '');
			});
			img.attr('src', image);
		},

		checkOrigin: function () {

			if (UserModel.modelStatus === "unset") {
				return true;
			} else {
				return false;
			}
		},

		startOrigin: function () {
			var that = this;

			if (this.checkOrigin() === true) {
				//show level up screen
				var originElement = $("<div id='pop-up-container'><div class='pop-up' id='origin'><div class='title'><img src='img/question-origin.jpg' /></div><div class='selectlist'><p>How did you find out about the <br/>Red Bull X-Fighters Trivia Game?</p><select><option value='picknpay'>Pick n Pay</option><option value='shell'>Shell</option><option value='bp'>BP</option><option value='spar'>Spar</option><option value='checkers'>Shoprite Checkers</option><option value='makro'>Makro</option><option value='facebook'>Facebook</option><option value='twitter'>Twitter</option><option value='wom'>Word of Mouth</option><option value='other'>Other</option></select></div><button id='enter-origin'>Enter</button></div></div>");
				var el = originElement.eq(0);

				$('body').append(el);
				var bg = $("<div class='pop-up-bg'></div>");
				$('body').append(bg);
				$('#pop-up-container').css({"overflow": "auto"})
				that.origin();

			}

		},

		playGame: function () {
			$('#play-again-profile').html('<img style="margin-top: 2px; max-width: 60px;" src="img/small-gears-gif.gif" />');
			Backbone.history.navigate('play', true);
		},

		origin: function() {
			var that = this;
			//activate
			$('#pop-up-container').fadeIn(400, function(){
				$(this).addClass('active');
				$('.pop-up-bg').addClass('active');
				$("#game").css({"position":"fixed" , "min-height":"800px"});
			});

			//On Close
			$('#enter-origin').click(function(){

				UserModel.modelStatus = "set";
				UserModel.set('Origin', $('.selectlist select').eq(0).val());
				UserModel.saveModel();

				//remove
				$("#pop-up-container").fadeOut(400, function(){
					$(this).remove();
					$('.pop-up-bg').remove();
				});
				$('.pop-up-bg').removeClass('active');
				$("#game").css({"position":"relative"});

			});
		},

		noTwitterSupport: function () {
			var oldBrowser = $("<div id='pop-up-container'><div class='pop-up' id='old-browser'><div class='title'><img src='img/sorry.jpg' /></div><h3>Your version of Internet Explorer doesn't support Twitter integration. Please update your browser.</h3><button id='update-now'>Update now</button><button id='cancel-pop-up'>Cancel</button></div></div>");
			var bg = $("<div class='pop-up-bg'></div>");
			var el = oldBrowser.eq(0);
			$('body').append(el);
			$('body').append(bg);
			$('#pop-up-container').css({"overflow": "auto"})
	
			//activate
			$('#pop-up-container').fadeIn(400, function(){
				$(this).addClass('active');
				$('.pop-up-bg').addClass('active');
				$("#game").css({"position":"fixed" , "min-height":"800px"});
			});			

			//On Close
			$('#cancel-pop-up').click(function(){
				$("#pop-up-container").fadeOut(400, function(){
					$(this).remove();
					$('.pop-up-bg').remove();
				});
				$('.pop-up-bg').removeClass('active');
			});

			$('#update-now').click(function(){
				window.location.href = App.config.updateBrowser;
			})

		}


	});

	return ProfileView;
})