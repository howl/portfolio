define([ 
	"app",
	"marionette",
	'text!templates/nav-bar.html',
	"app"
	], function (App, Marionette, NavigationTemplate, App) {

	var NavBar = Marionette.ItemView.extend({

		tagName: 'div',

		events: {
			"click .home": 					"home",
			"click .nav-login": 			"login",
			"click .hamburger": 			"hamburger",
			"click #nav-leaderboard-btn": 	"userLeaderboard",
			"click #nav-user-btn": 			"userProfile",
			"click #nav-howto": 			"howto",
			"click #nav-terms": 			"terms",
			"click #nav-social": 			"social" 	
		},

		template: function (data) {
			return _.template(NavigationTemplate, data, {variable: "data"});
		},

		initialize: function () {
			this.listenTo(this.model, "change:XFightersID", this.render);
		},

		onDomRefresh: function () {
			if ( this.model.get('XFightersID') === null ) {
				$('.nav-login').show();
				$('.nav-profile').hide();
			} else {
				$('.nav-login').hide();
				$('.nav-profile').show();
			}
		},

		serializeData: function () {
			var data = {
				FirstName: this.model.get('FirstName'),
				src: this.model.get('SelfieUrl')
			}
			
			return data;
		},

		userProfile: function () {
			$('.nav-btn').removeClass('active')
			this.hamburger();
			App.Router.navigate('profile', {trigger: true, replace: true})
		},

		home: function() {
			$('.nav-btn').removeClass('active')
			App.Router.navigate('index', {trigger: true, replace: true})
		},

		userLeaderboard: function () {
			$('.nav-btn').removeClass('active')
			this.hamburger();
			App.Router.navigate('leaderboard', {trigger: true, replace: true})
		},

		howto: function () {
			$('.nav-btn').removeClass('active')
			this.hamburger();
			App.Router.navigate('how-to-play', {trigger: true, replace: true})
		},

		terms: function () {
			$('.nav-btn').removeClass('active')
			this.hamburger();
			App.Router.navigate('terms-and-conditions', {trigger: true, replace: true})
		},

		social: function () {
			$('.nav-btn').removeClass('active')
			this.hamburger();
			App.Router.navigate('social-media', {trigger: true, replace: true})
		},

		login: function () {
			var that = this;

			// When clicking login - we check to see if the user has logged in with facebook or twitter before via local storage.
			// We then auto login via the same method each time to keep login methods on local computers consistent. If the user has never
			// logged in before it will default with a facebook login. Facebook is always the better choice.
			var method = localStorage.getItem("redbull-login-method");

			if (method === "twitter") {
				$('.nav-login .nav-name').html('<img src="img/white-loader.gif" />');
				that.model.twitter("login");

			//else its facebook or null - defaults to facebook 
			} else {

				// fix iOS Chrome
				if( navigator.userAgent.match('CriOS') )
				    window.open('https://www.facebook.com/dialog/oauth?client_id=400635373410198&redirect_uri='+ document.location.href +'&scope=email,public_profile,user_friends', '', null);
				else{
					FB.login(function(response) {
						if (response.authResponse) {
							that.model.getFacebookDetails();
						} else {
						}
					}, {scope: "user_friends, email"});
				}
			} 

		},

		hamburger: function () {
			$('.nav-mobile').toggleClass('show');
		}

	});

	return NavBar;
})