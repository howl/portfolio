define([
	"app",
	"marionette",
	"models/user",
	'text!templates/leaderboard-region.html', 
	], function (App, Marionette, UserModel, LeaderboardTemplate) {
	
	var LeaderboardView = Marionette.ItemView.extend({

		tagName: 'div',

		template: function(data) {
			return _.template(LeaderboardTemplate, data, {"variable": "data"});
		},

		initialize: function () {
		},

		onRender: function () {
			$("#preloader").show();
		},

		onShow: function () {  

			$('.podium').each(function(i) {
    			$(this).delay((i++) * 300).fadeTo(500, 1);
    		})

			$('.top10').delay(600).each(function(i) {
    			$(this).delay((i++) * 300).fadeTo(500, 1);
    		})

    		$(".picture").mouseenter(function(){
		        $(this).find(".hover").css("opacity", "0.4");
			});
			
			$(".picture").mouseleave(function(){
		        $(this).find(".hover").css("opacity", "0");
		    });

			// --- trim leader names if too long --- //
		    var $elem = $('.podium .tag .podium-tag-wrapper h3');
		    var $limit = 10;
		    $elem.each(function(i){
		    	var el = $elem.eq(i);
		    	var $str = el.text();
		    	var $strtemp = $str.substr(0,$limit);	// Get the visible part
    			$str = $strtemp + '<span class="hide">' + $str.substr($limit,$str.length) + '</span>' + '<span class="dots">' + "..." + '</span>';	// Recompose
    			el.html($str);
		    })

		    var $elem2 = $('.top10 .tag h3');
		    var $limit2 = 10;
		    $elem2.each(function(i){
		    	var el2 = $elem2.eq(i);
		    	var $str2 = el2.text();
		    	var $strtemp2 = $str2.substr(0,$limit2);	// Get the visible part
    			$str2 = $strtemp2 + '<span class="hide">' + $str2.substr($limit2,$str2.length) + '</span>' + '<span class="dots">' + "..." + '</span>';	// Recompose
    			el2.html($str2);
		    })

	    	var contenttitle = $(".content-title").height();
			var wrapdif = $(window).height() - contenttitle;

			$(".wrapper").css("min-height", wrapdif)

		    $( window ).bind("resize.leaderboard-region", function() {
				
				var contenttitle = $(".content-title").height();
				var wrapdif = $(window).height() - contenttitle;

				$(".wrapper").css("min-height", wrapdif)

			});

			setTimeout(function(){
				$(window).resize();
				$(window).unbind("resize.leaderboard-region");
			}, 2000)
		}

	})

	return LeaderboardView;
})
