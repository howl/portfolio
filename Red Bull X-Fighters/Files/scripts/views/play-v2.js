define([
	"app",
	'marionette',
	'text!templates/game-v2.html',
	], function (App, Marionette, GameTemplate) {
	
	var PlayView = Marionette.ItemView.extend({

		tagName: 'div',

		events: {
			"mouseup #answers li div": "answerQuestion"
		},

		attributes: {
			id: "play"
		},

		template: function (data) {
			return _.template(GameTemplate, data, {"variable": "data"});
		},

		initialize: function () {
			this.resetModel();
			this.endTime = 1000;

			this.listenTo(this.model, "change:seconds", this.changeTimer);
			this.listenTo(this.model, "change:outOfTime", this.outOfTime);
		},
		
		serializeData: function () {
			var data = Marionette.ItemView.prototype.serializeData.apply(this, arguments);
            data.question = this.collection.at(this.model.get('currentIndex'));
            data.question.set('results', this.model.get('results'));
            data.hasPicture = (data.question.get('picture') !== null) ? true : false;

            if ( data.question.get("picture") !== null ) {
            	data.template = $(GameTemplate).find('#game-picture-template').html();
            } else {
            	data.template = $(GameTemplate).find('#game-template').html();
            }

            return data;
		},
		
		onClose: function () {
			this.model.stopTimer();
			clearInterval(this.startGate);
			this.remove();
		},

		resetModel: function () {
			this.model.set('score', 0);
			this.model.set('currentIndex', 0);
			this.model.set('timer', 1000);
			this.model.set('seconds', 10);
			this.model.set('outOfTime', false);
			this.model.set('results', [null, null, null, null, null]);
		},

		onDomRefresh: function () {
			window.scrollTo(0, 0);
			this.delegateEvents();
			if ( this.model.get('currentIndex') !== 0 ) {
				$('.start-gate-wrapper').hide();
				this.fadeInQuestion();
			}
		},

		onShow: function () {
			if ( this.model.get('currentIndex') === 0 ) {
				this.startGate();
			};

			$('#play').click(function() {
				$(".nav-mobile").removeClass("show");
			});
			$(window).resize();
		},


		startGate: function () {
			var that = this;
			var num = $('.start-gate span');
			var i = 3;
			setTimeout(function(){

				//fade in start gate
				$('.start-gate').addClass("animate").css({"opacity": 1});

				//after 2.7 seconds - fade out start gate
				setTimeout(function () {
					$('.start-gate').css({"opacity": ""});
				}, 2000)
				setTimeout(function() {
					that.fadeInQuestion();
				}, 2700);

				//change number every second
				that.startGateInterval = setInterval(function () {
					if ( i !== 1 ) {
						i --;
						num.text(i);
					} else {
						clearInterval(that.startGateInterval);
					}
				}, 1000)

			}, 100)	
		},

		fadeInQuestion: function () {
			var that = this;
			setTimeout(function() {
				$(window).resize()
				that.initTimer();
				$('.question-answer').addClass('active');
				$('.start-gate').hide();
			}, 300)
			
		},

		initTimer: function () {
			var that = this;
			that.model.set('timer', 1000);
			that.model.set('seconds', 10);
			setTimeout(function(){
				that.model.countdown();
				$('.bar-timer div').animate({
					"width": "100%"
				}, 10000)
			}, 800)
		},

		changeTimer: function () {
			var seconds = this.model.get('seconds');
			$('.bar-timer span').html(seconds);
		},

		outOfTime: function () {
			var that = this;
			if (this.model.get('outOfTime') === true ) {
				this.undelegateEvents();
				this.setResultsCircles("incorrect");
				that.endTime += 1000;
				this.colourCorrect(that.getCorrect(), true, 0);
				this.endQuestion(0);
			}
			
		},

		answerQuestion: function (e) {
			if( !e ) e = window.event;

			//stop game 
			e.stopImmediatePropagation();
			this.model.stopTimer();
			this.undelegateEvents();

			//get clicked button
			var id = ($(e.target).hasClass('answer-option')) ? e.target.id : $(e.target).closest('.answer-option').attr('id');
			var selected = $('#' + id);
			var selectedText;

			try{
				selectedText = $(e.currentTarget).text();
			} catch(e) {
				if (e.srcElement)  selectedText = e.srcElement.innerText;
			 	else if (e.target) selectedText = e.target.innerText;
			}
			 

			//checkcheck answer (colour buttons) end with score
			if ( this.checkAnswer(selected, selectedText) ) {
				this.endQuestion(this.getScore());
			} else {
				this.endQuestion(0);
			};
		},

		checkAnswer: function (selected, selectedText) {
			if ( selectedText.toLowerCase().trim() === this.collection.at(this.model.get('currentIndex')).get('correct').toLowerCase().trim() ) {
				this.setResultsCircles("correct");
				this.colourCorrect(selected, false);
				return true;
			} else {
				this.setResultsCircles("incorrect");
				this.colourIncorrect(selected);
				this.endTime += 1000;
				this.colourCorrect(this.getCorrect(), true, 1000);
				return false;
			}
		},

		getCorrect: function () {
			var correct;
			var i = 0;
			var answers = $('li.answer-option div');
			for (i; i < answers.length; i++) {
				if ( $(answers[i]).text().toLowerCase().trim() === this.collection.at(this.model.get('currentIndex')).get('correct').toLowerCase().trim() ) {
					correct = $(answers[i]).closest('.answer-option')
				}
			}
			return correct;
		},

		colourCorrect: function (selected, animate, time) {
			if (animate === true){
				setTimeout(function() {
					try{
						selected.addClass('animate');
						selected.addClass('green');
					} catch(e) {
						
					}
					
				}, time)
				
			} else {
				selected.addClass('green');
			}
		},

		colourIncorrect: function (selected) {
			selected.addClass('red');
		},

		setResultsCircles: function (answer) {
			$('.timer-dots li').eq(this.model.get('currentIndex')).addClass(answer)
			var results = this.model.get('results');
			results[this.model.get('currentIndex')] = answer;
			this.model.set('results', results);
		},

		getScore: function () {
			var score;
			var timeRemaining = this.model.get('timer');
			var score = Math.ceil(100 - (timeRemaining / 100));
			return score;
		},

		endQuestion: function (score) {
			var that = this;

			this.model.set("score", this.model.get("score") + score);

			$('.quiz-answers').addClass('answered');

			$('.bar-timer div').stop();

			window.endRoundTimer = setTimeout(function(){
				if ( that.model.get('currentIndex') === 4 ) {
					$('#quiz').fadeOut(400, function(){
						that.endRound();
					})
				} else {
					that.model.set('outOfTime', false)
					that.model.set('currentIndex', that.model.get('currentIndex') + 1);
					$('.question-answer').fadeOut(400, function(){
						that.render();
					})
					
				}
			}, that.endTime)

		},

		endRound: function () {
			Backbone.history.navigate('results', true);
		}

	});

	return PlayView;
})