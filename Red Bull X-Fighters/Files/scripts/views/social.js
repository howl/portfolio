define([
	'app',
	'marionette',
	'models/user',
	'text!templates/social.html',
	"codebird",
	"isotope",
	"instafeed"
	], function (App, Marionette, UserModel, SocialTemplate, Codebird, Isotope) {
	
	var SocialView = Marionette.ItemView.extend({

		tagName: 'div',

		attributes: {
			id: "social"
		},

		events: {
			"click #allfeedbtn":  	"allfeed",
			"click #sitefeedbtn": 	"sitefeed", 
			"click #fbfeedbtn": 	"fbfeed", 
			"click #twitterfeedbtn":"twitterfeed", 
			"click #instafeedbtn": 	"instafeed",
			"click .facebookbtn": 	"shareFacebook",
			"click .twitterbtn": 	"shareTwitter"
		},

		onRender: function () {
			var that = this;

			this.siteRss();
			this.fbRss();
			this.instafeedRss();

			//if IE 9 - no twitter support
			if (window.browserSupport !== 9) {
				this.codeBird();
			} 
		},

		onShow: function() {
			this.initIsotope();
			$('#social').click(function() {
				$(".nav-mobile").removeClass("show");
			});
		},

		onClose: function () {
		},

		template: function(data) {
			return _.template(SocialTemplate);
		},

		initIsotope: function () {
			this.social = new Isotope('#latest-news', {
			  	itemSelector: '.social-item',
				sortBy: 'date',
				layoutMode: 'fitRows',
				getSortData: {
					'date': '[data-date]'
				},
				sortAscending: false
			});
		},

		allfeed: function() {
			this.social.arrange({
			    filter: '*'
			})

			$(".social-filter-icons .icons span").removeClass("active");
			$("#allfeedbtn").addClass("active");
		},

		sitefeed: function() {
			this.social.arrange({
			    filter: '.rss'
			})

			$(".social-filter-icons .icons span").removeClass("active");
			$("#sitefeedbtn").addClass("active");
		},

		siteRss: function() {
			var that = this;
			var rssfeedsite = new google.feeds.Feed("http://rbwsmedia.redbull.com/rss/xf/en");
		    rssfeedsite.setNumEntries(8); // how many posts to pull
		    rssfeedsite.load(showSiteEntries);

		    function showSiteEntries(siteresult){
		        if(!siteresult.error)
		        {
		            var feeds = siteresult.feed.entries;
		            var elements = [];
		                
		            for(var i=0; i<feeds.length; i++) { 
			            try{
			            	var content = String(feeds[i].content).replace("p&gt;", "").replace("&lt;", "").replace("</p>", "").replace("<p>", "").replace("&lt;/p&gt;", "").trim();  
			            	var elem = $("<div>");
			            	elem.addClass("social-item").addClass('rss');
			                var html = '<div id="rss-' + i + '""><a href="' + feeds[i].link + '"><h3>' + feeds[i].title + '</h3><p>' + that.formatText(content) + '</p></a></div>';
			                elem.html(html);
			                elem.attr('data-date', new Date(feeds[i].publishedDate).getTime());
			                elements.push(elem[0]);
			            } catch(e){}

		            }

		            that.social.insert( elements );

		            //fix layout glitches
		            setTimeout(function() {
		            	that.social.arrange();
		            	    $(window).resize()
		            }, 500)
		        }
		    }
		},

		fbRss: function () {
			var that = this;
			FB.api(
			    "/21470270055/posts?limit=7",
			    function (response) {
			      if (response && !response.error) {

			        var feeds = response.data;

			        var elements = [];

			        
			        for(var i=0; i<feeds.length; i++) {
			        	try{
			    			 if ( feeds[i].type === "photo" ) {
				    			var pic = feeds[i].picture;
				    			var res = pic.replace("_n.jpg", "_b.jpg");
				    			var res = pic.replace("_s.jpg", "_b.jpg");
				    			var html = '<div><a href="' + feeds[i].link + '">' + '<img src= " ' + res + ' " /><h3>Redbull SA<span>shared a photo</span><div class="icon-facebook social-icon"></div></h3><p>' + that.formatText(feeds[i].message) + '</p></a></div>';

				    			var elem = $("<div>");
				            	elem.addClass("social-item").addClass('fbrss').html(html);
				            	elem.attr('data-date', new Date(feeds[i].updated_time).getTime());
				                elements.push(elem[0]);
				        	} else if ( feeds[i].type === "link" ) {
				        		var pic = feeds[i].picture;
				        		pic = decodeURIComponent(pic.split("url=")[1]);
				    			var html = '<div><a href="' + feeds[i].link + '" target="_blank">' + '<img src= " ' + pic + ' " /><h3>Redbull SA<span>shared a link</span><div class="icon-facebook social-icon"></div></h3><p>' + that.formatText(feeds[i].message) + '</p></a></div>';

				    			var elem = $("<div>");
				            	elem.addClass("social-item").addClass('fbrss').html(html);
				            	elem.attr('data-date', new Date(feeds[i].updated_time).getTime());
				                elements.push(elem[0]);
				        	}  else if (feeds[i].type === "video"){
				        		var src = feeds[i].source;
				    			var html = '<div><video width="100%" controls preload="none">' + '<source src= " ' + src + '" type="video/mp4" ></video><h3>Redbull SA<span>shared a video</span><div class="icon-facebook social-icon"></div></h3><p>' + that.formatText(feeds[i].message) + '</p></div>';

				    			var elem = $("<div>");
				            	elem.addClass("social-item").addClass('fbrss').html(html);
				            	elem.attr('data-date', new Date(feeds[i].updated_time).getTime());
				                elements.push(elem[0]);
				        	}       
				        }catch(e) {}
		            }//for
	            	that.social.insert( elements );

	            	//fix layout glitches
		            setTimeout(function() {
		            	that.social.arrange();
		            	    $(window).resize()
		            }, 1000)

			      }//if

			    }//callback

			);//FB.api

		},

		fbfeed: function() {
			this.social.arrange({
			    filter: '.fbrss'
			})

			$(".social-filter-icons .icons span").removeClass("active");
			$("#fbfeedbtn").addClass("active");
		},


		codeBird: function() {
			var that = this; 
			var cb = Codebird;

			cb.__call(
			    "oauth2_token",
			    {},
			    function (reply) {
			        var bearer_token = reply.access_token;
			        cb.setBearerToken(bearer_token);

			        cb.__call(
		                "search_tweets",
		                {"q": "#redbullxfighters"},
		                function (reply) {
		                    var twitterFeed = reply.statuses;
		                    var elements = [];
		                    for(var i=0; i<7; i++)
		                    {
			                    try{
			                    	var tweetTime = new Date(twitterFeed[i].created_at).getTime();
				                    var dt = new Date().getTime();

				                    var diff = (dt - tweetTime) / 1000 / 60 /60;

				                    var ago = "hr ago";

				                    if (diff > 24){
				                    	var diff = diff / 24; 
				                    	ago = "days ago";
				                    	if (diff = 1) {
				                    		ago = "day ago";
				                    	}
				                    }
				                    
				                    var diff = Math.floor(diff) + " " + ago;

				                    var elem = $("<div>");

				                    var pic = "";

				                    //if twitter picture
				                    if (twitterFeed[i].entities.media) {
				                    	pic = "<img src='" + twitterFeed[i].entities.media[0].media_url + "' />";
				                    }

			                    	var html = '<div>' + pic + '<h3>@'+ twitterFeed[i].user.screen_name + "<div class='icon-twitter social-icon'></div></h3><p>" + that.formatText(twitterFeed[i].text) + "</p></div>";


					            	elem.addClass("social-item").addClass('twrss').html(html);
					            	elem.attr('data-date', tweetTime);
					                elements.push(elem[0]);
					            } catch (e) {}
		                    }

			        		that.social.insert( elements );

			        		//fix layout glitches
				            setTimeout(function() {
				            	that.social.arrange();
				            	    $(window).resize()
				            }, 500)
		                }
		            );
			    }
			);
		},

		twitterfeed: function() {
			var that = this;

			//if IE 9 - no twitter support
			if (window.browserSupport !== undefined && window.browserSupport === 9) {
				that.noTwitterSupport();
				return;
			} 

			this.social.arrange({
			    filter: '.twrss'
			})
			$(".social-filter-icons .icons span").removeClass("active");
			$("#twitterfeedbtn").addClass("active");
		},

		instafeedRss: function() {
			var that = this;
			var feed = new Instafeed({
		        get: 'tagged',
		        tagName: 'xfighters',
		        clientId: '2d1f5416eebd424c8689e5d00c90a4a9',//Mike's API ID
		        limit: 8,
			  	sortBy: 'most-liked',
			  	mock: true,
			  	template: '<a href="{{link}}" target="_blank"><img src="{{image}}" /><div class="likes"><p>&hearts; {{likes}}</p></div></a>',
			  	success: function(response) {
			  		var response = response.data;
			  		var elements = [];
			  		 for(var i=0; i<response.length; i++){
			  		 	try{
				  		 	if (response[i] !== null && response[i].caption !== null && response[i].caption.text !== null) {
				    			var pic = response[i].images.standard_resolution.url;
				    			
				    			//remove all other tags
				    			var tags = response[i].tags;
				    			for (var n=0; n < tags.length; n++) {
				    				if ( tags[n].toLowerCase() !== "xfighters" && tags[n].toLowerCase() !== "redbull" ) {
				    					if (response[i] !== null && response[i].caption !== null && response[i].caption.text !== null) {
				    						response[i].caption.text = response[i].caption.text.replace("#" + tags[n], "");
				    					}
				    				}
				    			}

				    			var html = '<div><img src= " ' + pic.replace("http", "https") + ' " /><h3>' + response[i].user.full_name + '<div class="icon-instagram social-icon"></div></h3><p>' + that.formatText(response[i].caption.text) + '</p></a></div>';

				    			var elem = $("<div>");
				            	elem.addClass("social-item").addClass('instarss').html(html);
				            	elem.attr('data-date', response[i].created_time);
				                elements.push(elem[0]);
				            }
				        }catch(e){}
		            }
		            that.social.insert( elements );
		            //fix layout glitches
		            setTimeout(function() {
		            	that.social.arrange();
		            	    $(window).resize()
		            }, 500)

			  	}
			});
		    feed.run();
		},

		instafeed: function() {
			this.social.arrange({
			    filter: '.instarss'
			})

			$(".social-filter-icons .icons span").removeClass("active");
			$("#instafeedbtn").addClass("active");
		},

		getTitle: function (level) {
			if (level > 9) return "FMX Spectator";
			switch (level) {
				case 0:
					return "<strong>FMX</strong> Spectator";
					break;
				case 1:
					return "<strong>FMX</strong> Follower";
					break;
				case 2:
					return "<strong>FMX</strong> Fan boy";
					break;
				case 3:
					return "<strong>FMX</strong> Buff";
					break;
				case 4:
					return "<strong>FMX</strong> Groupie";
					break;
				case 5:
					return "<strong>FMX</strong> Junkie";
					break;
				case 6:
					return "<strong>FMX</strong> Nerd";
					break;
				case 7:
					return "<strong>FMX</strong> Expert";
					break;
				case 8:
					return "<strong>FMX</strong> Fundi";
					break;
				case 9:
					return "<strong>FMX</strong> Warrior";
					break;
				default :
					return "<strong>FMX</strong> Spectator";
					break;
			}
		},

		shareFacebook: function () {

			var that = this;
			var body = "I’ve reached ’" + this.getTitle(UserModel.get('level')) + "’ in the Red Bull X-Fighters Trivia Game. I could be on my way to watch it live in Pretoria on the 23 of August!";
			body = body.replace("<strong>", "").replace("</strong>", "");

			if( navigator.userAgent.match('CriOS') ){
				that.addSharePoints();
				window.open("http://www.facebook.com/dialog/share?app_id=400635373410198&display=popup&redirect_uri=http://www.redbullxfighters.com/en_ZA/trivia&href=http://www.redbullxfighters.com/en_ZA/trivia&message=" + body, '', null);
			}else{
				FB.login(function(response) {
					if (response.authResponse) {
						
							FB.ui({
								method: 'share',
							 	message: body, 
							 	href: App.config.shareUrl 
							}, function(response) {
								if (!response.error) {
									that.addSharePoints();
								}
							});
						
					}
				});
			}
		},

		shareTwitter: function () {
			var that = this;
			
			//if IE 9 - no twitter support
			if (window.browserSupport !== undefined && window.browserSupport === 9) {
				that.noTwitterSupport();
				return;
			} 

			//if we have your twitter details
			if(UserModel.get("TwitterID") && UserModel.get("TwitterToken") && UserModel.get("TwitterSecret")) {

				//create tweet
				var body = "I’ve reached ’" + this.getTitle(UserModel.get('level')) + "’ in the Red Bull X-Fighters Trivia Game. #xfighters http://win.gs/TGXYoV";
				body = body.replace("<strong>", "").replace("</strong>", "");
				
				//post tweet
				Codebird.__call( 
					"statuses_update",
					{
						"status": body
					},
					function (reply) {
					    if (reply.httpstatus === 200) {
					    	that.addSharePoints();
					    } else if (reply.errors[0].code === 220){
					    	$('.twitterbtn').html('<img src="img/white-loader.gif" />');
							UserModel.twitter("login");
					    } else {
					    	console.error("Tweeting error", reply);
					    }
					}
				);
			} else {
				$('.twitterbtn').html('<img src="img/white-loader.gif" />');
				UserModel.twitter("login");
			}
		},

		formatText: function (string) {
			var array = string.split(" ");
			var len = array.length;
			var text = "";

			for ( var i = 0; i < len; i ++ ) {
				var word;

				//if link
				if ( array[i].indexOf("http://") > -1 ||  array[i].indexOf("https://") > -1) {
					var link = "<br/><a class='social-hyper-link' href='" + array[i] + "'>[Link]</a>"
					word = link;
				} else if ( array[i].indexOf("www.") > -1 ) {
					var link = "<br/><a class='social-hyper-link' href='http://" + array[i] + "'>[Link]</a>"
					word = link;
				}

				//if long word
				else if ( array[i].length > 25 ) {
					word = array[i].substring(0,25);
				} 

				//else just add
				else {
					word = array[i];
				}

				text += word + " ";
			}

			return text;

		},

		addSharePoints: function () {
			UserModel.set("Total", UserModel.get("Total") + 250);
			$.ajax({
				type: "POST",
				url: "api/result",
				data: { 
				    "XFightersID": UserModel.get("XFightersID"), 
				    "Result": "250"
				},
				success: function(data){
					
				},
				dataType: "json"
			});
		},

		noTwitterSupport: function () {
			var oldBrowser = $("<div id='pop-up-container'><div class='pop-up' id='old-browser'><div class='title'><img src='img/sorry.jpg' /></div><h3>Your version of Internet Explorer doesn't support Twitter integration. Please update your browser.</h3><button id='update-now'>Update now</button><button id='cancel-pop-up'>Cancel</button></div></div>");
			var bg = $("<div class='pop-up-bg'></div>");
			var el = oldBrowser.eq(0);
			$('body').append(el);
			$('body').append(bg);
			$('#pop-up-container').css({"overflow": "auto"})
	
			//activate
			$('#pop-up-container').fadeIn(400, function(){
				$(this).addClass('active');
				$('.pop-up-bg').addClass('active');
				$("#game").css({"position":"fixed"});
			});			

			//On Close
			$('#cancel-pop-up').click(function(){
				$("#pop-up-container").fadeOut(400, function(){
					$(this).remove();
					$('.pop-up-bg').remove();
				});
				$('.pop-up-bg').removeClass('active');
			});


			$('#update-now').click(function(){
				window.location.href = App.config.updateBrowser;
			})

		}


	});

	return SocialView;
})
