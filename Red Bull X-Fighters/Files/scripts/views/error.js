define([
	'marionette',
	'text!templates/error.html'
	], function (Marionette, ErrorTemplate) {
	
	var TermsView = Marionette.ItemView.extend({

		tagName: 'div',

		attributes: {
			id: "error"
		},

		template: function(data) {
			return _.template(ErrorTemplate, data, {"variable": "data"});
		},

		serializeData: function () {
			var data = {
				title: this.options.title,
				message: this.options.message
			}
			return data;
		},

		onShow: function () {
			$(window).resize()
		}

	});

	return TermsView;
})