define([
	"app",
	"marionette",
	'text!templates/leaderboard-v2.html', 
	"models/leaderboard",
	"models/user",
	"codebird",
	"views/leaderboard-region",
	"collections/friendsLeaderboard",
	"collections/globalLeaderboard",
	"instafeed"
	], function (App, Marionette, LeaderboardTemplate, LeaderboardModel, UserModel, Codebird, LeaderboardRegion, FriendsCollection, GlobalCollection) {
	
	var LeaderboardView = Marionette.Layout.extend({

		tagName: 'div',

		template: function(data) {
			return _.template(LeaderboardTemplate, data, {"variable": "data"});
		},

		attributes: {
			id: "leaderboard2"
		},

		regions: {
			leaderboard: '#leaderboard-container'
		},

		events: {
			"click #friendsbtn": 	"navigateToFriends",
			"click #globalbtn": 	"globalLeaderboard",
			"click #sitefeedbtn": 	"sitefeed", 
			"click #fbfeedbtn": 	"facebookfeed", 
			"click #twitterfeedbtn":"twitterfeed", 
			"click #instafeedbtn": 	"instafeed",
			"click #invitemates": 	"facebookShare",
			"click .facebookbtn": "facebookLogin"
		},


		initialize: function () {
		},

		onShow: function () {
			$(".leaders-national").addClass("active");
			$(".leaders-friends").removeClass("active");

			$('#leaderboard2').click(function() {
				$(".nav-mobile").removeClass("show");
			}); 

			//$(".preloader-test").show();
			$(".preloader-test").hide();

			this.globalLeaderboard();
		},

		inputfriends: function() {
		},

		friendsLeaderboard: function () {
			var that = this;

			$(".leaders-friends").addClass("active");
			$(".leaders-national").removeClass("active");

			FB.api("me/friends", function (response) {
				try {
					var array = [];
				    for (var i = 0; i < response.data.length; i++) {
				    	array.push(response.data[i].id)
				    }
				    array.push(UserModel.get('FacebookID'));
					 var friends = {
					 	"FighterFriends": array
					 };

					$.ajax({
						type: "POST",
						url: "api/leaderboard",
						data: friends,
						success: function(response) {
							FriendsCollection.reset(response, {parse: true});
							that.leaderboard.show(new LeaderboardRegion({ collection: FriendsCollection }));
						}
					})
				} catch(e) {
					App.Router.navigate('error', {trigger: true, replace: true})
				}
			    

			})

		},

		navigateToFriends: function (response) {
			var that = this;

			//check if logged into facebook
			if (UserModel.get('FacebookID') !== null) {

				//check if friends collection has already been fetched
				if ( FriendsCollection.length <= 1 ) {
					that.friendsLeaderboard();
				} else {

					//check if we have the correct permissions
					FB.api("me/permissions", function (response) {
						var permissions = response.data;
						var allowed = false;
						for (var i = 0; i < permissions.length; i ++) {
							if (permissions[i].permission === "user_friends" && permissions[i].status === "granted") {
								allowed = true;
							}
						}
						if (allowed) {
							that.leaderboard.show(new LeaderboardRegion({ collection: FriendsCollection }));
						} else {
							$('#leaderboard-container').html("<div class='white-top-wrap-leaders'><h1><img src='img/login-title.jpg'></h1><p>Please Login with Facebook to view your top friends!</p><button class='social-btn facebookbtn'><div class='icon-facebook share-icon'></div>Login</button></div>'");
						}
						
					})
				}

			} else {
				// $('#leaderboard-container').html($('#leaderboard-login').html());
				$('#leaderboard-container').html("<div class='white-top-wrap-leaders'><h1><img src='img/login-title.jpg'></h1><p>Please Login with Facebook to view your top friends!</p><button class='social-btn facebookbtn'><div class='icon-facebook share-icon'></div>Login</button></div>'");
			}
		},

		globalLeaderboard: function () {
			$(".leaders-national").addClass("active");
			$(".leaders-friends").removeClass("active");
			var that = this;

			if ( GlobalCollection.length == 0 ) {
				$('#leaderboard-container').html($('#preloader').html());
				GlobalCollection.fetch({
					success: function (response) {
						//show global leaderboard region
						that.leaderboard.show(new LeaderboardRegion({ collection: GlobalCollection }));
					},
					error: function (e) {
						console.error("Error getting global leaderboard: ", e);
						App.Router.navigate('error', {trigger: true, replace: true})
					}
				})
			} else {
				that.leaderboard.show(new LeaderboardRegion({ collection: GlobalCollection }));
			}
		},

		facebookLogin: function () {
			var that = this;
			FB.login(function(response) {
				if (response.authResponse) {
					UserModel.getFacebookDetails(function(){
						Backbone.history.navigate('profile', true);
					});
				} else {
				}
			}, {scope: "user_friends, email"});
		},

		facebookShare: function () {
			var that = this;

			FB.login(function(response) {
				if (response.authResponse) {
					FB.ui({
					  method: 'share',
					  href: App.config.shareUrl,
					}, function(response){

					});
				}
			}, {scope: "publish_actions, user_friends, email"});

		}

	});

	return LeaderboardView;

})
