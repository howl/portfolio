define([
	'marionette',
	'text!templates/howtoplay.html'
	], function (Marionette, HowtoplayTemplate) {
	
	var HowtoplayView = Marionette.ItemView.extend({

		tagName: 'div',

		attributes: {
			id: "howto",
		},

		events: {
			"click #howtoplay-play-btn": 	"playGame"  
		},

		template: function(data) {
			return _.template(HowtoplayTemplate);
		},

		playGame: function () {
			$('#howtoplay-play-btn').html('<img style="margin-top: 15px; max-width: 100px;" src="img/white-loader.gif" />');
			Backbone.history.navigate('index', true);
		},

		onShow: function () {
			$('#howto').click(function() {
				$(".nav-mobile").removeClass("show");
			});
			    $(window).resize();
		}

	});

	return HowtoplayView;
})