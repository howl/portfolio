define([
	"app",
	'marionette',
	'text!templates/terms.html'
	], function (App, Marionette, TermsTemplate) {
	
	var TermsView = Marionette.ItemView.extend({

		tagName: 'div',

		attributes: {
			id: "terms"
		},

		template: function(data) {
			return _.template(TermsTemplate);
		},

		onShow: function () {
			$('#terms').click(function() {
				$(".nav-mobile").removeClass("show");
			});
			    $(window).resize()
		}

	});

	return TermsView;
})