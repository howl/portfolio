define([ 
	"app",
	"marionette", 
	"facebook", 
	"text!templates/index.html", 
	"models/user"
	], function (App, Marionette, FB, IndexTemplate, UserModel) {

	var IndexView = Marionette.ItemView.extend({

		tagName: 'div',

		template: IndexTemplate,

		attributes: {
			id: "home"
		},

		events: {
			"click #facebook-login": 		"facebookLogin",
			"click #twitter-login": 		"twitterLogin",
			"click #play": 					"playGame",
			"click #how-to-play": 			"howToPlay"
		},

		onRender: function () {
			
			var that = this;
			

			this.initFacebook();
			UserModel.twitter("init");
			
		},

		onShow: function () {
			$('#home').click(function() {
				$(".nav-mobile").removeClass("show");
			});
			$(window).resize();
		},

		checkLogin: function () {
			if ( UserModel.get('XFightersID') !== null ) {
				this.showButtons("play");
			} else {
				this.showButtons("login");
			}
		},

		showButtons: function (buttons) {
			if (buttons === "login") $(this.el).find('#login-buttons').show();
			if (buttons === "play") $(this.el).find('#play-buttons').show();
		},

		initFacebook: function () {
			var that = this;
			FB.getLoginStatus ( function (response) {
				if ( response.status === 'connected' ) {
					UserModel.getFacebookDetails(function(){
						that.checkLogin();
					});
				}else{
					that.checkLogin();
				}
			});
		},

		facebookLogin: function () {
			var that = this;

			// fix iOS Chrome
			if( navigator.userAgent.match('CriOS') )
			    window.open('https://www.facebook.com/dialog/oauth?client_id=400635373410198&redirect_uri='+ document.location.href +'&scope=email,public_profile,user_friends', '', null);
			else{

				FB.login(function(response) {
					if (response.authResponse) {
						that.$('#login-buttons').hide();
						that.$('#play-buttons').show();
						UserModel.getFacebookDetails();
					} else {
					}
				}, {scope: "user_friends, email"});

			}
		},

		twitterLogin: function () {
			var that = this;
			
			//if IE 9 - no twitter support
			if (window.browserSupport !== undefined && window.browserSupport === 9) {
				that.noTwitterSupport();
				return;
			} 

			$('.twitterbtn').html('<img style="margin-top: 25px" src="img/white-loader.gif" />');
			UserModel.twitter("login");
		},

		howToPlay: function () {
			Backbone.history.navigate('how-to-play', true);
		},

		playGame: function () {
			$('#play .button').html('<img style="margin-top: 25px; max-width: 100px;" src="img/white-loader.gif" />');
			Backbone.history.navigate('play', true);
		},

		noTwitterSupport: function () {
			var oldBrowser = $("<div id='pop-up-container'><div class='pop-up' id='old-browser'><div class='title'><img src='img/sorry.jpg' /></div><h3>Your version of Internet Explorer doesn't support Twitter integration. Please update your browser.</h3><button id='update-now'>Update now</button><button id='cancel-pop-up'>Cancel</button></div><div class='pop-up-bg'></div></div>");
			var el = oldBrowser.eq(0);
			$('body').append(el);
	
			//activate
			$('#pop-up-container').fadeIn(400, function(){
				$(this).addClass('active');
				$("#game").css({"position":"fixed" , "min-height":"800px"});
			});

			//On Close
			$('#cancel-pop-up').click(function(){
				$("#pop-up-container").fadeOut(400, function(){
					$(this).remove();
				});
				$("#game").css({"position":"relative"});
			});


			$('#update-now').click(function(){
				window.location.href = App.config.updateBrowser;
			})

		}

	});

	return IndexView;
})
