define(["marionette", "models/user", "controller", "eventhandler" ], function (Marionette, UserModel, Controller, Vent){
	
	var AppRouter = Marionette.AppRouter.extend({
		
		appRoutes: {
			"" : 						"index",
			"index": 					"index",
			"leaderboard": 				"leaderboard",
			"profile": 					"profile",
			"play" : 					"play",
			"how-to-play" : 			"howToPlay",
			"terms-and-conditions" : 	"termsAndConditions",
			"results" : 				"results",
			"social-media": 			"socialMedia",
			"error": 					"error",
			"404-error": 				"notFound",
			"timeout" : 				"timeout",
			'*notFound': 				'notFound'
		},

		controller: Controller,

		initialize: function () {
			require([ "app", "views/navigation", "models/user" ], function (App, NavBar, UserModel) {
		        App.navigation.show(new NavBar({ model: UserModel }) )
		    });
		
			

			// var windowSize;
			// var contentSize;
			// function resize() {
			// 	windowSize = $(window).height();
			// 	contentSize = $('.content-wrapper').height();

			// 	if (windowSize > contentSize) {
			// 		$('#game > div').height('100%');
			// 	} else {
			// 		$('#game > div').height('auto');
			// 	}
			// }
			
			// setInterval(resize, 100);
		},

		browserCheck: function () {

          var ua = window.navigator.userAgent
          var msie = ua.indexOf ( "MSIE " )

            if ( msie > 0 )  {    
            	// If Internet Explorer, return version number
                return parseInt (ua.substring (msie+5, ua.indexOf (".", msie )))
            }
            else {                 
            	// If another browser, return 0
                return 0
            }
		}


	})

	return AppRouter;
})
