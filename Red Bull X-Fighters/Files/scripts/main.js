require.config({
	baseURL: 'scripts',
    paths: {
        "jquery":  "lib/jquery",
        "jqueryTransit":  "lib/jquery_transit",
        "underscore": "lib/underscore",
        "backbone": "lib/backbone",
        "marionette": 'lib/backbone.marionette',
        "templates": "../templates",
        "text": "lib/text",
        "facebook": '//connect.facebook.net/en_US/sdk',
        "codebird": 'lib/codebird',
        "errorlogger": 'lib/errorlogger',
        "instafeed": 'lib/instafeed',
        "isotope": 'lib/isotope',
        "modernizr": 'lib/modernizr',
        "jqueryIframe": 'lib/jquery-responsiveiframe'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        errorlogger: {
            deps: ['jquery']
        },
        marionette: {
            deps: ['jquery', 'underscore', 'backbone'],
            exports: 'Marionette'
        },
        facebook : {
            exports: 'FB'
        },
        jquery : {
            exports: '$'
        },
        jqueryIframe : {
            deps: ['jquery']
        },
        waitSeconds: 45
    }
});

require([ "app", "facebook", "jquery"], function (App, FB, $) {


    //Scroll to top on load
    scrollUp();

    //Facebook Settings
    FB.init({
        appId      : '400635373410198',
        //appId       : '424320184375050',
        version    : 'v2.0'
    });

    //Redirects
    allowAccess();
    oldBrowser();
    
    //Set url hosts
    setHosts();

    //initiate app
    App.start();

    //Start Audio
    initAudio();



   /*---------------------------------------------
        Initiate functions
   ----------------------------------------------*/

    //Set Hosts
    function setHosts() {

        App.config = {};

        var host = document.location.hostname.toLowerCase();
        var parts = window.location.href.split('/');

        if (host === 'localhost') {

            App.config.host = parts[0] + '//' + host + '/index.html';
            App.config.errorLoggerUrl = parts[0] + '//192.168.1.60/elmah/Error/LogJavascriptError';
            App.config.shareUrl = '';
            App.config.updateBrowser = parts[0] + '//' + host + '/browsers/';

        } else if (host === '192.168.1.60' ||
            host === 'devmachvm01') {

            App.config.host = parts[0] + '//' + host + '/dev-xfighters/';
            App.config.errorLoggerUrl = parts[0] + '//' + host + '/elmah/Error/LogJavascriptError';
            App.config.shareUrl = parts[0] + '//' + host + '/dev-xfighters/';
            App.config.updateBrowser = parts[0] + '//' + host + '/dev-xfighters/browsers/';

        } else if (host === '192.168.1.58' ||
            host === 'devstagingmachvm01' ||
            host === '41.222.48.242' ||
            host === 'staging.machineagency.co.za') {

            App.config.host = parts[0] + '//' + host + '/redbull/';
            App.config.errorLoggerUrl = parts[0] + '//' + host + '/elmah/Error/LogJavascriptError';
            App.config.shareUrl = parts[0] + '//' + host + '/redbull/';
            App.config.updateBrowser = parts[0] + '//' + host + '/redbull/browsers';

        } else if (host === 'redbullxfighters.machineagency.co.za') {

            App.config.host = parts[0] + '//' + host + "/";
            App.config.errorLoggerUrl = parts[0] + '//elmah.machineagency.co.za/Error/LogJavascriptError';
            App.config.shareUrl = "http://www.redbullxfighters.com/trivia";
            App.config.updateBrowser = parts[0] + '//' + host + '/browsers';

        }
    }



    function scrollUp () {
        window.addEventListener("load", function()
        {
            setTimeout( function(){ window.scrollTo(0, 1); }, 100 );
        });
   }   



   function allowAccess() {
        if (window.location.hash !== "#/terms-and-conditions" 
            && window.location.hash !== "#terms-and-conditions"
            && window.location.hash !== "#/how-to-play"
            && window.location.hash !== "#how-to-play" )
        {
            window.location.hash = "";
        }
   }



   function oldBrowser() {
        if (window.browserSupport === 8) {
            window.location.href = App.config.updateBrowser;
        }
   }



    /*AUDIO CONTROLS*/
    function initAudio () {

         $.fn.clickToggle = function(func1, func2) {
            var funcs = [func1, func2];
            this.data('toggleclicked', 0);
            this.click(function() {
                var data = $(this).data();
                var tc = data.toggleclicked;
                $.proxy(funcs[tc], this)();
                data.toggleclicked = (tc + 1) % 2;
            });
            return this;
        };


        var audioElement = document.createElement('audio');
        audioElement.setAttribute('src', 'audio/song.mp3');
        audioElement.setAttribute('autoplay', 'autoplay');
        audioElement.setAttribute('loop', 'loop');
        //audioElement.load()

        $.get();

        audioElement.addEventListener("load", function() {
            audioElement.play();
        }, true);

        
        $('.audio').clickToggle(function() {
            audioElement.pause();
            $(this).removeClass("playing").addClass("stopped");
            $(this).find("span").text("Play")
        }, function(){
            audioElement.play();
            $(this).removeClass("stopped").addClass("playing");
            $(this).find("span").text("Mute")
        });
    }


   

});

