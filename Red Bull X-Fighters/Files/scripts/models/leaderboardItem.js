define(["backbone"], function (Backbone) {
	var LeaderboardModel = Backbone.Model.extend({
		
		defaults: {
			FirstName: null,
			LastName: null,
			SelfieUrl: null,
			Points: null
		},

		initialize: function() {
		}

	});

	return LeaderboardModel;
})