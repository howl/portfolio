define(["backbone"], function (Backbone) {
	var ProfileModel = Backbone.Model.extend({
		
		defaults: {
			name: null,
			score: 5,
			status: 1
		},

		initialize: function() {
			
		}

	});

	return ProfileModel;
})