define(["backbone"], function (Backbone) {
	var NavModel = Backbone.Model.extend({
		
		defaults: {
			nav1: "timeline",
			nav2: "leaderboard"
		},

		initialize: function() {
			
		}

	});

	return NavModel;
})