define([ "app", "backbone", "facebook", "codebird" ], function (App, Backbone, FB, Codebird) {

	var User = Backbone.Model.extend({

		url: "api/member",

		idAttribute: "MemberID",

		defaults: {
		    "ApplicationID": "1BE343A4-EACF-47A1-84BB-DF1FAC310562",
		    "MemberID": null,
		    "FirstName": null,
		    "LastName": null,
		    "XFightersID": null,
		    "FacebookID": null,
		    "TwitterID": null,
		    "TwitterToken": null,
		    "TwitterSecret": null,
		    "SelfieUrl": "img/profile-default.jpg",
		    "Email" : null,
		    "Total": null,
		    "Origin": null
		},

		modelStatus: null,

		initialize: function () {

		},

		getFacebookDetails: function(callback) {
			var that = this;


			//get facebook details
			FB.api('/me', {fields: 'id, last_name , first_name, email, gender, middle_name, cover'}, function(response) {
				//Handle errors
				if (response.error) {
					//App.Router.navigate('error', {trigger: true, replace: true});
					return;
				};

			  	FB.api('/me/picture', {"width": "330", "height": "330"}, function(responseB) {

			  		//Handle errors
			  		if (response.error) {
			  			console.error(response);
						App.Router.navigate('error', {trigger: true, replace: true});
						return;
					};

			  		//add details to model
			  		if (typeof response.cover != "undefined") that.cover = response.cover.source;
			  		that.set('SelfieUrl', responseB.data.url);
			  		that.set('FacebookID', response.id);
				  	that.set('LastName', response.last_name);
				  	that.set('FirstName', response.first_name);
				  	that.set('Email', response.email);


			  		//save xfighters ID
				  	if (that.get("XFightersID") === null) {
				  		that.set("XFightersID", response.id)
				  	}

				  	//Get twitter details and save to model
				  	that.pullLocalTwitter();

			  		

				  	//save model
				  	that.getModelStatus(callback);
			  	})
			});
		},

		//function used in navigation login
		saveLoginMethod: function () {
			if (this.get('XFightersID') === this.get('FacebookID')) {
				localStorage.setItem("redbull-login-method", "facebook");
			} else if (this.get('XFightersID') === this.get('TwitterID')) {
				localStorage.setItem("redbull-login-method", "twitter");
			}
			
		},

		twitter: function (level) {
			var that = this;
			var cb = Codebird;
			
			if (level === "init") {
				var current_url = location.toString();

				//code uri is a check for something else
				if (current_url.indexOf("code") > -1) {
					return 0
				}
				var parameters  = {};
				var parameter;

				// Check if this is twitter redirect url
				try {
					var current_url = location.toString();
					var token1 = current_url.split("?")[1].split("&")[0].split("=");
					var token2 = current_url.split("?")[1].split("&")[1].split("=");
					window.history.pushState(null, "Redbull X Fighters", window.location.pathname);
				} catch (e) {}


				if ( token1 !=  null || token1 !=  undefined ) {
					$('#twitter').addClass('accepted');
					$('#twitter').unbind();
					var parameters  = { 
						"oauth_token" : token1[1],
						"oauth_verifier" : token2[1]
					};
				}
				// check if oauth_verifier is set
				if (typeof parameters.oauth_verifier !== "undefined") {
				    // assign stored request token parameters to codebird here
				    cb.setToken(parameters.oauth_token, parameters.oauth_verifier);

				    //get secrets and tokens
				    cb.__call(
				        "oauth_accessToken",
				        {
				            oauth_verifier: parameters.oauth_verifier
				        },
				        function (reply) {
				            cb.setToken(reply.oauth_token, reply.oauth_token_secret);

				            //save twitter details in user model
				            that.set("TwitterID", reply.user_id);
				            that.set("TwitterSecret", reply.oauth_token_secret);
				            that.set("TwitterToken", reply.oauth_token);

				            //save twitter locally to facebook id
				            that.saveTwitterLocally();
				            //save xfighters ID
						  	if (that.get("XFightersID") === null) {

					            //get user details
					            cb.__call(
					            	"users_show",
					            	{"user_id": reply.user_id},
					            	function (reply) {

					            		//checking if page was refreshed or tokens expired
					            		if (reply.httpstatus === 401) {
					            			try {
					            				console.error("Error getting twitter Auth. Refreshing page in attempt to auto resolve. Current url:" + window.location.href)
												window.location.href = window.location.href.split('?')[0];
					            			} catch(e) {}
					            		}
					            		//split name string
										var names = reply.name.split(" ");

					            		//add details to model
								  		that.set('SelfieUrl', reply.profile_image_url);
									  	that.set('LastName', names[names.length - 1]);
									  	that.set('FirstName', names[0]);

				            		 	that.set("XFightersID", that.get("TwitterID"));
									  	that.getModelStatus();

					            	}//callback
					            )//codbird

					        } else {
					        	that.getModelStatus();
					        }



				        }
				    );
				}


			} else if (level === "login") {

				// gets a request token
				cb.__call(
				    "oauth_requestToken",
				    {oauth_callback: 'oob'},
				    function (reply) {

				    	//request token and request token secret
				        cb.setToken(reply.oauth_token, reply.oauth_token_secret);

				        cb.__call(
				            "oauth_authorize",
				            {},
				            function (auth_url) {
				                // window.codebird_auth = window.open(auth_url, "", "width=750, height=550");

				                var oldBrowser = $("<div id='pop-up-container'><div class='pop-up' id='twitter-aouth'><div class='title'><img src='img/login.jpg' /></div><h3>Please enter your unique twitter pin?</h3><h3><input id='PINFIELD' value=''/><a id='twitterpin' href=' " + auth_url + "' target='_blank'>get pin</a></h3><button id='twitter-pin'>Okay</button><button id='cancel-pop-up'>Cancel</button></div><div class='pop-up-bg'></div></div>");
								var el = oldBrowser.eq(0);
								$('body').append(el);
						
								//activate
								$('#pop-up-container').fadeIn(400, function(){
									$(this).addClass('active');
									$("#game").css({"position":"fixed" , "min-height":"800px"});
								});

								//On Close
								$('#cancel-pop-up').click(function(){
									$("#pop-up-container").fadeOut(400, function(){
										$(this).remove();
									});
									$("#game").css({"position":"relative"});
								});


								$('#twitter-pin').click(function(){
									cb.__call(
									    "oauth_accessToken",
									    {oauth_verifier: document.getElementById("PINFIELD").value},
									    function (reply) {
									        // store the authenticated token, which may be different from the request token (!)
									        cb.setToken(reply.oauth_token, reply.oauth_token_secret);
									        //save twitter details in user model
								            that.set("TwitterID", reply.user_id);
								            that.set("TwitterSecret", reply.oauth_token_secret);
								            that.set("TwitterToken", reply.oauth_token);

								             //save twitter locally to facebook id
				            				that.saveTwitterLocally();

				            				//save xfighters ID
										  	if (that.get("XFightersID") === null) {

									            //get user details
									            cb.__call(
									            	"users_show",
									            	{"user_id": reply.user_id},
									            	function (reply) {

									            		//checking if page was refreshed or tokens expired
									            		if (reply.httpstatus === 401) {
									            			try {
									            				console.error("Error getting twitter Auth. Refreshing page in attempt to auto resolve. Current url:" + window.location.href)
																window.location.href = window.location.href.split('?')[0];
									            			} catch(e) {}
									            		}
									            		//split name string
									            		try {
									            			var names = reply.name.split(" ");
									            			var image = reply.profile_image_url.replace('_normal','');
									            			//add details to model
													  		that.set('SelfieUrl', image);
														  	that.set('LastName', names[names.length - 1]);
														  	that.set('FirstName', names[0]);

									            		 	that.set("XFightersID", that.get("TwitterID"));
														  	that.getModelStatus(function(){
														  		Backbone.history.navigate('profile', {trigger: true, replace: true})
														  	});
									            		} catch (e) {
									            			that.set('SelfieUrl', reply.profile_image_url);
														  	that.set('FirstName', names);

									            		 	that.set("XFightersID", that.get("TwitterID"));
														  	that.getModelStatus(function(){
														  		Backbone.history.navigate('profile', {trigger: true, replace: true})
														  	});
									            		}

									            	}//callback
									            )//codbird

									        } else {
									        	that.getModelStatus(function(){
											  		Backbone.history.navigate('profile', {trigger: true, replace: true})
											  	});
									        }

									    }
									);
									$("#pop-up-container").fadeOut(400, function(){
										$(this).remove();
									});
									$("#game").css({"position":"relative"});
								})
				                
				            }
				        );

				    }
				);

			}

		},

		saveTwitterLocally: function () {
			if ( this.get("FacebookID") === this.get("XFightersID")) {
				localStorage.setItem(this.get("FacebookID") + " TwitterID", this.get("TwitterID"));
				localStorage.setItem(this.get("FacebookID") + " TwitterToken", this.get("TwitterToken"));
				localStorage.setItem(this.get("FacebookID") + " TwitterSecret", this.get("TwitterSecret"));
			}
		},

		pullLocalTwitter: function() {
			//if twitter has been saved locally
			if ( localStorage.getItem(this.get("FacebookID") + " TwitterID") ) {

				//get twitter details
				var twID = localStorage.getItem(this.get("FacebookID") + " TwitterID"), 
					twToken = localStorage.getItem(this.get("FacebookID") + " TwitterToken"), 
					twSecret = localStorage.getItem(this.get("FacebookID") + " TwitterSecret");

				//save to model
				this.set("TwitterID", twID);
				this.set("TwitterToken", twToken);
				this.set("TwitterSecret", twSecret);
			}
		},

		getModelStatus: function (callback) {
			var that = this; 

			if (that.modelStatus === null) {
				var url = window.location.href.replace(window.location.hash, "").replace("#", "") + "api/member/" + this.get('XFightersID');
				$.ajax({
					type: 'GET',
					url: url,
					async: false,
					statusCode: {
					    200: function() {
					    	that.modelStatus = "set";
					    	that.unset("Origin");
					    	that.unset("MemberID");
					    	that.saveModel(callback);
					    },
					    204: function() {
					    	that.modelStatus = "unset";
					    	that.saveModel(callback);
					    }
					}
				})
			} else {
				that.saveModel(callback);
			}

			
		},

		saveModel: function (callback) {
			var that = this;

			this.unset("Total");
			
			this.save(null, {
				success: function (data) {

					that.saveLoginMethod();

					//callback
				  	if ( callback !== null && callback !== undefined ) {
				  		callback.call(this);
				  	}
				  	
				},
				error: function (data) {
					console.error(data)

					//callback
				  	if ( callback !== null && callback !== undefined ) {
				  		callback.call(this);
				  	}
				}
			})
		},

		addRoundPoints: function (toAdd) {
			var that = this;
			$.ajax({
				type: "POST",
				url: "api/result",
				data: { 
		            "XFightersID": that.get("XFightersID"), 
		            "Result": toAdd 
		        },
				success: function(){
				},
				error: function (data) {
					console.error(data)
				}
			}); 
		},


	});

	return new User;
})