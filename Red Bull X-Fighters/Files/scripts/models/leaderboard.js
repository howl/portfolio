define(["backbone", "facebook"], function (Backbone, FB) {
	var LeaderboardModel = Backbone.Model.extend({
		
		defaults: {
			first: 		null,
			second: 	null,
			third:  	null,
			forth: 		null,
			fifth: 		null,
			sixth:  	null,
			seventh:  	null,
			eighth: 	null,
			ninth: 		null,
			tenth: 		null    
		},

		initialize: function() {
		},

		sortFriends: function () {

		}

	});

	return LeaderboardModel;
})