define(["backbone"], function (Backbone) {
	var HowtoplayModel = Backbone.Model.extend({
		
		defaults: {
			step1: null,
			step2: null,
			step3: null
		},

		initialize: function() {
		}

	});

	return HowtoplayModel;
})