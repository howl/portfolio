define([
	"app",
	"backbone"], function (App, Backbone) {

		
	var PlayModel = Backbone.Model.extend({
		
		defaults: {
			score: 0,
			currentIndex: 0,
			timer: 1000,
			seconds: 10,
			outOfTime: false
		},

		interval: null,

		initialize: function () {
		},

		countdown: function () {
			var that = this;

			var start = new Date().getTime(),
    		elapsed = '0.0';


			function count() {
				window.playIntervalCheck = true;

				var time = new Date().getTime() - start;
   				elapsed = Math.round(time / 10) / 100;
   				if(Math.round(elapsed) == elapsed) { elapsed += '.0'; }

				that.set('timer', time );
				
				if (time % 1000 > 990 || time % 1000 < 10) that.set('seconds', Math.ceil(10 - elapsed));
				if ( time > 10000 && that.attributes.outOfTime !== true) {
					that.set("outOfTime", true);
					that.stopTimer();
				}
			}
			count();
			window.playInterval = setInterval(count, 10);
		},

		stopTimer: function () {
			var that = this;
			var checking = 0;

			//if timer hasnt initiated yet but told to stop, wait for it to start then stop it.
			window.checkingTimer = setInterval(function(){
				if (window.playIntervalCheck === true) {
					clearInterval(window.playInterval);
					window.playIntervalCheck = false;
					var offset = $('.svg-timer svg').css("stroke-dashoffset");
					$('.svg-timer svg').css("stroke-dashoffset", offset);
					clearInterval(window.checkingTimer);
				} else if (checking > 10) {
					clearInterval(window.checkingTimer);
				} else {
					clearInterval(window.checkingTimer);
				}
				checking ++;
			}, 100)
			
		}

	});

	return new PlayModel;
})
