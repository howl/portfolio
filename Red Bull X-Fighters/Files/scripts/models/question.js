define(["backbone"], function (Backbone) {

	var QuestionModel = Backbone.Model.extend({
		
		defaults: {
			question: null,
			correct: null,
			first: null,
			second: null,
			third: null,
			picture: null
		}
	})

	return QuestionModel;
})