define(["app"], function (App) {
    var controller = {
        index: function () {
            require([ "app", "views/index", "models/user" ], function (App, IndexView, UserModel) {
                App.page.show(new IndexView({ model: UserModel }))
            })
        },

        leaderboard: function () {
            require([ "app", "views/leaderboard-v2", "models/leaderboard" ], function (App, LeaderboardView, LeaderboardModel) {
                var leaderboardModel = new LeaderboardModel();
                App.page.show(new LeaderboardView({model: leaderboardModel}))
            });
        },

        profile: function () {
            require([ "app", "views/profile-v2", "models/profile" ], function (App, ProfileView, ProfileModel) {
                var profileModel = new ProfileModel();
                App.page.show(new ProfileView({model: profileModel}))
            });
        },

        play: function () {
            require([ "app","views/play-v2", "models/play", "collections/questions" ], function (App, PlayView, PlayModel, Questions) {
                var questions = new Questions();
                questions.fetch({
                    success: function(response) {

                        //preload images
                        var models = response.models;
                        var url = window.location.href.replace(window.location.hash, "") + "Images/";

                        for (var i = 0; i < models.length; i ++ ) {
                            if ( models[i].get("picture") !== null ) {
                                jQuery.get(url + models[i].get("picture"));
                            }
                        }

                        var timeout = setTimeout(function(){
                            App.Router.navigate('timeout', {trigger: true, replace: true})
                        }, 45000)

                        //once images have loaded
                        $(document).one("ajaxStop", function() {
                            clearTimeout(timeout);
                            App.page.show(new PlayView({model: PlayModel, collection: questions}) )
                        });
                        
                    },
                    error: function() {
                        App.Router.navigate('error', {trigger: true, replace: true})
                    }
                });                
            });
        },

        results: function () {
            require([ "app", "views/results-v2" ], function (App, ResultsView) {
                App.page.show(new ResultsView())
            });
        },

        howToPlay: function () {
            require([ "app", "views/howtoplay" ], function (App, HowtoplayView) {
                App.page.show(new HowtoplayView())
            });
        },

        socialMedia: function () {
            require([ "app", "views/social" ], function (App, SocialView) {
                App.page.show(new SocialView())
            });
        },

        termsAndConditions: function () {
            require([ "app", "views/terms" ], function (App, TermsView) {
                App.page.show(new TermsView())
            });
        },

        error: function () {
            require([ "app", "views/error" ], function (App, ErrorView) {
                App.page.show(new ErrorView({
                    title: "Error",
                    message: "We had to bail out of that one. Please reload."
                }));
            });
        },

        timeout: function () {
            require([ "app", "views/error" ], function (App, ErrorView) {
                App.page.show(new ErrorView({
                    title: "Timeout!",
                    message: "We had to bail out of that one. Please reload."
                }));
            });
        },

        notFound: function () {
            require([ "app", "views/error" ], function (App, ErrorView) {
                App.page.show(new ErrorView({
                    title: "404 Error",
                    message: "The page you're looking for has gone riding. Please reload to see if it's landed yet."
                }));
            });
        }

        // browserSupport: function () {

        // }
    }

    return controller;
});