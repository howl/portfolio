define(["backbone", "models/leaderboardItem"], function (Backbone, LeaderboardItem) {

	var LeaderboardCollection = Backbone.Collection.extend({

		model: LeaderboardItem,

		url: "api/leaderboard",

		initialize: function () {
			
		},

		parse: function (response) {
			response.sort(function (a, b) {
				return b.Points - a.Points;
			});

			for (var i = 0; i < response.length; i ++ ) {
				response[i].badge = this.getBadge(response[i].Points);
				response[i].rank = i + 1;
			}

			return response;
		},

		getLevel: function (points) {
			return Math.floor(((-25 + Math.sqrt(5 * (125 + points))) / 50));
		},

		getBadge: function (points) {
			var badge = this.getLevel(points);
			if (badge > 9) badge = 9;
			var badgeSrc = "img/badges/" + badge + ".png";
			return badgeSrc;
		}
	})

	return new LeaderboardCollection;
})