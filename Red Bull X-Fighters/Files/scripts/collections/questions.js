define(["backbone", "models/question"], function (Backbone, QuestionModel) {

	var PlayCollection = Backbone.Collection.extend({

		model: QuestionModel,

		url: "api/question",

		initialize: function () {

		},

		parse: function (response) {
			var i = 0,
				l = response.length;
			for (i; i < l; i ++) {
				response[i].id = i;
				response[i].array = this.shuffle([response[i].correct, response[i].first, response[i].second, response[i].third]);
				for (var n = 0; n < response[i].array.length; n ++) {
					try{
						response[i].array[n].trim();
					} catch (e) {}
					
				}
			}
			return response;
		},

		shuffle: function (array) {
			var currentIndex = array.length,
				temporaryValue,
				randomIndex;

			while (0 !== currentIndex) {
				randomIndex = Math.floor(Math.random() * currentIndex);
				currentIndex -= 1;
				temporaryValue = array[currentIndex];
				array[currentIndex] = array[randomIndex];
				array[randomIndex] = temporaryValue;
			}

			return array;
		}

	})

	return PlayCollection;
})