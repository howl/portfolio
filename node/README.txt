This was for an internal project to help manage timesheets at our agency
and overcome the struggles with getting staff to use timesheets. It was
designed to remove some of the hasstles with our timesheet system.

The api is only open to the agency so only the scripts are viewable - not the
live project.


This was a fairly small project with no budget that rides off the current systems
API. Using packet sniffers we recreated the calls needed to manage it.

This project uses ExpressJS, MongoDB and Require for http calls.
