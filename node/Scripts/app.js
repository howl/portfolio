/*
	Publicis Machine
	
	-- Footprint--
	Main server script controller.

	Go live date: 2015
	Status: unfinished

	
	This is a fairly small app, so i made the main file include the paths and respectiv function calls
	
	We're using Q for promises.
	To all asynchronous functions we pass  the USER object. This will hold the data needed for that specific route and the return data
*/



/*
	Get dependencies
*/
var express = require('express');
var app = express();
var Q = require('q');
var bodyParser = require('body-parser');
var chaseProxy = require('./chaseProxy.js');
var database = require('./database.js');
var clientResponser = require('./clientResponser.js');
var functions = require('./functions.js');




// Prepare body parser to read JSON
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));




/* 
	POST - LOGIN
	{
		pw: password, 
		em: username
	}

	Logs into the CHASE system and saves the login info to cancel the need for login on the frontend.
	There is no sensitive information. No need to encrypt or implement high security as this is an internal project.

	returns userID. Must be stored locally
	
*/
app.use('/login', function(req, res) {
	var user = {req: req, res: res};
	user.username = user.req.body.em;
	user.password = user.req.body.pw;

	var promise = chaseProxy.login(user)
		.then(function (data){
			console.log("Inserting\r\n")
			return database.insertDetails(data);
		})
		.then(function (data){
			console.log("checking\r\n")
			return database.checkUserType(data);
		})
		.then(function (data){
			console.log("responding\r\n")
			return clientResponser.login(data)
		})
});






/* 
	GET - GETJOBTYPES
	{
		id: user-ID
	}

	Get job types. This is for the login process when declaring the users role.
		ie: Developer, Copywriter..

	It does require the users ID and potentially logging in if CHASE cookies expire. It will automatically login.
*/
app.use('/getjobtypes', function(req, res) {
	var user = {req: req, res: res};

	user.id = req.body.id;

	if (user.id === undefined || user.id.length !== 24) {
		clientResponser.relogin(user);
		return;
	}

	var promise = (function getJobTypes () {
		return database.getUserCookies(user)
		.then(function (data){
			return chaseProxy.getJobTypes(data)
		})
			.fail(function (data) {
				if (data.rejected === 302) {
					var relogin = database.fetchAuth(data)
						.then(function (data) {
							return chaseProxy.login(data)
						})
						.then(function (data){
		            		return database.insertDetails(data);
						})
						.then(function (data){
							return promise.call();
						}).end();
				} else if (data.rejected === 408) {
					var timeout = clientResponder.timeout(data).end();
					return;
				}
			})
		.then(function (data){
			return clientResponser.returnData(data)
		})
	});

	promise.call();
});





/*
	POST - SETJOBTYPE
	{
		id: userID,
		userTypeId: job-type
	}

	Set the users job type once globally for the user.
	This job type will be used when ever the user creates a new job
*/
app.use('/setjobtype', function(req, res) {
	var user = {req: req, res: res};

	user.id = req.body.id;
	user.userTypeId = req.body.userTypeId;

	if (user.id === undefined || user.id.length !== 24) {
		clientResponser.relogin(user);
		return;
	}

	var promise = (function setJobType () {
		return database.setJobType(user)
		.then(function (data){
			return clientResponser.success(data)
		})
	});

	promise.call();
});






/* 
	GET - timesheets 
	{
		id: userID
	} 

	Returns an array of all the users time sheets from chase for the day.
	First it checks if there are any outstanding timesheets that need to be filled in

*/
app.use('/timesheets', function(req, res) {
	var user = {req: req, res: res};
	user.id = req.body.id;

	if (user.id === undefined || user.id.length !== 24) {
		clientResponser.relogin(user);
		return;
	}

	var promise = (function getTimesheets () {
			return database.getUserCookies(user)
			.fail(function (data) {
				clientResponser.relogin(data);
			})
		.then(function (data) {
			return chaseProxy.getTimesheets(data);
		})
			.fail(function (data) {
				console.log("fail")
				if (data.rejected === 302) {
					var relogin = database.fetchAuth(data)
						.then(function (data) {
							return chaseProxy.login(data)
						})
						.then(function (data){
		            		return database.insertDetails(data);
						})
						.then(function (data){
							return promise.call();
						}).end();
				} else if (data.rejected === 408) {
					var timeout = clientResponder.timeout(data).end();
					return;
				}
			})
		.then(function (data) {
			return database.saveTimeSheets(data);
		})
		.then(function (data) {
			return database.updateCurrentDates(data);
		})
		.then(function (data) {
			// Return timesheets
			clientResponser.timeSheets(data);
		});
	});

	promise.call();
});



/* 
	POST - Update single timesheet (locally - doesnt save to main Db / Chase) 
	{
		id: _id, 
		job: _id,
		todaysTime: [time in minutes]  
	}

	Timesheets are stored in total minutes
*/
app.use('/updatesingle', function(req, res) {
	var user = {req: req, res: res};
	user.id = req.body.id;

	if (user.id.length !== 24) {
		clientResponser.relogin(user);
		return;
	}

	user.job = {
		id: req.body.job,
		appTime: req.body.todaysTime
	}

	var promise = database.updateSingleJob(user)
	.then(function (data) {
		return clientResponser.updatedJob(data);
	})
	.fail(function (data) {
		clientResponser.couldntUpdate(data);
	})

});


/* 
	POST - Save all timesheets to CHASE 
	{
		id: _id 
	}

	Will update chase with all local timesheets data
*/
app.use('/saveall', function(req, res) {
	var user = {req: req, res: res};
	user.id = req.body.id;

	if (user.id.length !== 24) {
		clientResponser.relogin(user);
		return;
	}

	var promise = database.getAllTimesheets(user)
	.then(function (data) {
		return database.getUserCookies(data);
	})
	.then(function (data) {
		return chaseProxy.saveAllTimesheets(data);
	})
		.fail(function (data) {
			console.log("failed somewhere")
			if (data.rejected === 302) {
				var relogin = database.fetchAuth(data)
					.then(function (data) {
						return chaseProxy.login(data)
					})
					.then(function (data){
	            		return database.insertDetails(user);
					})
			}
		})
	
	.then(function (data) {
		return database.updateCurrentDates(data);
	})
	.then(function (data) {
		console.log("saved all - respond")
		return clientResponser.updatedAll(data);
	});


});







/* 
	Search 
	{
		id: _id,
		searchTerm: string
	}

	Search for jobs using Chases JobNumberSearch

	Returns an array of jobs with id, name, client.
*/
app.use('/search', function(req, res) {
	var user = {req: req, res: res};
	user.id = req.body.id;
	
	if (user.id.length !== 24) {
		clientResponser.relogin(user);
		return;
	}

	user.searchTerm = req.body.searchTerm;

	var promise = (function trySearch() {
			return database.getUserCookies(user)
			.fail(function (data) {
				clientResponser.relogin(data);
			})
		.then(function (data){
			return chaseProxy.searchJobNumber(data);
		})
		.fail(function (data) {
			if (data.rejected === 302) {
				var relogin = database.fetchAuth(data)
					.then(function (data) {
						return chaseProxy.login(data)
					})
					.then(function (data){
	            		return database.insertDetails(data);
					})
					.then(function (data){
						return promise.call();
					});

			}
		})
		.then(function (data){
			return clientResponser.searchJob(data);
		})
	});
	
	promise.call();
});






/* 
	Get job details with ID
		- This is used after searching ETC
	{
		id: _id,
		jobNumber: int
	}


	Returns object with details of job in array.
	We need to parse it and sort the data here and save it in the mongo db.
	We then return a clean object
 */
app.use('/grabjob', function(req, res) {
	var user = {req: req, res: res};
	user.id = req.body.id;
	
	if (user.id.length !== 24) {
		clientResponser.relogin(user);
		return;
	}

	user.jobNumber = req.body.jobNumber;

	var promise = (function sequence () {
			return database.getUserCookies(user)
			.fail(function (data) {
				clientResponser.relogin(data);
			})
		.then(function (data){
			return chaseProxy.grabJob(data);
		})
			.fail(function (data) {
				console.log("rejecting")
				if (data.rejected === 302) {
					console.log("302")
					var relogin = database.fetchAuth(data)
						.then(function (data) {
							return chaseProxy.login(data)
						})
						.then(function (data){
		            		return database.insertDetails(data);
						})
						.then(function (data){
							return promise.call();
						}).end();
				}
			})
		.then(function (data){
			return clientResponser.grabJob(data);
		})
	});

	promise.call();
});







/*
	POST - Add new job to user
	{
		id: _id,
		job: {
			customTitle: 		string,
			colour: 			string, // Optional
			isAnonymous: 		Boolean,
			linkedJob: 			{
				jobid: 				int,
				customerid: 		int,
				productid: 			int,
				notes: 				string 
			}
		}
	}

	Saves the new job to the user in the mongoDB
*/
app.use('/addnew', function(req, res) {
	var user = {req: req, res: res};
	user.id = req.body.id;
	if (user.id.length !== 24) {
		clientResponser.relogin(user);
		return;
	}


	user.job = {
		customTitle: 		req.body.customTitle,
		colour: 			req.body.colour,
		isAnonymous: 		Boolean(parseInt(req.body.isAnonymous)),
		linkedJob: 			{
			jobid: 				req.body.linkedJob.jobid,
			customerid: 		req.body.linkedJob.customerid,
			productid: 			req.body.linkedJob.productid,
			tasktypeid: 		null,
			notes: 				req.body.linkedJob.notes 
		}
	}


	// Otherwise add to chase and return. When navigating to index page it will automatically update local database
	var promise = (function sequence () {
			return database.getJobType(user)
			.then(function (data) {
				return functions.createNewLineItem(data);
			})
			.then(function (data) {
				return chaseProxy.saveNewLine(data);
			})
			.then(function (data) {
				return database.createNewJob(data);
			})
				.fail(function (data) {
					if (data.rejected === 302) {
						var relogin = database.fetchAuth(data)
							.then(function (data) {
								return chaseProxy.login(data)
							})
							.then(function (data){
			            		return database.insertDetails(data);
							})
							.then(function (data){
								return promise.call();
							});

					}
				})
			.then(function (data) {
				return clientResponser.success(data);
			});
	});

	promise.call();



});






app.use('/addanonymous', function(req, res) {
	var user = {req: req, res: res};
	user.id = req.body.id;
	if (user.id.length !== 24) {
		clientResponser.relogin(user);
		return;
	}

	user.job = {
		customTitle: 		req.body.customTitle,
		colour: 			req.body.colour,
		isAnonymous: 		Boolean(parseInt(req.body.isAnonymous)),
		//linkedJob: 			linkedJob
	}

	// If anonymour - create anonymous timesheet and end function
	if (Boolean(parseInt(req.body.isAnonymous))) {
		var promise = database.createNewJob(user)
		.then(function (data) {
			return clientResponser.success(data);
		});

		return;
	}


});



app.use('/linkanonymous', function(req, res) {
	var user = {req: req, res: res};
	user.id = req.body.id;
	if (user.id.length !== 24) {
		clientResponser.relogin(user);
		return;
	}


})



/* Change Colour - id: _id, job: _id, colour: {{'red', 'blue', 'yellow', null}} */
app.use('/colour', function(req, res) {
	var user = {req: req, res: res};
	user.id = req.body.id;
	
	if (user.id.length !== 24) {
		clientResponser.relogin(user);
		return;
	}

	user.jobID = req.body.job;
	user.colour = req.body.colour;

	var promise = database.colour(user)
		.then(function (data){
			return clientResponser.success(data);
		})
});



/* Hide / Show - id: _id, job: _id, hide: {{true, false}} */
app.use('/togglehide', function(req, res) {
	var user = {req: req, res: res};
	user.id = req.body.id;
	
	if (user.id.length !== 24) {
		clientResponser.relogin(user);
		return;
	}

	user.jobID = req.body.job;
	user.isHidden = req.body.hide;

	var promise = database.toggleHide(user)
		.then(function (data){
			return clientResponser.success(data);
		})
});



app.use('/starttiming', function(req, res) {
	var user = {req: req, res: res};
	user.id = req.body.id;

	user.jobID = req.body.jobID;
	user.timingStamp = req.body.timingStamp;
	user.maxTiming = req.body.maxTiming;

	var promise = (function startTiming () {
			return database.startTiming(user)
		.then(function (data) {
			clientResponser.success(data);
		});
	});

	promise.call();
});



app.use('/stoptiming', function(req, res) {
	var user = {req: req, res: res};
	user.id = req.body.id;

	user.jobID = req.body.jobID;
	user.addedTime = req.body.addedTime; //Work out the added time on the front end to avoid delays. Front end has the start and finish timestamp.

	var promise = (function stopTiming () {
			return database.stopTiming(user)
		.then(function (data) {
			clientResponser.success(data);
		});
	});

	promise.call();
});


// Favourite / unfavourite - isFavourite: {{0 / 1}}
app.use('/favourite', function(req, res) {
	var user = {req: req, res: res};
	user.id = req.body.id;

	user.jobID = req.body.jobID;
	user.isFavourite = parseInt(req.body.isFavourite);

	var promise = (function favourite () {
			return database.favourite(user)
		.then(function (data) {
			clientResponser.success(data);
		});
	});

	promise.call();
});


// Favourite / unfavourite - isFavourite: {{0 / 1}}
app.use('/purgeall', function(req, res) {
	var user = {req: req, res: res};
	user.id = req.body.id;

	var promise = (function favourite () {
			return database.purgeall(user)
		.then(function (data) {
			clientResponser.success(data);
		});
	});

	promise.call();
});



/*
	Start server
*/
console.log("listening on 7501")
app.listen(7501);



