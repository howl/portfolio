Fullstack is a software development agency that launched mid 2014.

Their website was built using Backbone, Marionette, and Require. It's a small, clean, responsive website with smooth animations and transitions.

The live site can be found here: http://fullstack.co.za/#!/
(It has been updated a few times since launch by the Fullstack team. We've included the original go live files)