require.config({
    baseUrl:"./js/app",
    // 3rd party script alias names (Easier to type "jquery" than "../libs/jquery/dist/jquery.min, etc")
    // probably a good idea to keep version numbers in the file names for updates checking
    paths:{
        // Core Libraries
        //"require": '../libs/requirejs/require',
        "jquery":"../libs/jquery/dist/jquery",
        "underscore":"../libs/lodash/dist/lodash",
        "backbone":"../libs/backbone/backbone",
        "marionette":"../libs/marionette/lib/core/backbone.marionette",

        // Plugins
        "backbone.validateAll":"../libs/Backbone.validateAll/src/javascripts/Backbone.validateAll",
        "text":"../libs/text/text",
        "async":"../libs/requirejs-plugins/src/async",
        "backbone.wreqr" : "../libs/backbone.wreqr/lib/backbone.wreqr",
        "backbone.babysitter" : "../libs/backbone.babysitter/lib/backbone.babysitter",
        "foundation": "../libs/foundation/foundation.min",
        "fastclick": "../libs/fastclick/lib/fastclick",
        "abide": "../libs/foundation/foundation/foundation.abide",
        "equalizer": "../libs/foundation/foundation/foundation.equalizer",
        "slick": "../libs/slick-carousel/slick/slick.min",
        "easing": "../libs/jquery-easing/jquery-easing",
        "form-helper-phone": "../libs/bootstrap-formhelpers/js/bootstrap-formhelpers-phone",
        "preloader": "../libs/pre-loader/pre-loader"

    },
    // Sets the configuration for your third party scripts that are not AMD compatible
    shim:{
        "jquery" : {
            exports : "jQuery"
        },
        "underscore": {
            exports: "_"
        },
        "backbone":{
            "deps":["jquery", "underscore"],
            // Exports the global window.Backbone object
            "exports":"Backbone"
        },
        // Backbone.validateAll plugin (https://github.com/gfranko/Backbone.validateAll)
        "backbone.validateAll":["backbone"],

        "foundation": {
            "deps": ["jquery"],
            "exports": "foundation"
        },

        "fastclick": {
            "deps": ["jquery"],
            "exports": "fastclick"
        },

        "abide": {
            "deps": ["jquery", "foundation"],
            "exports": "abide"
        },

        "equalizer": {
            "deps": ["jquery", "foundation"],
            "exports": "abide"
        },

        "slick": {
            "deps": ["jquery"],
            "exports": "slick"
        },

        "easing": {
            "deps": ["jquery"],
            "exports": "easing"
        },

        "form-helper-phone": {
            "exports": "form-helper-phone"
        }
    }
});

// Includes Desktop Specific JavaScript files here (or inside of your Desktop router)
require(["jquery", "foundation","App", "routers/AppRouter", "controllers/Controller", "preloader", "fastclick"],
    function ($, Foundation, App, AppRouter, Controller, preLoader) {

    


    //Once we've preloaded and ready to go
    function startApp () {
        App.appRouter = new AppRouter({
            controller:new Controller()
        });

        App.start();

        $(document).foundation({
            equalizer : {equalize_on_stack: true}
        });
    }

    //All images to preload
    var preloadImages = [
        "img/faded-desk.jpg",
        "img/founders-mike.jpg",
        "img/founders-silk.jpg",
        "img/home-bg-img.jpg",
        "img/contact-bg-img.jpg",
        "img/home-logo.png",
        "img/phone-slide-1.jpg",
        "img/phone-slide-2.jpg",
        "img/phone-slide-3.jpg",
        "img/phone-slide-4.jpg",
        "img/quote-img.jpg",
        "img/phone.png",
        "img/slider-dev.jpg",
        "img/slider-design.jpg",
        "img/slider-strategy.jpg",
        "img/slider-ux.jpg",
        "img/thumb-comfort.jpg",
        "img/thumb-sanlam.jpg",
        "img/thumb-soco.jpg",
        "img/thumb-xfighter2014.jpg"
    ];

    //Initiate Preloader
    $(function() {
        new preLoader(preloadImages, {
            pipeline: false,
            auto: true,
            onComplete: function(loaded, errors){
                $("#preloader-gif").fadeOut(400, function () {
                    startApp();
                });
            }
        });
    });

});