define([ 'App', 'marionette', 'text!templates/header.html'],
    function (App, Marionette, template) {

        return Marionette.ItemView.extend({

            template: _.template(template),

            className: "header-wrapper",

            onRender: function () {
                var that = this;

                this.scrollPosition = "above";

                App.Vent.bind("routeChange", this.routeChange, this);
            },

            onShow: function () {
                $(".top-bar .toggle-topbar.menu-icon a").click(function() {
                    $(".mobile-navigation").slideToggle();
                    $("nav.top-bar").toggleClass("open-menu");
                });

                $(".mobile-navigation ul a").click(function(){
                    $(".mobile-navigation").slideUp();
                });

                $( window ).resize(function() {
                    if ( $(window).width() > 640 ){
                        $(".mobile-navigation").hide();
                        $("nav.top-bar").removeClass("open-menu");
                    }
                });
            },

            onDestroy: function () {
                $(window).off("scroll.navbar");
                $(window).off("scroll.header");
            },

            onScroll: function () {
                var that = this;
                
                /*
                    Show or hide nav bar on scroll
                */

                if (!this.isAnimating && this.scrollPosition === "above" && $(window).scrollTop() > $("header").height() / 2.7) {
                    this.isAnimating = true;

                    $(".top-bar").css({
                        "position": "fixed",
                        "top": "-5.5rem"
                    }).addClass("home-page-show").animate({
                        "top": 0
                    }, 400, function () {
                        that.isAnimating = false;
                    })

                    this.scrollPosition = "below";


                } else if (!this.isAnimating && this.scrollPosition === "below" && $(window).scrollTop() < $("header").height() / 2.7){
                    this.isAnimating = true;

                	$(".top-bar").animate({
                        "top": "-5.5rem"
                    }, 400, function () {
                    	$(this).removeClass("home-page-show").hide().css({
                    		"position": "",
                    		"top": ""
                    	}).fadeIn(300, function () {
                            that.isAnimating = false;
                        })
                    })

                    this.scrollPosition = "above";
                };

            },

            routeChange: function () {
                /*
                    Home page nav bar scroll into position
                */
                if (window.route === "home") {
                    $(window).on("scroll.navbar", $.proxy( function(){
                        this.onScroll();
                    }, this));
                } else {
                    $(window).off("scroll.navbar");
                }

            }


        });
    });