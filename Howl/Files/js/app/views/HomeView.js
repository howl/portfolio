 define( [ 'App', 'marionette', 'text!templates/home.html', 'slick', 'equalizer', 'easing'],
    function( App, Marionette, template, slick, equalizer, easing) {

        return Marionette.ItemView.extend( {

            template: _.template(template),

            events: {
                "click .factors-selector": "selectFactor",
                "click .effect-work": "GoTo"
            },

            onRender: function () {
                var that = this;

                $(window).on("scroll.home", function(e){
                    that.parallax();
                });
            },

            onShow: function () {
                this.parallax();
                this.initSlider();
                $(document).foundation('equalizer', 'reflow');

                if (window.location.hash.indexOf("what-we-do") > -1) {
                    this.scrollTo("#what-we-do")
                } else if (window.location.hash.indexOf("our-work") > -1) {
                    this.scrollTo("#our-work")
                }

                App.Vent.trigger("showContent");

                $(".header-background").animate({"opacity": 1}, 1000);
                $("header").addClass("has-image");

                setTimeout(function(){
                    $("#preloader").fadeOut(400);
                }, 100);
                
            },

            onDestroy: function () {
                $(window).off("scroll.home");
                $("header").removeClass("has-image");
            },

            GoTo: function (e) {
                var route = $(e.target).closest(".effect-work").attr("data-href");
                App.Vent.trigger("GoTo", route);
            },

            parallax: function () {
                var scrolled = $(window).scrollTop();
                var startPos = $('.quote-image-wrapper').offset().top * 0.2 + 0;
                $('.quote-image').css('top', +(scrolled * 0.2) - startPos + 'px');

                var headerScrolled = $(window).scrollTop();
                $('.header-background').css('top', +(headerScrolled * 0.3) + 'px');
            },

            initSlider: function () {
                //What we do
                this.$('.factors-slides').slick({
                    autoplay: true,
                    autoplaySpeed: 5000,
                    speed: 1000,
                    initialSlide: 1,
                    adaptiveHeight: true,
                    nextArrow: "<a class='next-arrow'></a>",
                    prevArrow: "<a class='prev-arrow'></a>"
                });

                this.$('.factors-slides').on('beforeChange', function(event, slick, currentSlide, nextSlide){
                    var target = $(".factors-selector").eq(nextSlide);

                    //Move arrow
                    var left = target.offset().left;
                    left += target.width() / 2;
                    $(".factors-triangle").css({"left": left});

                    //Change Active
                    $(".factors-selector.active").removeClass("active")
                    target.addClass("active");
                });

                //Iphone device
                this.$('.slides').slick({
                    appendArrows: false,
                    autoplay: true,
                    autoplaySpeed: 5000,
                    speed: 1000,
                    initialSlide: 1,
                    dots: true
                });
            },

            selectFactor: function (e) {
                //Select slide
                var target = $(e.target).closest(".factors-selector");
                var index = $(".factors-selector").index(target);
                $(".factors-slides").slick("slickGoTo", index);
            },

            scrollTo: function (target) {
                setTimeout(function () {
                    $("html, body").animate({scrollTop: $(target).offset().top}, 1000, "easeInOutQuad")
                }, 300)
                
            }

        });
    });