 define( [ 'App', 'marionette', 'text!templates/project.html'],
    function( App, Marionette, template, Model) {

        return Marionette.ItemView.extend( {

            template: _.template(template, this.model),

            onRender: function () {
            	var that = this;

            	$("header").addClass("has-image");

            	$(window).on("scroll.project", function(e){
                    that.parallax();
                });
            },

            onDestroy: function () {
                $(window).off("scroll.project");
                $("header").removeClass("has-image");
            },

            parallax: function () {
                var scrolled = $(window).scrollTop();
                $('.header-background').css('top', +(scrolled * 0.3) + 'px');
            },

            onShow: function () {
                setTimeout(function(){
                    $("#preloader").fadeOut(400);
                }, 100);
            }
            
        });
    });