define([ 'marionette', 'text!templates/footer.html'],
    function (Marionette, template) {

        return Marionette.ItemView.extend({

            template: _.template(template),

            onRender: function () {
            	//Remove empty div wrapper
            	this.$el = this.$el.children();
		        this.$el.unwrap();
		        this.setElement(this.$el);
            }
        });
    });