 define( [ 'App', 'marionette', 'text!templates/services.html', 'equalizer'],
    function( App, Marionette, template, equalizer) {

        return Marionette.ItemView.extend( {

            template: _.template(template),

            onShow: function () {
            	$(document).foundation('equalizer', 'reflow');
            	
            	setTimeout(function(){
            		$("#preloader").fadeOut(400);
            	}, 100);
            }
        });
    });