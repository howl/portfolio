define( [ 'App', 'marionette', 'models/ContactModel', 'text!templates/contact.html', 'abide', "form-helper-phone"],
    function( App, Marionette, Model, template, abide) {

        return Marionette.ItemView.extend( {

            template: _.template(template),

            model: new Model(),

            onRender: function () {
                var that = this;

                this.$('#contact-us-form').foundation({bindings: 'events'});

                this.$('#contact-us-form').off('valid').on('valid', function() {
                    that.submitForm();
                });

                //Initiate bootstrap phone form helper
                this.$("#contact-number").bfhphone(this.$("#contact-number").data());

                $("header").addClass("has-image");

                $(window).on("scroll.header", $.proxy( function(){
                    this.parallax();
                }, this));
            },

            onShow: function () {
                var that = this;
                require(["async!http://maps.google.com/maps/api/js",], function () {
                    that.initiateGoogleMaps();
                });

                setTimeout(function(){
                    $("#preloader").fadeOut(400);
                }, 100);
            },
            
            submitForm: function () {
                var that = this;

                var data = $("#contact-us-form").serializeArray();
                $.map(data, function(n, i){
                    that.model.set( n.name, n.value );
                });

                that.model.submitForm(
                    function () {
                        $(".hide-on-success").hide();
                        $(".show-on-success").show();
                    },
                    function () {
                        $(".hide-on-fail").hide();
                        $(".show-on-fail").show();
                        setTimeout(function () {
                            $("form").find("input[type=text], textarea, input[type=email]").val("");
                            $("#contact-number").val("+27 ");
                            $(".show-on-fail").hide();
                            $(".hide-on-fail").show();
                        }, 15000);
                });
                
            },

            onDestroy: function () {
                $(window).off("scroll.header");
                $("header").removeClass("has-image");
            },

            parallax: function () {
                var scrolled = $(window).scrollTop();
                $('.header-background').css('top', +(scrolled * 0.3) + 'px');
            },

            initiateGoogleMaps: function () {
                var main_color = '#00000';
                var saturation_value= -99;
                var brightness_value= -44;

                var isDraggable = $(document).width() > 480 ? true : false;

                var style= [ 
                    {
                        "featureType": "all",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "saturation": 36
                            },
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 40
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 17
                            },
                            {
                                "weight": 1.2
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 21
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 17
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 29
                            },
                            {
                                "weight": 0.2
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 18
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 19
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#323232"
                            },
                            {
                                "lightness": 17
                            }
                        ]
                    }
                ];
                var mapOptions = {
                    center: { lat: -33.927658, lng: 18.437024},
                    zoom: 15,
                    panControl: false,
                    zoomControl: false,
                    mapTypeControl: false,
                    streetViewControl: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false,
                    styles: style,
                    draggable: isDraggable,
                };
                var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                var myLatlng = new google.maps.LatLng(-33.927658,18.437024);
                var image = 'img/map-marker.png';

                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: 'Hello World!',
                    icon: image
                });
            }
        });
    });