 define( [ 'App', 'marionette', 'text!templates/people.html'],
    function( App, Marionette, template) {

        return Marionette.ItemView.extend( {

            template: _.template(template),

            onShow: function () {
            	setTimeout(function(){
            		$("#preloader").fadeOut(400);
            	}, 100);
            }
        });
    });