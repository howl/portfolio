define(['App', 'backbone', 'marionette', 'views/HomeView', 'views/HeaderView', 'views/FooterView'],
    function (App, Backbone, Marionette, HomeView, HeaderView, FooterView) {
    return Backbone.Marionette.Controller.extend({
        initialize:function (options) {
            App.headerRegion.show(new HeaderView());
            App.footerRegion.show(new FooterView());
        },

        home:function (e) {
            App.mainRegion.show(new HomeView());
        },
        contact:function () {
            require(['views/ContactView'], function (ContactView) {
                App.mainRegion.show(new ContactView());
            });
        },
        people:function () {
            require(['views/PeopleView'], function (PeopleView) {
                App.mainRegion.show(new PeopleView());
            });
        },
        services:function () {
            require(['views/ServicesView'], function (ServicesView) {
                App.mainRegion.show(new ServicesView());
            });
        },
        work:function (project) {
            project = project.replace("!/", "");
            switch (project) {
                case "sanlam-reality":
                    break;
                case "red-bull-x-fighters2014":
                    break;
                case "go-durban":
                    break;
                case "comfort-lavender":
                    break;
                case "southern-comfort-ownyourawesome":
                    break;
                default:
                    project = null;
                    break;
            }
            if (project === null) Backbone.history.navigate("!/", { trigger:true, replace: true });

            require(['views/ProjectView', 'models/ProjectModel'], function (ProjectView, ProjectModel) {
                var model = new ProjectModel({project: project});
                App.mainRegion.show(new ProjectView({model: model}));
            });
        }
    });
});