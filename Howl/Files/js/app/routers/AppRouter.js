define(['App', 'marionette', 'controllers/Controller'], function(App, Marionette, Controller) {
	return Marionette.AppRouter.extend({
		//"index" must be a method in AppRouter's controller
		appRoutes: {
			"": "home",
			"!": "home",
			"!/": "home",
			"!/home": "home",
			"!/what-we-do": "home",
			"!/our-work": "home",
			"!/services": "services",
			"!/people": "people",
			"!/contact": "contact",
			"!/work/:project": "work",
			"notFound": "home"
		},

		initialize: function () {
			var that = this;

			this.on("route", function(route, params) {
				that.routeChange(route, params)
			});

			App.Vent.bind("GoTo", this.GoTo, this);

			App.Vent.bind("showContent", this.showContent, this);

			$(document).on("click", "a", function (e) {
				var href = $(e.target).closest("a").attr("href");
				if (href === undefined) return;

				$(".mobile-navigation").hide();
                $("nav.top-bar").removeClass("open-menu");

				if (window.route === "home" && (href === "#!/what-we-do" || href === "#!/our-work")) {
					App.Vent.trigger("scrollTo", href.replace("!", ""));
					return;
				}

				if ( href.indexOf("#") === 0) {
					that.hideContent(href);
					return false;
				}
			})

		},

		routeChange: function (route, params) {
			$("body").removeClass(window.route).addClass(route);
			window.route = route;

			App.Vent.trigger("routeChange");
		},

		GoTo: function (route) {
			if (route !== "#!/what-we-do" && route !== "#!/our-work") {
				$("html, body").scrollTop(0);
			}

			ga('send', 'pageview', route);

			this.navigate(route, {trigger: true});
		},

		showContent: function () {

		},

		hideContent: function (route) {
			var that = this;
			//$("html, body").animate({scrollTop: 0}, 400, "easeInOutQuad");
			$("#preloader").fadeIn(400, function () {
				that.GoTo(route);
			});
			
		}
	});
});