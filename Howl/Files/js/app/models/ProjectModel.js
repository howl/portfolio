define(["jquery", "backbone"],
    function ($, Backbone) {
        var Model = Backbone.Model.extend({
            initialize:function () {
                this.set("title", this.getTitle());
                this.set("agency", this.getAgency());
                this.set("subtitle", this.getSubTitle());
                this.set("copy", this.getCopy());
                this.set("livelink", this.getLiveLink());
                this.set("behancelink", this.getBehanceLink());
            },

            defaults:{
                project: ""
            },

            getTitle: function () {
                switch(this.get("project")) {
                    case "sanlam-reality":
                        return "Sanlam Reality";

                    case "red-bull-x-fighters2014":
                        return "Red Bull X-Fighters";

                    case "go-durban":
                        return "Project Title";

                    case "comfort-lavender":
                        return "Comfort Digital Campaign";

                    case "southern-comfort-ownyourawesome":
                        return "SoCo Own Your Awesome";

                    default:
                        return "";
                }
            },

            getAgency: function () {
                switch(this.get("project")) {
                    case "sanlam-reality":
                        return "Agency: <a href='http://www.publicismachine.co.za/' target='_blank'><h4>Publicis Machine</h4></a>";

                    case "red-bull-x-fighters2014":
                        return "Agency: <a href='http://www.publicismachine.co.za/' target='_blank'><h4>Publicis Machine</h4></a>";

                    case "go-durban":
                        return "Project Title";

                    case "comfort-lavender":
                        return "Agency: <a href='http://www.thenicheguys.com/' target='_blank'><h4>Niche Digital</h4></a>";

                    case "southern-comfort-ownyourawesome":
                        return "Agency: <a href='http://www.publicismachine.co.za/' target='_blank'><h4>Publicis Machine</h4></a>";

                    default:
                        return "";
                }
            },

            getSubTitle: function () {
                switch(this.get("project")) {
                    case "sanlam-reality":
                        return "Sanlam Reality is a lifestyle, wellness and rewards programme available to Sanlam group clients. To bring Sanlam Reality inline with today's web trends & practice, we gave it a complete UX overhaul and rebuilt it from the ground up giving users a simpler and more enjoyable experience.";

                    case "red-bull-x-fighters2014":
                        return "The Red Bull X-Fighters World Tour is the premier Freestyle Motocross series in its discipline of motorsport. 2014 marked the first time a Red Bull X-Fighters World Tour stop was to be held in South Africa. Along with an integrated campaign to build hype around the event we set out to educate consumers on the sport by creating a web based trivia game where players could compete for the grand prize.";

                    case "go-durban":
                        return "Project Blurb";

                    case "comfort-lavender":
                        return "Comfort Fabric Conditioner was launching its newest fragrent here in South Africa, Comfort Lavender. The new arrival was branded around the love of two of its favourite mascots and to celebrate this we launched a complete web based competition where mums could win a second honeymoon. This has been our most successful campaign to date.";

                    case "southern-comfort-ownyourawesome":
                        return "Southern Comfort was on the search for South Africa's trediest and coolest cats to serve as 'flagraisers', a young ambassador of the brand.<br/> We created an online competition entry submission to scratch at the cool kids, and have them come to us, by offering prizes and promoting their unique selves on the brand's channels.";

                    default:
                        return "";
                }
            },

            getCopy: function () {
                switch(this.get("project")) {
                    case "sanlam-reality":
                        return "As with all projects, we began by analysing Reality's business needs and its market, and broke down their existing information architecture to simplify and ensure all focalpoints were correctly hit.<br/><br/>" + 
                        "Once we completed wireframes and sent them off to design, we began developing with an agile approach, building the site piece by piece with modular atoms and integrating with the wordpress CMS and its data endpoints.";

                    case "red-bull-x-fighters2014":
                        return "Working with such a high calibre of designers, writers and strategists made this project a breeze. We began with wireframes, breaking down the challenge of making a trivia app accessible on desktop, smartphones and even outdated mobile devices.<br/><br/> The game was built on a Backbone, Marrionette and Require foundation with a .NET endpoint to give a smooth and efficient feel without heavy loads.";

                    case "go-durban":
                        return "";

                    case "comfort-lavender":
                        return "The game was simple: We hid Comfort Lavender bottles all across the internet in key places that both translated the brand message and got mums excited for the possiblity of winning a second honeymoon. Whenever a bottle was found or the game shared over social media; points were added to the users unique profile. <br/><br/>Everyday clues to the where abouts of the bottles were released on Comforts Facebook page. We also teamed up with bloggers, news agents, TV shows and people of interest to drive our mums to their sites in search of bottles in exchange for reviews and articles. <br/><br/>" + 

                        "To begin playing the game mums had to simply click the start button and a browser extention automatically installed. The browser extension was built completely in JavaScript with a PHP and MySql endpoint and database." + 

                            "Using the global key visuals we designed and built the entire solution.";

                    case "southern-comfort-ownyourawesome":
                        return "The project involved a fully responsive and social-intergrated digital service.<br/>A timeline of current 'flagraisers' and projects Southern Comfort was initiating, with branded content and related links navigating from the landing page. Further, there offered Southern Comfort fans to speak and say why they believe they could be a suitable candidate and brand ambassador. The user would have to share an array of media of their choice (from songs to photos, to videos etc) showcasing their talented selves and abilities. Their submissions were then captured, moderated and displayed on the enties page for all to observe and take in the wonderful fans, and all their flavour and taste, of Southern Comfort.";

                    default:
                        return "";
                }
            },

            getLiveLink: function () {
                switch(this.get("project")) {
                    case "sanlam-reality":
                        return "https://www.sanlamreality.co.za/";

                    case "red-bull-x-fighters2014":
                        return "";

                    case "go-durban":
                        return "http://www.godurban.co.za/";

                    case "comfort-lavender":
                        return "";

                    case "southern-comfort-ownyourawesome":
                        return "http://web.southerncomfort.com/SA/ownyourawesome/";

                    default:
                        return "";
                }
            },

            getBehanceLink: function () {
                switch(this.get("project")) {
                    case "sanlam-reality":
                        return "";

                    case "red-bull-x-fighters2014":
                        return "";

                    case "go-durban":
                        return "";

                    case "comfort-lavender":
                        return "";

                    case "southern-comfort-ownyourawesome":
                        return "";

                    default:
                        return "";
                }
            }

        });

        return Model;
    }
);