define(["jquery", "backbone"],
    function ($, Backbone) {
        var Model = Backbone.Model.extend({
            submitForm: function (success, fail) {
                if ($("#last-name").val() !== "") return;

                $.ajax({
                    type: "POST",
                    url: "server/contact-form.php",
                    data: this.toJSON()
                })
                .done(function() {
                    success.call();
                })
                .fail(function() {
                    fail.call();
                });
                
            }
        });

        return Model;
    }
);