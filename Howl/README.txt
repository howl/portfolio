HOWL, our own agency, practices software development on a client contractual basis for a mix of projects.
It launched 03/2015.  

The porftfolio-website was built using Backbone, Marionette, and Require. As well as utilizing GULP (instead of GRUNT)
& using Bower as web app management. 
It's a small, clean, responsive website to showcase our agency & service offering.

The live site can be found here: http://howlagency.co.za/
(There will be further additions and 'fluff pieces' to be added in the early months).