Sanlam Reality is a lifestyle and rewards program owned by Sanlam - one of the largest financial services groups in South Africa.
Sanlam Reality required an overhaul of their website from the ground up. Over 2 months we desiphered and broke down the businiess
offerings and scattered information on theirold website, put together wireframes and sent them in for design. 

The website was built to allow for white labeling of the program - to sell reward programs to other businesses under the Reality name.
There are currently 4 variations of the main site.

The process for building the frontend of this website was to use an atomic approach (http://bradfrost.com/blog/post/atomic-web-design/)
with grunt to compile the atoms. The front end uses Zurbs Foundation framework to speed up developement with various javascript plugins.

The site had its soft launch at the end of February 2015, with the second round of updates set to launch at the end of March 2015.

You can view the current live site at: http://www.sanlamreality.co.za

We've included some of the JavaScript files and some of the atom files (CSS). This project did not use SCSS or any precompiler for CSS.
