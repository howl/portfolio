$(document).ready(function () {

    // -----------------------------------------//
    //     Search                               //
    //------------------------------------------//


    // Open / Close the search input box in the header
    function toggleNavSearch () {
        var navSearch = $(".nav-search");
        var navSearchInput = $(".nav-search input");

        navSearch.toggleClass("open");
        navSearchInput.val("");
        navSearch.addClass("empty");
    }


    // Show / submit search button
    $(".nav-search a.show").click(function () {
        var navSearch = $(this).closest(".nav-search");
        
        //Submit form if it has value
        if ( !navSearch.hasClass("empty") ) {
            navSearch.find("form").submit();
            return;
        }

        // Toggle form
        toggleNavSearch();

        // Hide the search input when clicking outside of the form
        $('body').on("click.nav-search", function(evt) {
            if ( $(evt.target) === $(".nav-search") || $(evt.target).closest('.nav-search').length) return;
            toggleNavSearch();
            $('body').off("click.nav-search");
        });
    });



    // Change the search / submit icon when typing
    $(".nav .nav-search input[name=s]").keyup(function () {
        var this = $(this);
        var navSearch = $(this).closest(".nav-search");

        if ( this.val().trim() === "" ) {
            navSearch.addClass("empty");
        } else {
            navSearch.removeClass("empty");
        }
    });


    // Don't submit the form if it is empty
    $(".nav-search form").on("submit", function () {
        var this = $(this);
        if ( this.find("input[name=s]").val().trim() === "" ) {
            return false;
        }
    });


    // Mobile search icon
    $(".mobile-search-input").keyup(function () {
        var this = $(this);
        var parent = $(this).parent();

        if ( this.val().trim() === "" ) {
            parent.addClass("empty");
        } else {
            parent.removeClass("empty");
        }
    });

    $(".mobile-search-submit").on("click", function () {
        $(this).parent().submit();
    });

    $(".mobile-search-form").on("submit", function () {
        if ($(".mobile-search-input").val().trim() === "") return false; 
    })




    // -----------------------------------------//
    //     login                                //
    //------------------------------------------//

    $("#mobile-login").click(function () {
        $(".mobile-login-form").slideToggle();
    })

    $("label[for='login-number']").click(function(event) {
        event.preventDefault();
    })

    $("input[name=login]").click(function () {
        var id = $(this).closest("form").find(".number").val();
        if (id.length < 1 ) {
            $(this).closest("form").find("label.error").text("I'm sorry, your login is not correct");
            return false;
        }
    });




    // -----------------------------------------//
    //     Benefits grid                        //
    //------------------------------------------//

    // Create Isotope
    var gridContainer = $('#benefit-grid');
    
    gridContainer.isotope({
        itemSelector: '.benefit-block',
        layoutMode: 'fitRows'
    });

    gridContainer.imagesLoaded( function() {
        gridContainer.isotope('layout');
    });


    // Full menu filter
    $("#grid-menu a").click(function () {
        var menuItem = $(this).parent();
        var filterType = $(this).attr('data-filter-selector');

        // Add Active State CSS
        if (!menuItem.hasClass("active")) {
            $("#grid-menu li.active").removeClass("active");
            menuItem.addClass("active");
        }

        //Filter based on data-filter
        gridContainer.isotope({
            filter: function () {
                if (filterType === "all") return true;
                return $(this).attr('data-filter').indexOf(filterType) > -1;
            }
        });
    });

    // Mobile select menu filter
    $("select#grid-menu-mobile").change(function () {
        var filterType = $("select#grid-menu-mobile option:selected").attr('data-filter-selector');

        gridContainer.isotope({
            filter: function () {
                if (filterType === "all") return true;
                return $(this).attr('data-filter').indexOf(filterType) > -1;
            }
        });
    });




    // -----------------------------------------//
    //     Table                                //
    //------------------------------------------//

    var tableControl = $('.table-control')

    if (tableControl.length > 0) {
        tableController.init();
    }



    // -----------------------------------------//
    //     All Dropdowns                        //
    //------------------------------------------//

    function toggleDropdown (el) {
        var shadowOverlay = $(".shadow-overlay");

        if (el !== undefined) {
            $(el).slideDown(400).addClass('active');

            //Dont focus on input on mobile
            if (!Modernizr.mq('(max-width: 1024px)')) {
                $(el).find("input").eq(0).focus();
            }
        } else {
            $('.dropdown.active').slideUp().removeClass('active');
        }

        if ( shadowOverlay.length < 1) {
            $('body').append("<div class='shadow-overlay' />")
            shadowOverlay.click(function() {
                toggleDropdown();
            });
        }

        shadowOverlay.fadeToggle(200);
        $("html").toggleClass("show-shadow");

    }

    $(".show-dropdown").click(function(){
        var el = "#" + $(this).attr('data-custom-dropdown');
        toggleDropdown(el);
    });

    $(".close-dropdown").click(function(){
        toggleDropdown();
    });

    $(".shadow-overlay").click(function() {
        toggleDropdown();
    });




    // -----------------------------------------//
    //     Opening Drawer                       //
    //------------------------------------------//

    $(".expanding-button").click(function(){
        var el = $(this).attr("data-for");
        el = $("#" + el);

        if ($(this).hasClass("clicked")) {
            el.slideUp();
            $(this).removeClass("clicked");
        } else {
            el.slideDown();
            $(this).addClass("clicked");
        }
    });

    // -----------------------------------------//
    //     Scroll to element                    //
    //------------------------------------------//
    $(".scroll-to-element").click(function (e) {
        var target = $(this).attr("data-target");

        if ($(target).length < 1) return false;
        e.preventDefault();

        var pos = $(target).offset().top;
        $('html, body').animate({scrollTop: pos}, 900);
    })

    // -----------------------------------------//
    //     Accordian                            //
    //------------------------------------------//
    
    //first remove empty p tags causeing layout issues
    $('.sng-accordion p:empty').remove();
    $('.sng-accordion p > span:empty').parent().remove();

    var accordion = $( ".sng-accordion" ).accordion({
        autoHeight: true,
        collapsible: true,
        active: false,
        heightStyle: "content",
        animate: 200
    });

    // Open content that matches the hash
    var hash = window.location.hash;
    if(hash && hash !== "#" && hash !== "#_"){
        var anchor = hash.substring(hash.lastIndexOf('#') + 1, hash.length);
        var target = accordion.find('a[name*='+ anchor + ']').closest('.ui-accordion-header');
        var container = target.closest(".sng-accordion");
        var index = container.children(".ui-accordion-header").index(target);

        container.accordion( "option", "active", index);
    }

    if ( window.location.href.indexOf("terms-conditions") > -1 ) {
        accordion.accordion( "option", "activate", function( event, ui ) {
            if ($(".ui-state-active").length > 0) {
                var name = ui.newHeader.find("a").attr("name");
                if (name) {
                    window.location.hash = name;
                } else {
                    window.location.hash = "#_"; 
                }
            } else {
                window.location.hash = "#_";
            }
        });
    }

    // -----------------------------------------//
    //     Action Panel                         //
    //------------------------------------------//

    //Action panel login / signup
    $("#signup-login").click(function() {
        $("#login-actionpanel").show();
        $("#signup-actionpanel").hide();
    });
    $("#login-signup").click(function() {
        $("#signup-actionpanel").show();
        $("#login-actionpanel").hide();
    });



    var title = document.title;
    var url = location.href;

    $('.social .facebook').click(function (e) {
        e.preventDefault();
        window.open('http://www.facebook.com/sharer.php?u='+ encodeURIComponent(url),'sharer','toolbar=0,status=0,width=626,height=436');
    });

    $('.social .twitter').click(function (e) {
        e.preventDefault();
        window.open('http://twitter.com/share?text=Great%20stuff%20from%20Reality: ', 'MyWindow' , 'location=0,status=0,scrollbars=0, width=650,height=300');
    });

    $('.social .linkedin').click(function (e) {
        e.preventDefault();
        window.open('http://www.linkedin.com/shareArticle?mini=true&url=' + url + '&title=' + title + '&source=' + window.location.hostname, 'MyWindow', 'location=0,status=0,scrollbars=0, width=650,height=300');
    });

    $('.social .google').click(function (e) {
        e.preventDefault();
        window.open('https://plus.google.com/share?url=' + url, 'MyWindow', 'location=0,status=0,scrollbars=0, width=650,height=300');
    });


    // -----------------------------------------//
    //     Initiate functions                   //
    //------------------------------------------//

    $('.table-stack table').stacktable();


    if ($("#action-panel").length) {
        ActionPanel.init();
        ActionPanel.checkPosition();
    }

    if ($("#banner").length) {
        imagesLoaded( $("#banner"), function() {
            //$("#banner").fadein(300);
            mainSlider.init();
        })
    }

    if ($(".matrix-slider-container").length) {
        //imagesLoaded( $(".matrix-slider-container"), function() {
            matrixSlider.init();
        //})
    }

    if ($(".block-slider").length) {
        blockSliders.init();
    }

    if ($(".secondary-slider").length) {
        secondarySlider.init();
    }

    if ($(".movie-cart-wrapper").length) {
        moviesCart.initCart();
    }


    $(".membership-block h3").click(function(){
        $(this).closest(".membership-block").toggleClass("open");
    })

    // Tel links do nothing on desktop
    $('.no-touch a[href^="tel"]').on('click',function (e) {
        e.preventDefault();
    });

    $(".view-restaurants").click(function (e) {
        if ($(".dining-matrix").length < 1) return false;
        e.preventDefault();
        var pos = $(".dining-matrix").offset().top;
        $('html, body').animate({scrollTop: pos}, 900);
    })


    var hasSlider = $(".secondary-slider").length;
    var hasDiscountBlocks = $(".discount-block").length;
    var hasMembershipBlocks = $(".membership-block").length;

    function onResize () {
        winWidth = $(window).width();
        ActionPanel.checkPosition();
        mediaQueriesEveryFrame();
        mainSlider.resize();
        blockSliders.hideShowArrows();

        if (matrixSlider.settings.hasDrawer && !Modernizr.mq('(max-width: 640px)')) {
            matrixSlider.hideDrawer();
        }

        if (hasSlider) {
            secondarySlider.resize();
        }

        if(hasDiscountBlocks) {
            resizeDiscountBlocks();
        }

        if(hasMembershipBlocks) {
            resizeMembershipBlocks();
        }

    }

    $(window).resize(function () {
        onResize();
        $(window).resize(function() {
            clearTimeout($.data(this, 'resizeTimer'));
            $.data(this, 'resizeTimer', setTimeout(function() {
                onResize();
            }, 200));
        });
    });

    setTimeout(function () {
        onResize();
    }, 200)

    $(window).scroll(function() {
        winTop = $(window, document).scrollTop();
        ActionPanel.checkPosition();
    });

    /* Forms */
    $(".form").validate();
    $('input.number').jStepper({disableNonNumeric:true, allowDecimals: false, disableMouseWheel: true});
    $('form textarea').autosize();


    $(document).foundation();


    $(".toggle-button").each(function(i, item){
        var selector = $(this).data("selector");
        if(selector){
            $(selector).hide();
        }
    });

    $(".toggle-button").on("click", function(){
        var selector = $(this).data("selector");
        $(selector).slideToggle(300);
    });

});