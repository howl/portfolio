"use strict";
var DEBUG = false;
try{
    window.console.log("");
}
catch(e){
    var console={};
        console.log = function(a){}
}


// -----------------------------------------//
//     Main slider                          //
//------------------------------------------//

var mainSlider = {
    init: function () {
        var that = this;

        $('.main-slider').slick({
            adaptiveHeight: true,
            prevArrow: "#banner .arrow.l",
            nextArrow: "#banner .arrow.r",
            speed: 1000,
            autoplay: true,
            autoplaySpeed: 7000,
            onInit: function () {
                that.resize();
            }
        });
    },

    resize: function () {
        var margin = (getCurrentBreakpoint() === 480) ? "" : $("#banner .slick-slide a").width() / -2;
        $("#banner .banner-slide a").css({
            "margin-left": margin
        });

        if ( $("#banner .main-slider .banner-slide").length > 1 && getCurrentBreakpoint() > 640) {
            $("#banner .arrow-container").show();
        } else {
            $("#banner .arrow-container").hide();
        }
    }
};

// -----------------------------------------//
//     Secondary slider                     //
//------------------------------------------//

var secondarySlider = {
    init: function () {
        $('.secondary-slider').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 1000,
            prevArrow: ".secondary-slider-wrapper .arrow.l",
            nextArrow: ".secondary-slider-wrapper .arrow.r"
        });
    },

    resize: function () {
        var sliderImage = $(".secondary-slider .image");

        if (Modernizr.mq('(max-width: 720px)')) {
            sliderImage.css({"height": ""});
            return;
        }
        
        sliderImage.hide();

        sliderImage.each(function (){
            var height = $(this).closest(".slick-track").height();
            $(this).height(height);
        });
        
        sliderImage.show();
    }
};



// -----------------------------------------//
//     Single benefit matrix                //
//------------------------------------------//

var matrixSlider = {
    settings: {
        total: 27,
        current: 18,
        rowLength: 3
    },

    init: function () {
        var that = this;

        if ( Modernizr.mq('(max-width: 640px)') ) {
            this.initEvents();
            return;
        }

        that.setRowLength();

        that.setupDomStructure();

        // Hide arrows if not enough items in slider
        $(".matrix-slider .slider-fade").show();
        if ($('.matrix-slider-item').length < 10 && this.settings.rowLength === 3) {
            $(".matrix-slider .slider-fade").hide();
        } else if ($('.matrix-slider-item').length < 5 && this.settings.rowLength === 2) {
            $(".matrix-slider .slider-fade").hide();
        }

        //Create Slick
        $('.matrix-slider-container').slick({
            variableWidth: true,
            infinite: false,
            speed: 600,
            prevArrow: $(".matrix-slider .slider-fade.l"),
            nextArrow: $(".matrix-slider .slider-fade.r"),
            onBeforeChange: function (object, currentIndex,targetIndex) {
                that.isSliding = true;
                that.hideDrawer();
                $(".matrix-slider-block").addClass("force-show");
            },
            onAfterChange: function (object, index) {
                that.isSliding = false;
                $(".matrix-slider-block").removeClass("force-show");
            }
        });

        this.initEvents();

    },

    initEvents: function () {
        var that = this;

        //Show drawer
        $(".matrix-slider-container").off("click", ".matrix-slider-item .book-seats, .matrix-slider-item a")
            .on("click", ".matrix-slider-item .book-seats, .matrix-slider-item a", function (e) {
                if ( that.isSliding === true ) return;

                that.openingDrawer = true;
                var target = $(e.target).closest(".matrix-slider-item");

                //Close draw if same draw is already open
                if ( target.attr("data-id") === $(".booking-drawer").find("#show-booking-form").attr("data-id") || 
                    target.attr("data-id") === $(".booking-drawer").find(".add-to-cart").attr("data-id") || 
                    target.attr("data-id") === $(".booking-drawer").attr("data-id")) {
                    that.hideDrawer();
                    return;
                }

                //Hide drawer is already open
                if ( $(".booking-drawer").length > 0 ) {
                    that.changeDrawer(target);
                    return;
                }

                // Dont open draw if clicking on non-active slide
                // Slide to clicked slide
                if ( $(this).closest(".slick-active").length === 0 && !Modernizr.mq('(max-width: 640px)')) {
                    return;
                }

                that.showDrawer(target);
            });

        $(window).on("mobile", function () {
            //Timeout to stop bug (Function running multiple times on resize slowing browser down considerable)
            clearTimeout($.data(this, 'destroyTimeout'));
            $.data(this, 'destroyTimeout', setTimeout(function() {
                that.destroy();
                that.initEvents();
                $(".slider-fade").css({"display": ""});
            }, 200));
        });
        $(window).on("tablet", function () {
            //Timeout to stop bug (Function running multiple times on resize slowing browser down considerable)
            clearTimeout($.data(this, 'destroyTimeout'));
            $.data(this, 'destroyTimeout', setTimeout(function() {
                that.destroy();
                that.init();
            }, 200));
        });
        $(window).on("desktop", function () {
            //Timeout to stop bug (Function running multiple times on resize slowing browser down considerable)
            clearTimeout($.data(this, 'destroyTimeout'));
            $.data(this, 'destroyTimeout', setTimeout(function() {
                that.destroy();
                that.init();
            }, 200));
        });
    },

    setRowLength: function () {
        //Set row length
        if (getCurrentBreakpoint() <= mobile) {
            this.settings.rowLength = 1;
        } else if (getCurrentBreakpoint() > mobile && getCurrentBreakpoint() < 961) {
            this.settings.rowLength = 2;
        } else {
            this.settings.rowLength = 3;
        }
    },

    setupDomStructure: function () {
        //Set how many items per slide based on window width
        if (!$(".matrix-slider").hasClass("nu-metro")) {

            var itemsPerBlock;
            if ( getCurrentBreakpoint() > 960 ) {
                itemsPerBlock = 9;
            } else if ( getCurrentBreakpoint() > 640 ) {
                itemsPerBlock = 4;
            } else {
                itemsPerBlock = 1;
            }
        
        //If it is Nu Metro - minus a row per slide cause images are massive
        } else {
            var itemsPerBlock;
            if ( getCurrentBreakpoint() > 960 ) {
                itemsPerBlock = 6;
            } else if ( getCurrentBreakpoint() > 640 ) {
                itemsPerBlock = 2;
            } else {
                itemsPerBlock = 1;
            }
        }


        //Setup DOM structure based on window size
        var items = $('.matrix-slider-item').clone();
        $('.matrix-slider-item').remove();

        for ( var i = 0; i < Math.ceil(items.length / itemsPerBlock); i ++ ) {
            var block = $("<div class='matrix-slider-block'><div class='matrix-slider-inner' /></div>");
            block.find(".matrix-slider-inner").append(items.slice(i * itemsPerBlock, i * itemsPerBlock + itemsPerBlock));
            $(".matrix-slider-container").append(block);
        }
    },

    destroy: function () {
        var arrows = $(".matrix-slider .slider-fade").clone();
        var items = $(".matrix-slider-item").clone();

        $('.matrix-slider-container').unslick();
        $(".matrix-slider-container").html("");

        $(".matrix-slider-container").append(items);
        $(".matrix-slider").prepend(arrows);
        
    },

    appendItems: function (data) {
        var items = data.items;

        //Add data to template object in html. Can easily pull it when needed. No need to store it in javascript memory
        for ( var i = 0; i < items.length; i ++ ) {

            //Create element
            var element = $(".matrix-slider-item").eq(0).clone();
            element.find("h3 a").text(items[i].title);
            element.find("p").html(items[i].details);
            element.find("img").attr("src", items[i].image.src)
                .attr("alt", items[i].image.alt);
            element.attr("data-description", items[i].description)
                .attr("data-large-image", items[i].image.large);

            //Check if last slider block has space for more items
            var itemsInLastBlock = $(".matrix-slider-block").last().find(".matrix-slider-item").length;
            var hasSpace = (itemsInLastBlock < 9) ? true : false;

            //Create new slider block
            if (!hasSpace) {
                $('.matrix-slider-container').slickAdd("<div class='matrix-slider-block' />");
            }

            //Append item
            $(".matrix-slider-block").last().append(element);

        }

        this.initEvents();
    },

    // The booking drawer is added as a floating Absolute element over the entire matrix.
    // This is to allow for the breaking through of the slider overflow and its wrapper.
    // We reposition the drawer as the window sizes and add a margin to the bottom of the matrix items to
    // give the effect that the drawer has opened up the slider.
    showDrawer: function (target) {
        var that = this;

        // Animate the drawer opening by first animating the margins of the items in the row above it.
        // Find the items in the row above it in all slider blocks

        // Find the row that must open
        that.setRow(target);

        //Create drawer
        that.createAndInsertDrawer(target);

        that.settings.drawer = $(".matrix-slider .booking-drawer");

        //Reposition Drawer
        that.positionDrawer();
        that.settings.hasDrawer = true;

        $(".booking-drawer").css({"display": "block","opacity": 0});
        that.setItemMargins();
        $(".booking-drawer").css({"display": "none","opacity": 1});

        //Set triangle position
        this.positionTriangle(target);

        //show draw
        setTimeout(function () {
            $(".booking-drawer").slideDown(300, function () {
                that.initDrawerEvents();
                $(".booking-drawer-arrow").animate({"top": "-25px"});
                that.openingDrawer = false;
            });
        }, 40);


        //animate scroll
        var selector = (Modernizr.mq('(max-width: 640px)') ? ".matrix-slider-item" : ".matrix-slider-block .matrix-slider-item");
        var distance = (Modernizr.mq('(max-width: 640px)') ? 0 : target.height() - 90);
        $("html, body").animate({ scrollTop: target.offset().top + distance}, 1000);

    },

    // If a drawer is open and a user clicks to open another drawer,
    // fade out the old one and fade in the new one
    changeDrawer: function (target) {
        var that = this;
        that.openingDrawer = true;


        // If its a new row. first close the row.
        var index = target.closest(".matrix-slider-block").find(".matrix-slider-item").index(target);
        if (index + 1 <= that.settings.insertStart || index + 1 > that.settings.insertEnd) {

            // Animate margins Up
            setTimeout(function () {
                $(".matrix-slider-item").css({"margin-bottom": ""});
            }, 50);

            // Slide up
            $(".booking-drawer").slideUp(350, function(){
                setTimeout(function () {
                    // Change content
                    that.createAndInsertDrawer(target);
                    that.positionTriangle(target);
                    $(".booking-drawer-arrow").css({"top": "0"});
                    that.setRow(target);
                    that.positionDrawer();

                    // Slide down
                    $(".booking-drawer").css({"display": "block","opacity": 0});
                    that.setItemMargins();
                    $(".booking-drawer").css({"display": "none","opacity": 1});


                    $(".booking-drawer").slideDown(350, function () {
                        that.initDrawerEvents();
                        $(".booking-drawer-arrow").animate({"top": "-25px"});
                        that.openingDrawer = false;
                    });


                    var selector = (Modernizr.mq('(max-width: 640px)') ? ".matrix-slider-item" : ".matrix-slider-block .matrix-slider-item");
                    var distance = (Modernizr.mq('(max-width: 640px)') ? 0 : target.height() - 90);
                    $("html, body").animate({ scrollTop: target.offset().top + distance}, 1000);

                }, 50);

            });


            // Else just fade in new content
        } else {
            $(".booking-drawer .content").animate({
                "margin-left": "-50%",
                "opacity": "0"
            }, 250, function () {
                that.createAndInsertDrawer(target);
                that.positionTriangle(target);
                $(".booking-drawer .content").css({"margin-left": "50%"});
                $(".booking-drawer .content").animate({
                    "margin-left": "0px",
                    "opacity": "1"
                }, 250, function () {
                    that.openingDrawer = false;
                    that.initDrawerEvents();
                });

            });
        }
    },

    createAndInsertDrawer: function (target) {
        var that = this,
            eventdates = [],
            drawer;


        // If booking drawer doesn't exist, create it. Otherwise just insert new data
        if ($(".booking-drawer").length < 1) {
            drawer = $("<div class='booking-drawer' />").html($("#matrix-drawer-template").html());
            $(document).trigger('matrix-drawer-opened', drawer);
        } else {
            drawer = $(".booking-drawer");
        }

        drawer.find("h3").text(target.find("h3 a").text().replace(target.find("h3 span").text(), "") + " " );
        drawer.find("p.description").html(target.attr("data-description"));

        drawer.find("p.dates").html(moment(target.attr("data-dates-start")).format("DD MMMM YYYY") + " <strong>&nbsp;&nbsp; to &nbsp;&nbsp;</strong> " + moment(target.attr("data-dates-end")).format("DD MMMM YYYY"));
        drawer.find("p.venue").html(target.attr("data-venue"));
        drawer.find("p.price").html(target.attr("data-price"));

        drawer.attr({"data-id": target.attr("data-id")});
        drawer.find("a").attr({
            "data-id": target.attr("data-id"),
            "data-incentiv-id": target.attr("data-incentiv-id")
        });
        drawer.find("p.callus").html("<br />Call us on: 0860 732 5489 to book your ticket");



        //If video or image - insert video / image
        if (target.attr("data-large-image") !== undefined) {
            drawer.find(".image img").attr("src", target.attr("data-large-image"));
        } else {
            drawer.find(".trailer source").attr("src", target.attr("data-video"));
        }

        // Insert drawer
        if ($(".booking-drawer").length < 1) {
            $(".matrix-slider").append(drawer);
        }

        //if mobile list. Place draw beneath selected item
        if (Modernizr.mq('(max-width: 640px)')) {
            var mobileDraw =  $(".booking-drawer").detach();
            mobileDraw.insertAfter(target);
        }
    },

    //Set the start and end point of the row that the drawer should open between
    setRow: function (target) {
        var that = this;
        var container = target.closest(".matrix-slider-block");
        var rowLength = this.settings.rowLength;
        var index = container.find(".matrix-slider-item").index(target);

        if( index + 1 <= rowLength ) {
            that.settings.insertStart = 0;
            that.settings.insertEnd = rowLength;
        } else if ( index + 1 > rowLength && index + 1 < rowLength * 2 + 1) {
            that.settings.insertStart = rowLength;
            that.settings.insertEnd = rowLength * 2 ;
        } else if ( index + 1 >= rowLength * 2 ) {
            that.settings.insertStart = rowLength * 2;
            that.settings.insertEnd = rowLength * 3;
        }

        // Add all items in the row to an array then animate
        that.settings.rowItems = [];
        for (var n = 0; n < $(".matrix-slider-block").length; n ++) {
            for (var i = that.settings.insertStart; i < that.settings.insertEnd; i ++) {
                that.settings.rowItems.push($(".matrix-slider-block").eq(n).find(".matrix-slider-item").eq(i));
            }
        }
    },

    setItemMargins: function () {
        // Add margins
        for (var i = 0; i < this.settings.rowItems.length; i ++) {
            this.settings.rowItems[i].css({"margin-bottom": $(".booking-drawer").height() + 80 + "px"});
        }
    },

    hideDrawer: function (slide) {
        $(document).off("click.drawer");
        if ( slide === false ) {
            $(".booking-drawer").remove();
        } else {
            $(".booking-drawer").slideUp(300, function(){
                $(".booking-drawer").remove();
            });
            setTimeout(function () {
                $(".matrix-slider-item").css({"margin-bottom": ""});
            }, 50);

        }
        this.settings.hasDrawer = false;
    },


    initDrawerEvents: function () {
        var that = this;

        $(".booking-drawer .close").off("click").on("click", function () {
            that.hideDrawer();
        });

        $("#show-booking-form").off("click").on("click", function (event) {
            that.showBookingForm(event);
        });

        $(".add-to-cart").off("click").on("click", function (event) {
            that.addToCart(event);
        });

        $(".hide-booking-form").off("click").on("click", function () {
            that.hideBookingForm(false);
        });

        //Hide drawer if trying to use the slider but the drawer is open
        //Timeout for browser click delay bug
        setTimeout(function () {
            $(document).off("click.drawer").on("click.drawer", function (e) {
                // - If there is a booking drawer in the DOM
                // && You're not clicking inside the booking drawer
                // && The drawer isnt already opening
                if ( $(".booking-drawer").length > 0  && $(e.target).closest(".booking-drawer").length < 1 && that.openingDrawer !== true ) {
                    that.hideDrawer();
                    return false;
                }
            });
        }, 20);
    },

    positionDrawer: function () {
        var drawer = this.settings.drawer;

        // Set top position based on first items height;
        // If mobile then just the first item, if desktop then first item of each slide block
        var selector = (Modernizr.mq('(max-width: 640px)') ? ".matrix-slider-item" : ".matrix-slider-block .matrix-slider-item");
        var top = $(selector).eq(this.settings.insertStart).position().top;
        var height = $(selector).eq(this.settings.insertStart).height() + 50;
        var offset = top + height;

        //Set the margin left to align with window;
        var left = $(".matrix-slider").offset().left * -1;

        drawer.css({
            "top": offset + "px",
            "width": $(window).width(),
            "margin-left": left + "px"
        });
    },

    positionTriangle: function (target) {
        var position = target.position().left + target.width() / 2;
        $(".booking-drawer-arrow").css({"left": position});
    },

    showBookingForm: function (e) {
        //Quickly unhide the form to get its height, then set margins  accordinly. Then show the form
        $(".booking-drawer-form").css({"display": "block","opacity": 0});
        this.setItemMargins();
        $(".booking-drawer-form").css({"display": "none","opacity": 1});

        $(".booking-drawer-form").slideDown();

        $("#show-booking-form").hide();
    },

    hideBookingForm: function () {
        $(".booking-drawer-form").hide();
        this.setItemMargins();
        $("#show-booking-form").show();

    },

    addToCart: function (e) { //show booking must activate here
        var target = $(e.target).attr("data-id");
        moviesCart.createMovieCartItem(target);
        this.hideDrawer();
    }
};

// -----------------------------------------//
//     Nu metro Cart                        //
//------------------------------------------//

var moviesCart = {

    MovieCartID: null,

    MoviesList: [],

    CurrentMovie: "",
    PriceIndex: [],
    cartTotal: 0,

    tempTicketNumberSelect: null,
    isLoadingSeatLayout: false,
    mustCancelSeatLayout: false,

    initCart: function () {
        this.initEvents();
    },

    initEvents: function () {
        var that = this;

        // remove the movie from cart
        $(".movie-cart-item > .remove-btn").off("click").on("click", function(e) {
            that.removeMovie($(this));
        });

        // populate selected cinema in the cart
        $(".movie-cart-item").off("change.cinema-select").on("change.cinema-select", ".cart-cinema-select select", function () {
            //that.checkCancelSeatSelector();
            var movieID = $(this).attr("id").replace("cart-cinema-select-", "");
            var index = that.findMovieIndex(movieID);
            var cinemaID = $(this).val();

            
            //that.checkCancelSeatSelector();
            that.showPreloader(movieID, "date");
            that.saveCinema(movieID, cinemaID);
            that.initDatepicker(cinemaID, movieID);

            that.clearCartDetails(movieID, ["cinemaID", "number", "date", "seats"]);
            that.showHideCartItemDetails(movieID);
        });

        // populate selected time in the cart
        $(".movie-cart-item").off("change.date-select").on("change.date-select", ".cart-date-select input", function () {
            //that.checkCancelSeatSelector();
            if ($(this).val() === "") return;
            var movieID = $(this).attr("id").replace("datepicker-", "");

            that.clearCartDetails(movieID, ["number", "seats"]);
            that.saveTime(movieID, $(this).val());
            that.showHideCartItemDetails(movieID);
        });

        $(".cart-ticket-number select").on("blur", function () {
            var movieID = $(this).closest('.movie-cart-item').attr('data-incentiv-movie-booking-id');
            if ($(this).val() === 0) {
                that.clearCartDetails(movieID, ["seats"]);
                //that.saveNumberOfTickets(movieID, 0);
                that.showHideCartItemDetails(movieID);
            }
        });

        $(".movie-cart-item").off("change.number-select").on("change.number-select", ".cart-ticket-number select", function () {
            //that.checkCancelSeatSelector();
            var movieTicketNo =  $(this).val();
            var movieID = $(this).attr("id").replace("cart-ticket-number-", "");
            var index = that.findMovieIndex(movieID);    

            
            if (movieTicketNo === 0) {
                //that.saveNumberOfTickets(movieID, 0);
                that.clearCartDetails(movieID, ["seats"]);
                that.showHideCartItemDetails(movieID);
                return;
            }
            
            that.showPreloader(movieID, "seats");
            that.clearCartDetails(movieID, ["seats"]);
            that.showHideCartItemDetails(movieID);
            that.fetchSeats(movieID, movieTicketNo);
        });

        $(".movie-cart-item").on("click", ".seats-selected span", function () {
            var movieID = $(this).closest('.movie-cart-item').attr('data-incentiv-movie-booking-id');
            var index = that.findMovieIndex(movieID);
            if (that.MoviesList[index].numberOfTickets === 0) return;
            that.showSeatingModal(movieID);
        });

        //this gets called when it is time to check out
        $('#payment-button').off("click").on("click", function () { 
            //Make sure the button is first active
            moviesCart.checkOut();
            return false;
        });


    },

    /**
     * Works out the total cost of the cart.
     *
     * @returns {number}
     */
    totalCost: function () {
        var that = this,
            total = 0,
            savedTotal = 0, // savedTotal is used for all the savedbooking seats
            i=0;

        for(i; i < that.MoviesList.length; ++i) {
            if(that.MoviesList[i].SavedSeats && parseFloat(that.MoviesList[i].SavedSeats.TotalValueCents) > 0) {
                savedTotal += parseFloat( that.MoviesList[i].SavedSeats.TotalValueCents / 100);
            }
            if(that.MoviesList[i].Booking && parseFloat(that.MoviesList[i].Booking.Amount) > 0) {
                total += that.MoviesList[i].Booking.Amount;
            }
        }

        return total;

        /*
        if(total === savedTotal) {
            if(DEBUG) console.log("Cart Total: R" + savedTotal);
            return savedTotal;

        }
        */
    },

    /**
     * Checkout Cart function
     */
    checkOut: function () {
        var that = this,
            BookingCartID = that.MovieCartID;

        return $.ajax('/wp-admin/admin-ajax.php', {
                type: 'POST',
                data: { 
                    action: 'get_nu_metro_booking', 
                    booking_id: BookingCartID
                },
                dataType: 'json' 
            }).done(function (data) {
                if(data.TotalPayable && parseFloat(data.TotalPayable) > 0) {
                    that.cartTotal = parseFloat(data.TotalPayable);

                    $('#MovieVcsForm input[name=m_1]').val(that.MovieCartID);
                    $('#MovieVcsForm input[name=p4]').val(that.cartTotal);

                    $('#vcssubmit').trigger('click'); //send to vcs after change is made
                } else {
                    // Error - Empty cart message
                    if(that.MovieCartID === null || that.MoviesList.length === 0) {
                        modal({
                            "successButton": "Continue",
                            "title": "Please choose a movie",
                            "content": "It looks like you haven\'t chosen any movies. If you believe this is incorrect or the problem persists please refresh your browser or contact our call centre on <strong>0860 732 5489</strong>."
                        });
                    } else {
                        modal({
                            "successButton": "Continue",
                            "title": "Unable to complete booking",
                            "content": "An error has occurred and we were unable to complete your booking. Please make sure all movie fields are filled out correctly. If the problem persists please refresh your browser or contact our call centre on <strong>0860 732 5489</strong>."
                        });
                    }

                    return false;
                }
              
            }).fail(function () {

            });
        
    },

    findTrailer: function (hopk) {
        var details = this.getNuMetroDetails(hopk);
        if (DEBUG) console.log("numetro details: ", details);
    },

    /**
     * Adds a selected movie to the Movie Shopping Cart, then you can choose your cinema, datetime and seats
     * @param targetID string The movie HOP id we are adding to the cart - looks like "A00004179"
     */
    createMovieCartItem: function (targetID) { // targetID looks like the Movie's HOP
        var that = this;

        // Fetch Cart Details
        var target = $("#" + targetID); //A00004179
        var targetCode = target.attr("data-id");
        var movieID = target.attr("data-incentiv-id");

        //Save movie details in memory and return is movie already exists
        var addMovieToList = that.addMovieToList(movieID, targetCode);
        if (addMovieToList === false) return;

        if(that.MovieCartID === null) {
            $.when(that.saveBooking())
                .fail(function (e){  
                    modal({
                        "successButton": "Try Again",
                        "failButton": "Cancel",
                        "title": "Oops, error!",
                        "content": "We were unable to start the booking process. If the problem persists refresh your browser or contact our call centre on <strong>0860 732 5489</strong>.",
                        onSuccess: function () {
                            that.createMovieCartItem(targetID);
                        }
                    });
                })
                .done(function (b) {
                    if (b.BookingID) {
                        that.MovieCartID = b.BookingID;
                    } else {
                        that.MovieCartID = b;
                    }
                    
                    that.appendMovie(targetID);
                });
        } else {
            that.appendMovie(targetID);
        }

    },

    appendMovie: function (targetID) {
        var that = this;

        // Fetch Cart Details
        var target = $("#" + targetID); //A00004179
        var targetImage = target.find(".image img").attr("src");
        var targetTitle = target.attr("data-title");
        var targetCode = target.attr("data-id");
        var movieID = target.attr("data-incentiv-id");

        // Create Cart Item
        var cartItem = $("<div class='movie-cart-item' id='cart-item-code-" + movieID + "'' />");
        cartItem.html($("#movie-cart-item").html());

        // Insert movie into cart item
        cartItem.attr("data-incentiv-movie-booking-id", movieID);
        cartItem.find(".poster img").attr("src", targetImage);
        cartItem.find(".movie-title").html(targetTitle);
        cartItem.find("input.code").val(targetCode);
        cartItem.find(".cart-cinema-select select").attr("id", "cart-cinema-select-" + movieID);
        cartItem.find(".cart-ticket-number select").attr("id", "cart-ticket-number-" + movieID);
        cartItem.find(".datepicker").attr("id", "datepicker-" + movieID);

        $(".movie-cart-list").append( cartItem );

        that.initCinemaSelector(movieID);

        that.initEvents();
        that.showCart();

        that.showHideTotalPrice();
    },

    resetMovieTimeout: function () {
        var that = this, i=0;

        for (i; i < that.MoviesList.length; ++i) {
            if(that.MoviesList[i].SavedSeats && that.MoviesList[i].Booking) {

                $.when(that.resetMovie(that.MoviesList[i].Booking.MovieBookingID))
                    .done(function (data) {
                        if(DEBUG) console.log("Success for Movie RESET: ", data);
                    })
                    .fail(function (data) {
                        if(DEBUG) console.log("Failure for Movie RESET: ", data);
                    });    
            }
            
        }
    },

    findMovieIndex: function (movieID) {
        for (var i = 0; i < this.MoviesList.length; i ++) {
            if (this.MoviesList[i].movieID === movieID) {
                return i;
            }
        }
        return -1;
    },

    addMovieToList: function (movieID, targetCode) {
        if (this.findMovieIndex(movieID) < 0) {
            this.MoviesList.push({
                "movieID": movieID, 
                "targetCode": targetCode,
                "cinemaTime": null,
                "cinemaID": null,
                "numberOfTickets": null,
                "seats": null,
                "Seats": [],
                "MovieCinemaSessions": null
            });
            return true;

        } else {
            return false;
        }
    },

    removeFromMovieList: function (movieID) {
        var index = this.findMovieIndex(movieID);
        this.MoviesList.splice(index, 1);
    },

    saveCinema: function (movieID, cinemaID) {
        var index = this.findMovieIndex(movieID);
        this.MoviesList[index].cinemaID = cinemaID;
    },

    saveTime: function (movieID, cinemaTime) {
        var index = this.findMovieIndex(movieID);
        this.MoviesList[index].cinemaTime = cinemaTime;
    },

    saveNumberOfTickets: function (movieID, numberOfTickets) {
        var index = this.findMovieIndex(movieID);
        this.MoviesList[index].numberOfTickets = parseInt(numberOfTickets);
    },

    saveSeats: function (movieID, seats) {
        var index = this.findMovieIndex(movieID);
        this.MoviesList[index].seats = seats;
    },

    showCart: function () {
        var hide = ($(".movie-cart-row").css('display') == 'none') ? true : false;

        $(".movie-cart-row").show();
        var offset = $(".movie-cart-wrapper").offset().top - 80;

        if (hide) {
            $(".movie-cart-row").hide();
        }

        $("html, body").animate({scrollTop: offset}, 600, function () {
            $(".movie-cart-row").slideDown(600);
        });
    },

    removeMovie: function (target) {
        var that = this;
        var movieID;
        var bid = this.getBooking();

        if (typeof target === "string") {
            movieID = target;
        } else {
            movieID = $(target).closest('.movie-cart-item').attr('data-incentiv-movie-booking-id');
        }

        var cartItem = $("#cart-item-code-" + movieID);
        
        cartItem.hide();

        $.when(this.removeMovieFromBooking(bid, movieID)).done(function() {
            that.removeFromMovieList(movieID);
            cartItem.remove();
            that.deleteSeatingModal(movieID);
            that.showHideTotalPrice();

            that.resetMovieTimeout(); //reset all the other movies in the cart

        }).fail(function(){
            cartItem.show();
            var title = cartItem.find(".movie-title").text();
            if (title.indexOf(", The") > -1) {
                title = "The " + title.replace(", The", "");
            }
            
            modal({
                "successButton": "Continue",
                "title": "Error",
                "content": "Something went wrong while trying to remove \"" + title + "\" from your cart. If the problem persists try refreshing the page."
            });
        });
    },


    initCinemaSelector: function(movieID) {
        var that = this;
        var target = $("#" + "cart-cinema-select-" + movieID);
        var value = $("#movies-matrix-cinema").val();

        that.showPreloader(movieID, "cinema");

        if(target && !target.hasClass('cinema-selector-initiated')) {
            $.when(that.getCinemas(movieID))
                .done(function(cinemas) {   // returns a list of cinemas
                    var cines = cinemas;

                    if(cines.length > 0) {
                        cines = cinemas.sort(function(a, b) {
                            return a.MovieCinemaName == b.MovieCinemaName ? 0 : a.MovieCinemaName < b.MovieCinemaName ? -1 : 1;
                        });
                    }
                    
                    //Create cinema select box
                    target.append('<option disabled selected value="disabled">--</option>');
                    for(var j = 0; j < cines.length; j++){
                        var selected = (value !== null && value === cines[j].MovieCinemaID) ? 'selected' : '';
                        target.append('<option value="' + cines[j].MovieCinemaID + '" ' + selected + '>' + cines[j].MovieCinemaName + '</option>');
                    }


                    target.addClass("cinema-selector-initiated");
                    that.hidePreloader(movieID);
                    that.showHideCartItemDetails(movieID);

                //Fail
                }).fail(function () {
                    that.hidePreloader(movieID);
                    modal({
                        "successButton": "Try Again",
                        "failButton": "Cancel",
                        "title": "Error",
                        "content": "We were unable to fetch the cinemas available for this movie. If the problem persists refresh your browser or contact our call centre on <strong>0860 732 5489</strong>.",
                        onSuccess: function () {
                            that.initCinemaSelector(movieID);
                        }
                    });
                });

            if (value !== null) {
                that.hidePreloader(movieID);
                that.initDatepicker(value, movieID);
            }
            
        }
    },

    initDatepicker: function(cineID, movieID){
        var that = this;
        var target = $("#" + "datepicker-" + movieID);
        var container = $(".movie-cart-item[data-incentiv-movie-booking-id='" + movieID + "']");

        if(target && !target.hasClass('datepicker-initiated')) {
            $.when(that.getCinemaSessions(cineID, movieID))
                .done(function(sessions) {  
                    /*
                        NB: Need a solution to check if cinemas are available before selecting it and remove them from the list
                    */

                    //If no sessions. Remove datetime picker
                    target.val("");
                    if (sessions.length < 1) {
                        var input = target.clone();
                        var parent = target.parent();
                        target.remove();

                        input.val("There are no shows available.");
                        parent.append(input);

                        container.find(".cinema-selector-initiated option[value=" + cineID + "]").attr("disabled", true).parent().val("disabled");
                        return;
                    }


                    var index = that.findMovieIndex(movieID);
                    that.MoviesList[index].MovieCinemaSessions = sessions;
                    that.MoviesList[index].cinemaID = cineID;

                    that.hidePreloader(movieID);
                    that.showHideCartItemDetails(movieID);

                    var dates = _.map(sessions, function(session){
                        var m = null;
                        if(moment && session.SessionDateTime && session.MovieCinemaSessionID) {
                            m = moment(session.SessionDateTime);

                            return {
                                date: m.format('YYYY/MM/DD'),
                                time: m.format('HH:mm'),
                                unix: m.valueOf(),
                                id: session.MovieCinemaSessionID,
                                cine: cineID,
                                movie: movieID
                            };
                        } else {
                            return false;
                        }
                    });

                    if(false !== dates) {
                        var min = _.min(dates, function(d) { return d.unix; });
                        var max = _.max(dates, function(d) { return d.unix; });

                        dates.sort(function(a, b){
                            return (new Date(a.date) - new Date(b.date));
                        });

                        //Fix weird bug when adding a movie thats already in the cart 
                        //even though it doesnt add it if its there already :/
                        target.datetimepicker('destroy');

                        var allowTimes =_.pluck(_.filter(dates, function(s){return s.date == dates[0].date}), 'time');

                        target.datetimepicker({
                            minDate: min.date,
                            maxDate: max.date,
                            timepicker: true,
                            defaultDate: dates[0].date,
                            defaultTime: dates[0].time,
                            allowTimes: that.sortTimes(allowTimes),
                            allowBlank: true,
                            closeOnDateSelect: false,
                            format: 'Y/m/d  H:i',
                            onChangeDateTime: function(d){
                                if (d === null) return false;

                                var sessions = _.filter(dates, function(s){
                                    return s.date == d.dateFormat('Y/m/d');
                                });
                                this.setOptions({
                                    allowTimes: that.sortTimes(_.pluck(sessions, 'time'))
                                });
                                var session = _.find(sessions, function(s){
                                    return s.time == d.dateFormat('H:i');
                                });
                                if(session){
                                    var booking = container.attr('data-incentiv-movie-booking-id');
                                    that.requestTickets(booking, session);
                                }

                                // Clear number of tickets and seats
                                that.clearCartDetails(movieID, ["number", "seats"]);
                                that.showHideCartItemDetails(movieID);
                                that.hidePreloader(movieID);
                            }
                        });
                    }
                })
                .fail(function () {
                    container.find(".cart-date-select").addClass("show");
                    container.find(".preloader").hide();

                    var input = target.clone();
                    var parent = target.parent();
                    target.remove();

                    input.val("There are no shows available.");
                    parent.append(input);

                    container.find(".cinema-selector-initiated option[value=" + cineID + "]").attr("disabled", true).parent().val("disabled");
                });
        }
    },

    sortTimes: function (array) {
        return array.sort(function (a, b) {
            if (parseInt(a.split(":")[0]) - parseInt(b.split(":")[0]) === 0) {
                return parseInt(a.split(":")[1]) - parseInt(b.split(":")[1]);
            } else {
                return parseInt(a.split(":")[0]) - parseInt(b.split(":")[0]);
            }
        })
    },

    requestTickets: function(booking, session) {
        var that = this;
        $.when(that.getTicketTypes(session.id))
            .done(function(data){
                var ticketType = _.find(data, function(tt){
                    return tt.TicketTypeCode == '0001'; // standard ticket
                });

                if(ticketType){
                    if(DEBUG) console.log(JSON.stringify(ticketType));
                }
            })
            .fail(function(err){
                if(DEBUG) console.log("Session Error: " + err);
                modal({
                    "successButton": "Try Again",
                    "failButton": "Cancel",
                    "title": "Oops, error!",
                    "content": "We were unable to fetch the session details. If the problem persists refresh your browser or contact our call centre on <strong>0860 732 5489</strong>.",
                    onSuccess: function () {
                        that.requestTickets(booking, session);
                    }
                });
            });
    },

    fetchSeats: function (movieID, movieTicketNo) {
        var that = this,
            cinemaID = _.findWhere(that.MoviesList, { movieID: movieID }).cinemaID,
            showTime = _.findWhere(that.MoviesList, { movieID: movieID }).cinemaTime,
            sessionID = '',
            index = that.findMovieIndex(movieID),
            CinemaSession = that.MoviesList[index].MovieCinemaSessions,
            container = $(".movie-cart-item[data-incentiv-movie-booking-id='" + movieID + "']");

        this.isLoadingSeatLayout = true;

        if(_.findWhere(CinemaSession, { SessionDateTime: moment(showTime).format('YYYY-MM-DD[T]HH:mm:ss') } )) {
            sessionID = _.findWhere(CinemaSession, { SessionDateTime: moment(showTime).format('YYYY-MM-DD[T]HH:mm:ss') }).MovieCinemaSessionID;
        }

        that.saveNumberOfTickets(movieID, movieTicketNo);

        that.showPreloader(movieID, "seats");

        //add movie to booking
        $.when(that.addMovieToBooking(that.MovieCartID, movieID, movieTicketNo, cinemaID, sessionID))
            .done(function (data){
                // if (that.mustCancelSeatLayout === true) {
                //     that.mustCancelSeatLayout = false;
                //     that.isLoadingSeatLayout = false;
                //     return;
                // }
                var index = that.findMovieIndex(movieID);
                var numberOfTickets = that.MoviesList[index].numberOfTickets;
                that.MoviesList[index].Booking = data.Booking;
                that.initSeatSelector(movieID, data, numberOfTickets);
                that.isLoadingSeatLayout = false;
                that.showHideCartItemDetails(movieID);
                that.hidePreloader(movieID);
                
                that.resetMovieTimeout(); //resets the timeouts for all movies
            })
            .fail(function (error) { // not getting seating back for this movie
                if(DEBUG) console.log("Seats Error: ", error);
                var response = JSON.parse(error.responseText);
                that.hidePreloader(movieID);
                that.clearCartDetails(movieID, ["number", "seats"]);

                if (response.Message === "No standard tickets are available") {
                    modal({
                        "successButton": "Continue",
                        "title": "No tickets available",
                        "content": "There are no standard tickets available for this session. Try another date, time, or cinema."
                    });
                } else if (response === "This booking cannot be made because the benefit has been exchausted.") {
                    var message;
                    if (movieTicketNo > 1) {
                        message = "We were unable to create the booking for the selected number of tickets. Please reduce the amount of tickets and try again.";
                    } else {
                        message = "You have reached your monthly limit for booking movies. Please try again next month.";
                    }
                    modal({
                        "successButton": "Continue",
                        "title": "Benefit exhausted",
                        "content": message
                    });
                } else{
                    modal({
                        "successButton": "Try Again",
                        "failButton": "Cancel",
                        "title": "Oops, error!",
                        "content": "We were unable to fetch the available seats for this movie. If the problem persists please refresh your browser or contact our call centre on <strong>0860 732 5489</strong>.",
                        onSuccess: function () {
                            that.fetchSeats(movieID, movieTicketNo);
                        },
                        onFail: function () {
                            //that.removeMovie(movieID);
                        }
                    });
                }

            });
    },

    // Check whether or not the seat layout is loading and should be ignored now or not.
    // If layout is loading a user changes cinema or time or number of seats.
    checkCancelSeatSelector: function () {
        if (this.isLoadingSeatLayout === true) {
            this.mustCancelSeatLayout = true;
        }
    },


    // Run this function before and after any data changes.
    // This should show / hide the preloaders and booking details
    showHideCartItemDetails: function (movieID) {
        var container = $(".movie-cart-item[data-incentiv-movie-booking-id='" + movieID + "']");
        var cinemaSelect = container.find(".cinema-selected");
        var dateSelect = container.find(".cart-date-select");
        var numberSelect = container.find(".cart-ticket-number");
        var seatSelect = container.find(".seats-selected");
        var itemPrice = container.find(".price");
        var index = this.findMovieIndex(movieID);
        var bookingData = this.MoviesList[index];

        
        if (cinemaSelect.find("select").children().length > 0) {
            cinemaSelect.addClass("show");
        } else {
            dateSelect.removeClass("show");
        }


        if (cinemaSelect.find("select").val() !== null && 
            cinemaSelect.find("select").val() !== "disabled" && 
            bookingData.MovieCinemaSessions !== null &&
            bookingData.cinemaID !== null) {

            dateSelect.addClass("show");

        } else {
            dateSelect.find("input").val("");
            dateSelect.removeClass("show");
        }


        if (dateSelect.find("input").val() !== "" && 
            bookingData.cinemaTime !== null) {
            numberSelect.addClass("show");

        } else {
            numberSelect.removeClass("show");
        }


        if (numberSelect.find("select").val() !== 0 && 
            numberSelect.find("select").val() !== null && 
            bookingData.numberOfTickets !== null && 
            bookingData.cinemaData !== null) {

            seatSelect.addClass("show");

        } else {
            seatSelect.removeClass("show");
        }


        if (seatSelect.find("span").text() !== "--" && 
            bookingData.Seats.length > 0) {

            itemPrice.addClass("show");

        } else {
            itemPrice.removeClass("show");
        }

        this.showHideTotalPrice();
    },

    // checks to see if all MovieBookings have had their seats saved.
    showHideTotalPrice: function () {
        var fullBooking = true;

        if(this.MoviesList.length < 1) {
            $('div.continue-btn').fadeOut();
            return;
        }

        for (var i=0; i < this.MoviesList.length; ++i) { 
            if (!this.MoviesList[i].SavedSeats) {
                fullBooking = false;
            }
        }

        if (fullBooking) {
            var total = 0;
            for (var i = 0; i < this.MoviesList.length; i ++) {
                total += parseFloat(this.MoviesList[i].Booking.Amount);
            }
            $(".total-price").text("Total: R" + total);
            $('div.continue-btn').slideDown();
        
        } else {
            $('div.continue-btn').fadeOut();
        }
    },

    clearCartDetails: function (movieID, detailsArray) {
        var container = $(".movie-cart-item[data-incentiv-movie-booking-id='" + movieID + "']");
        var cinemaSelect = container.find(".cinema-selected");
        var dateSelect = container.find(".cart-date-select");
        var numberSelect = container.find(".cart-ticket-number");
        var seatSelect = container.find(".seats-selected");
        var index = this.findMovieIndex(movieID);

        for (var i = 0; i < detailsArray.length; i ++) {
            switch (detailsArray[i]) {
                case "cinema":
                    cinemaSelect.find("select").val("disabled");
                    this.MoviesList[index].cinemaID = null;
                    break;
                case "cinemaID":
                    this.MoviesList[index].cinemaID = null;
                    break;
                case "date":
                    dateSelect.find("input").val("");
                    this.MoviesList[index].cinemaTime = null;
                    break;
                case "number":
                    numberSelect.find("select").val(0);
                    this.MoviesList[index].numberOfTickets = 0;
                    break;
                case "seats":
                    seatSelect.find("span").text("--");
                    this.MoviesList[index].seats = null;
                    this.MoviesList[index].Seats = [];
                    this.MoviesList[index].SavedSeats = false;
                    this.MoviesList[index].cinemaData = null;
                    break;
                default:
                    break;
            }
        }
    },

    showPreloader: function (movieID, target) {
        var container = $(".movie-cart-item[data-incentiv-movie-booking-id='" + movieID + "']");
        var preloader = container.find(".preloader");
        var position = null;

        switch (target) {
            case "cinema":
                var item = container.find(".cart-cinema-select");
                item.show();
                position = item.position();
                item.hide().css({"display": ""});
                break;
            case "date":
                var item = container.find(".cart-date-select");
                item.show();
                position = item.position();
                item.hide().css({"display": ""});
                break;
            case "number":
                var item = container.find(".cart-ticket-number");
                item.show();
                position = item.position();
                item.hide().css({"display": ""});
                break;
            case "seats":
                var item = container.find(".seats-selected");
                item.show();
                position = item.position();
                item.hide().css({"display": ""});
                break;
            case "price":
                var item = container.find(".price");
                item.show();
                position = item.position();
                item.hide().css({"display": ""});
                break;
            default:
                break;
        }

        preloader.css({
            left: position.left,
            top: position.top
        });

        preloader.addClass("show").fadeIn();
        
    },

    hidePreloader: function (movieID) {
        var container = $(".movie-cart-item[data-incentiv-movie-booking-id='" + movieID + "']");
        var preloader = container.find(".preloader");
        preloader.removeClass("show");
    },

    /* **********************************
        Seat Selector functions
    ********************************** */

    seatOptions: {
        seatWidth: 20,
        seatWidthMobile: 45,
    },

    setOptions: function (movieID, cinemaData, numberOfTickets) {
        var index = this.findMovieIndex(movieID);
        this.MoviesList[index].cinemaData = {
            seats: cinemaData.Seating,
            numberOfTickets: numberOfTickets,
            width: cinemaData.Seating.Areas[0].ColumnCount,
            height: cinemaData.Seating.Areas[0].RowCount
        };

        return this.MoviesList[index].cinemaData;
    },

    initSeatSelector: function (movieID, cinemaData, numberOfTickets) {
        var that = this;

        var cinemaData = this.setOptions(movieID, cinemaData, numberOfTickets);

        //Create modal
        if ($(".seating-modal[data-incentiv-movie-booking-id=" + movieID + "]").length < 1) {
            var modal = $("<div />").addClass("seating-modal").attr("data-incentiv-movie-booking-id", movieID);
            $("body").append(modal);
        } else {
            var modal = $(".seating-modal[data-incentiv-movie-booking-id=" + movieID + "]");
        }
        
        


        //On click open seat
        $(".seating-modal").off("click.seats").on("click.seats", ".seat-layout .circle:not(.selected):not(.disabled)", function () {
            clearTimeout($.data(document.body, 'hoverTimer'));
            that.selectSeat($(this));
        });

        //On click selected seat
        $(".seating-modal").on("click.seats", ".seat-layout .circle.selected", function () {
            that.unselectSeat($(this));
        });

        //On mouseover open seat
        $(".seating-modal").off("mouseenter.seats").on("mouseenter.seats", ".seat-layout circle:not(.selected):not(.disabled)", function () {
            clearTimeout($.data(document.body, 'hoverTimer'));
            that.onHoverYourSeatsList($(this));
        });

        $(".seating-modal").off("mouseleave.seats").on("mouseleave.seats", ".seat-layout circle:not(.selected):not(.disabled)", function () {
            var target = this;
            clearTimeout($.data(document.body, 'hoverTimer'));
            $.data(document.body, 'hoverTimer', setTimeout(function() {
                that.offHoverYourSeatsList($(target));
            }, 500));
        });

        $(".seating-modal").off("click.complete-booking").on("click.complete-booking", ".complete-booking", function () {
            if($(this).attr("disabled")) return;
            that.completeBooking($(this));
        });

        $(".seating-modal").off("click.mobile-row").on("click.mobile-row", ".seats-mobile-row-title", function () {
            that.toggleRow($(this));
        });

        $(".seating-modal").off("click.cancel-seats").on("click.cancel-seats", ".cancel-seats", function () {
            that.cancelSeatingModal();
        });



        //Insert Seat Selector
        $(".seating-modal[data-incentiv-movie-booking-id='" + movieID + "']").html(
            this.createSeatSelection(cinemaData)
        );

        //Create Sliders
        this.createMobileSliders();

        //Resize width to fix IE
        if ($(".seat-layout").hasClass("desktop")) {
            $(window).off("resize.seat-layout").on("resize.seat-layout", function () {
                clearTimeout($.data(this, 'seatResizeTimer'));
                $.data(this, 'seatResizeTimer', setTimeout(function() {
                    that.resizeSeatingModals(that);
                }, 200));
            });
            $(window).resize();
        }


        //this.showSeatingModal(movieID);


    },

    createSeatSelection: function (cinemaData) {
        var platform = (Modernizr.mq('(max-width: 640px)')) ? "mobile" : "desktop";

        var html = "";

        html += '<h1>Select your seats</h1>';
        html += '<div class="seat-selector-container">';

        html +=     '<div class="selected-seats">';
        html +=         '<span class="your-seats-title">Your Seats</span>';
        html +=         '<ul class="selected-seats-list">';
        html +=             this.createSelectedSeats(cinemaData);
        html +=         '</ul>';
        html +=         '<a class="complete-booking button small" disabled="disabled">Save Seats</a>';
        html +=         '<a class="cancel-seats button small grey" >Cancel</a>';
        html +=     '</div>';

        html +=     '<div class="seat-layout ' + platform + '" style="overflow: hidden; position: relative;">';
        html +=         '<p class="screen-title">SCREEN</p>';
        html +=          (Modernizr.mq('(max-width: 640px)')) ? this.createMobileSeatLayout(cinemaData) : this.createSeatLayout(cinemaData);
        html +=         '<ul class="seating-info">';
        html +=             '<li>Free</li>';
        html +=             '<li>Taken</li>';
        html +=             '<li>Selected</li>';
        html +=             '<li>Wheelchair</li>';
        html +=         '</ul>';
        html +=     '</div>';

        html += '</div>';

        return html;

    },

    getScreen: function (cinemaData) {
        var width = ((this.seatOptions.seatWidth * cinemaData.width) + 20);
        var rect = '<rect width="' + width + '" height="7" style="fill:#333e48;" />';
        return rect;
    },

    createSelectedSeats: function (cinemaData) {
        var html = "";
        for (var i = 0; i < cinemaData.numberOfTickets; i ++) {
            html += '<li>-</li>';
        }
        return html;
    },

    createSeatLayout: function (cinemaData) {
        var elements = "";

        elements +=  '<svg preserveAspectRatio="xMidYMin" style="transform: matrix(1, 0, 0, 1, 0, 0);" viewBox="0, 0, ' +  ((this.seatOptions.seatWidth * cinemaData.width) + 20) + ', ' + ((this.seatOptions.seatWidth * cinemaData.height) + 20) + '">';
        elements +=   this.getScreen(cinemaData);

        //Cycle through areas
        var Areas = cinemaData.seats.Areas;
        for (var i = 0; i < Areas.length; i ++) {
            var AreaCategoryCode = Areas[i].AreaCategoryCode;
            var AreaNumber = Areas[i].Number;

            //Cycle through rows
            var rows = Areas[i].Rows;
            for (var k = 0; k < rows.length; k ++) {
                var row = rows[k].PhysicalName;
                

                //Cycle through seats
                var seats = rows[k].Seats;
                for (var n = 0; n < seats.length; n ++) {
                    var SeatNumber = row + seats[n].SeatID;
                    var RowIndex = seats[n].SeatPosition.RowIndex;
                    var ColumnIndex = seats[n].SeatPosition.ColumnIndex;

                    // Crazy math because the seats in the JSON are in reverse
                    var left = (cinemaData.width * this.seatOptions.seatWidth) - (ColumnIndex * this.seatOptions.seatWidth);
                    var top = (cinemaData.height * this.seatOptions.seatWidth) - (RowIndex * this.seatOptions.seatWidth);
                    
                    var fill = "#c6cacd";
                    var type = "";

                    if (seats[n].Status === 1 || //booked
                        seats[n].Status === 2) //"M" ? (Still disabled)
                    type = "disabled";

                    if (seats[n].Status === 3) //Wheel chair
                    type = "wheelchair";

                    var seat =  '<g transform="translate(' + left + ', ' + top + ')">';
                    seat +=         '<circle cx="10" cy="10" r="8" class="' + type + ' circle seat-' + SeatNumber + '" fill="' + fill + '" data-rowindex="' + RowIndex + '" data-columnindex="' + ColumnIndex + '" data-seatnumber="' + SeatNumber + '" data-areacategorycode="' + AreaCategoryCode + '" data-areanumber="' + AreaNumber + '"></circle>';
                    seat +=     '</g>';

                    elements += seat;
                }   
            }
        }

        elements +=  '</svg>';

        return elements;
    },

    createMobileSeatLayout: function (cinemaData) {
        var elements = "";

        elements +=  '<svg preserveAspectRatio="xMidYMin" height="100%" width="100%" viewBox="0, 0, ' +  ((this.seatOptions.seatWidth * cinemaData.width) + 20) + ', 7">';
        elements +=     this.getScreen(cinemaData);
        elements +=  '</svg>';
        

        //Cycle through areas
        var Areas = cinemaData.seats.Areas;
        for (var i = 0; i < Areas.length; i ++) {
            var AreaCategoryCode = Areas[i].AreaCategoryCode;
            var AreaNumber = Areas[i].Number;

            //Cycle through rows
            var rows = Areas[i].Rows.reverse();
            for (var k = 0; k < rows.length; k ++) {
                var row = rows[k].PhysicalName;
                var seats = rows[k].Seats.reverse();

                if (seats.length < 1) continue;
                
                elements += "<div class='seats-mobile-row'>";
                elements +=     "<div class='seats-mobile-row-title'>Row " + row + "</div>";
                elements +=     "<div class='seats-mobile-row-slider swiper-container'>";
                elements +=         "<div class='swiper-wrapper'>";

                //Cycle through seats
                for (var n = 0; n < seats.length; n ++) {
                    var SeatNumber = row + seats[n].SeatID;
                    var RowIndex = seats[n].SeatPosition.RowIndex;
                    var ColumnIndex = seats[n].SeatPosition.ColumnIndex;

                    var fill = "#c6cacd";
                    var type = "";

                    if (seats[n].Status === 1 || //booked
                        seats[n].Status === 2) //"M" ? (Still disabled)
                    type = "disabled";

                    if (seats[n].Status === 3) //Wheel chair
                    type = "wheelchair";

                    var seat =  '<div class="mobile-seat-group swiper-slide" >';
                    seat +=         '<span class="text ' + type + ' seat-' + SeatNumber + '">' + seats[n].SeatID + '</span>';
                    seat +=         '<span class="circle ' + type + '" data-rowindex="' + RowIndex + '" data-columnindex="' + ColumnIndex + '" data-seatnumber="' + SeatNumber + '" data-areacategorycode="' + AreaCategoryCode + '" data-areanumber="' + AreaNumber + '"></span>';
                    seat +=     '</div>';

                    elements += seat;
                }  

                elements +=         "</div>";
                elements +=     "</div>";
                elements += "</div>";
            }
        }

        return elements;
    },

    createMobileSliders: function () {
        $(".seats-mobile-row-slider").each(function () {
            $(this).swiper({
                initialSlide: Math.floor($(this).find(".swiper-slide").length / 2),
                freeMode: true,
                freeModeFluid: true,
                centeredSlides: true,
                grabCursor: true,
                slidesPerView: "auto"
            });
        });
    },

    showSeatingModal: function (movieID) {
        var modal = $(".seating-modal[data-incentiv-movie-booking-id=" + movieID + "]");

        $("html").addClass("show-seat-modal");
        modal.addClass("active");

        /* Select seats that are stored in memory */
        var index = this.findMovieIndex(movieID);
        var seats = this.MoviesList[index].Seats;
        if (seats !== undefined && seats.length > 0) {
            for (var i = 0; i < seats.length; i ++) {
                var target = modal.find(".seat-" + seats[i].SeatNumber);
                this.selectSeat(target);
            }
            
        }
    },

    cancelSeatingModal: function () {
        var movieID = $(".seating-modal.active").attr("data-incentiv-movie-booking-id");
        var index = this.findMovieIndex(movieID);
        if (this.MoviesList[index].Seats.length === 0) {
            $(".seating-modal[data-incentiv-movie-booking-id=" + movieID + "] .seats-selected span strong").text("--");
        }
        

        $("html").removeClass("show-seat-modal");
        $(".seating-modal").removeClass("active");
    },

    deleteSeatingModal: function (movieID) {
        $(".seating-modal[data-incentiv-movie-booking-id=" + movieID + "]").remove();
    },

    // Need this for IE which struggles with sizing svg elements.
    resizeSeatingModals: function (that) {
        $(".seat-layout").each(function (i) {
            var movieID = $(this).closest(".seating-modal").attr("data-incentiv-movie-booking-id");
            var index = that.findMovieIndex(movieID);
            var cinemaData = that.MoviesList[index].cinemaData;

            var drawer = $(this).closest(".seating-modal");
            var shouldHide = !drawer.is(":visible");

            drawer.show();

            var w = $(this).width();
            var h = (cinemaData.height + 2) / cinemaData.width * w;
            h += $(".seating-info").eq(i).outerHeight();
            $(this).height(h);

            var container = $(this).closest(".seat-selector-container");
            var conH = container.find("h1").outerHeight() + h + 60;
            container.height(conH);

            if (shouldHide) drawer.hide();
        });
    },

    toggleRow: function (target) {
        if (target.next().hasClass("open")) {
            target.next().slideUp().removeClass("open");
            return;
        } else {
            $(".seats-mobile-row-slider.open").slideUp().removeClass("open");
            target.next().slideDown().addClass("open");
            this.createMobileSliders();
        }
       
    },

    selectSeat: function (target) {
        var movieID = target.closest(".seating-modal").attr("data-incentiv-movie-booking-id");
        var index = this.findMovieIndex(movieID);

        //If you've selected maximum number of tickets - don't select the seat
        // (Desktop and Mobile)
        if (this.MoviesList[index].numberOfTickets === target.closest(".seating-modal").find(".selected-seats-list li.selected").length) {
            return;
        }

        //Hide the continue button
        target.closest(".seating-modal").find(".complete-booking").attr("disabled", "disabled");

        this.addSvgClass(target, "selected");
        this.addSvgClass(target.prev(".text"), "selected");

        this.addToYourSeatsList(target);

        if (this.MoviesList[index].numberOfTickets === target.closest(".seating-modal").find(".selected-seats-list li.selected").length) {
            target.closest(".seating-modal").find(".complete-booking").removeAttr("disabled");
        }
    },

    unselectSeat: function (target) {
        this.removeSvgClass(target, "selected");
        this.removeSvgClass(target.prev(".text"), "selected");

        target.closest(".seating-modal").find(".complete-booking").attr("disabled", "disabled");
        this.removeFromYourSeatsList(target);
    },

    //Can't use jquery addClass on SVG elements. 
    addSvgClass: function (target, newClass) {
        //Check if target exists. (Mostly a fail safe for mobile / desktop layout discrepencies)
        if (target.length < 1) return;
        if (target.attr("class").match(newClass)) return;

        var oldClass = target.attr("class");
        var fullClass = oldClass + " " + newClass;
        target.attr("class", fullClass);
    },

    removeSvgClass: function (target, removeClass) {
        //Check if target exists. (Mostly a fail safe for mobile / desktop layout discrepencies)
        if (target.length < 1) return;

        var oldClass = target.attr("class");
        var fullClass = oldClass.replace(removeClass, "");
        target.attr("class", fullClass);
    },

    //Add purple dot to "Your Seats"
    addToYourSeatsList: function (target) {
        var movieID = target.closest(".seating-modal").attr("data-incentiv-movie-booking-id");
        var list = $(".seating-modal[data-incentiv-movie-booking-id='" + movieID + "'] .selected-seats-list li");
        var seatID = target.attr("data-seatnumber");

        list.not(".selected").eq(0).text(seatID).addClass("selected");

    },

    //Remove purple dot from "Your Seats"
    removeFromYourSeatsList: function (target) {
        var movieID = target.closest(".seating-modal").attr("data-incentiv-movie-booking-id");
        var list = $(".seating-modal[data-incentiv-movie-booking-id='" + movieID + "'] .selected-seats-list li");
        var seatID = target.attr("data-seatnumber");

        list.filter(".selected").last().text("-").removeClass("selected");
    },

    //When hovering on a dot - show the seatID in the "Your Seats" list
    onHoverYourSeatsList: function (target) {
        var movieID = target.closest(".seating-modal").attr("data-incentiv-movie-booking-id");
        var list = $(".seating-modal[data-incentiv-movie-booking-id='" + movieID + "'] .selected-seats-list li");
        var seatID = target.attr("data-seatnumber");

        list.not(".selected").eq(0).text(seatID);
    },

    //When hovering off of a dot - remove the seatID in the "Your Seats" list
    offHoverYourSeatsList: function (target) {
        var movieID = target.closest(".seating-modal").attr("data-incentiv-movie-booking-id");
        var list = $(".seating-modal[data-incentiv-movie-booking-id='" + movieID + "'] .selected-seats-list li");
        
        list.not(".selected").text("-");
    },

    completeBooking: function (target) {
        var movieID = target.closest(".seating-modal").attr("data-incentiv-movie-booking-id");
        var index = this.findMovieIndex(movieID);

        // Clear selected movies list
        this.MoviesList[index].Seats = [];

        // Save seats in memory
        var selectedTargets = target.closest(".seating-modal").find(".circle.selected");
        for (var i = 0; i < selectedTargets.length; i ++) {
            var seat = {
                AreaCategoryCode: selectedTargets.eq(i).attr("data-areacategorycode"),
                AreaNumber: selectedTargets.eq(i).attr("data-areanumber"),
                SeatNumber: selectedTargets.eq(i).attr("data-seatnumber"),
                RowIndex: selectedTargets.eq(i).attr("data-rowindex"),
                ColumnIndex: selectedTargets.eq(i).attr("data-columnindex")
            };
            this.MoviesList[index].Seats.push(seat);
        }
        

        var that = this,
            return_seats = {},
            finished_seats = this.MoviesList[index].Seats,
            saved_seats = {},
            booking = this.MoviesList[index].Booking;


        var seatsString = "";
        var seats = $(".seating-modal[data-incentiv-movie-booking-id='" + movieID + "'] .selected-seats-list li");
        for (var i = 0; i < seats.length; i ++) {
            seatsString += seats.eq(i).text();
            if (i !== seats.length - 1) {
                seatsString += ", ";
            }
        }

        $(".movie-cart-item[data-incentiv-movie-booking-id='" + movieID + "'] .seats-selected span").text(seatsString);
        saved_seats.Seats = finished_seats;

        this.cancelSeatingModal();

        //xxxx
        return_seats = that.sendSeats(booking.MovieBookingID, saved_seats, booking.MovieID);
    },




    /* **********************************
        Data functions
    ********************************** */
    getCinemas: function(mid){
        return $.getJSON('/wp-admin/admin-ajax.php', {
            action: 'get_nu_metro_cinemas',
            mid: mid
        });
    },

    getCinemaSessions: function(cid, mid){
        return $.getJSON('/wp-admin/admin-ajax.php', {
            action: 'get_nu_metro_cinema_sessions',
            cid: cid,
            mid: mid
        });
    },

    getTicketTypes: function(sid){
        return $.getJSON('/wp-admin/admin-ajax.php', {
            action: 'get_nu_metro_ticket_types',
            sid: sid
        });
    },

    resetMovie: function (moviebookingid) {
        return $.getJSON('/wp-admin/admin-ajax.php', {
            action: 'reset_movie_booking_expiry',
            mbid: moviebookingid
        });
    },

    /**
     *
     * @param mbid string Movie Booking ID NOT The Movie Cart ID or the Movie ID
     * @param selected_seats object
     * @param movieID string The Movie ID
     * @returns {*}
     */
    sendSeats: function (mbid, selected_seats, movieID) {
        var that = this,
            index = that.findMovieIndex(movieID),
            json_seats = JSON.stringify(selected_seats); //this is now an object containing an array

        that.showPreloader(movieID, "price");

        return $.ajax('/wp-admin/admin-ajax.php', {
            type: 'POST',
            data: {
                action: 'set_nu_metro_movie_booking_seats',
                mbid: mbid,
                seats: json_seats
            },
            dataType: 'json'
        }).done(function (data) { //need to show that their booking has been saved
            that.MoviesList[index].SavedSeats = data;

            $('#cart-item-code-' + movieID + ' .price span strong').html('R' + parseFloat(that.MoviesList[index].Booking.Amount));
            that.showHideCartItemDetails(movieID);
            that.hidePreloader(movieID);

            if (DEBUG) console.log("Saved Seats: ", data);

        }).fail(function (error) {
            if (DEBUG) console.log("Seating Save Error: ", error);
            modal({
                "successButton": "Try Again",
                "failButton": "Cancel",
                "title": "Unable to save seats",
                "content": "An error has occurred and we were unable to save your seats. If the problem persists please refresh your browser or contact our call centre on <strong>0860 732 5489</strong>.",
                onSuccess: function () {
                    that.sendSeats(mbid, selected_seats, movieID);
                },
                onFail: function () {
                    $(".seating-modal[data-incentiv-movie-booking-id='" + movieID + "'] .seats-selected span").text("--");
                }
            });
        });
    },

    saveBooking: function(){
        var knownBooking = this.getBooking(),
            that = this;
        if(knownBooking){
            return $.Deferred().resolve(knownBooking);
        }

        return $.ajax('/wp-admin/admin-ajax.php', {
            type: 'POST',
            data: { action: 'create_nu_metro_booking' },
            dataType: 'json'
        }).done(function(b){
            that.MovieCartID = b.BookingID;
            //$.cookie('reality-nm-booking', b.BookingID, { path: '/' });
        }).fail(function () {
            if(DEBUG) console.log("Args: ", arguments);

        });
    },

    getBooking: function() {
        if(this.MovieCartID !== null) {
            return this.MovieCartID;
        }
        //return $.cookie('reality-nm-booking');
    },

    getBookingInfo: function (BookingCartID) {
        if(null !== BookingCartID) {
            return $.ajax('/wp-admin/admin-ajax.php', {
                type: 'POST',
                data: { 
                    action: 'get_nu_metro_booking', 
                    booking_id: BookingCartID
                },
                dataType: 'json' 
            }).done(function (data) {
                if(data.TotalPayable && parseFloat(data.TotalPayable) > 0) {
                    that.cartTotal = parseFloat(data.TotalPayable);
                }
            }).fail(function () {

            });
        }
    },

    /**
     * @param booking object
     * @param movieID string
     * @param numTickets int
     * @param cinemaID string
     * @param sessionID string
     */
    addMovieToBooking: function(booking, movieID, numTickets, cinemaID, sessionID) {
        return $.ajax('/wp-admin/admin-ajax.php', {
            type: 'POST',
            data: {
                action: 'add_nu_metro_movie_to_booking',
                bid: booking,
                mid: movieID,
                cid: cinemaID,
                sid: sessionID,
                nt: numTickets
            },
            dataType: 'json'
        });
    },

    removeMovieFromBooking: function(booking, movieBooking){
        return $.ajax('/wp-admin/admin-ajax.php', {
            type: 'POST',
            data: {
                action: 'delete_nu_metro_movie_from_booking',
                bid: booking,
                mbid: movieBooking
            },
            dataType: 'json'
        });
    },

    getNuMetroDetails: function (hopk) {
        return $.getJSON('http://api.numetro.co.za/0.2/movie?hopk=' + hopk);
    }
};


// -----------------------------------------//
//     Block sliders                        //
//------------------------------------------//

var blockSliders = {

    sliders: [],

    init: function () {
        var that = this;
        $('.block-slider .swiper-container').each(function (i) {
            $(this).attr("data-sliderid", i).attr("id", "slider-" + i);

            var slider = $(this).swiper({
                calculateHeight: true,
                offsetPxAfter: 90,
                speed: 400,
                pagination: ($(this).find(".swiper-slide").length > 1) ? $(this).parent().find(".pagination")[0] : false,
                paginationAsRange: true,
                paginationClickable: true,
                slidesPerView: (Modernizr.mq('(max-width: 640px)')) ? 1 : 'auto',
                onTouchStart: function (swiper) {
                    $(swiper.container).find('.force-invis').removeClass('force-invis');
                },
                onSlideChangeStart: function (swiper) {
                    $(swiper.container).find('.force-invis').removeClass('force-invis');
                    $(swiper.container).find('.swiper-slide-visible').eq(3).addClass('force-invis');
                },
                resizeReInit: false

            });

            $(this).parent().find('.force-invis').removeClass('force-invis');
            $(this).parent().find('.swiper-slide-visible').eq(3).addClass('force-invis');

            that.sliders.push(slider);

            that.hideShowArrows();
        });

        this.initEvents();
    },

    initEvents: function () {
        var that = this;

        $(".block-slider .slider-fade.l").click(function () {
            var slider = that.sliders[$(this).parent().find('.swiper-container').attr('data-sliderid')];
            slider.swipePrev();
            $(this).parent().find('.force-invis').removeClass('force-invis');
            $(this).parent().find('.swiper-slide-visible').eq(3).addClass('force-invis');
        });

        $(".block-slider .slider-fade.r").click(function () {
            var slider = that.sliders[$(this).parent().find('.swiper-container').attr('data-sliderid')];
            slider.swipeNext();
            $(this).parent().find('.force-invis').removeClass('force-invis');
            $(this).parent().find('.swiper-slide-visible').eq(3).addClass('force-invis');
        })
    },

    recreateAll: function () {
        var that = this;
        $('.block-slider').each(function (i) {
            var inner = $(this).find(".swiper-wrapper").children(".swiper-slide").attr("class", "swiper-slide").attr("style", "").clone(true);
            that.sliders[i].destroy();
            $(this).find(".swiper-wrapper").empty().append(inner).attr("style", "");
        });
        this.sliders = [];
        this.init();
    },

    hideShowArrows: function () {
        $('.block-slider .swiper-container').each(function (i) {
            var containerWidth = $(this).width() + 30;
            var itemsWidth = $(this).find(".swiper-wrapper").width();

            if (itemsWidth > containerWidth) {
                $(this).parent().find(".slider-fade").show();
            } else {
                $(this).parent().find(".slider-fade").hide();
            }
        });
    }
};

// -----------------------------------------//
//     ACTION PANEL                         //
//------------------------------------------//

var ActionPanel = {

    settings: {
        topPos: 0,
        headerH: 330,
        isFixed: false,
        inFooter: false,
        isOpen: false
    },

    actionPanelOpen: false,

    init: function () {
        s = this.settings;
        this.bindUIActions();
    },

    bindUIActions: function () {
        var that = this;
        
        $('#action-panel #control').click(function () {
            if (that.actionPanelOpen == false) {
                ActionPanel.openIt();
            } else {
                ActionPanel.closeIt();
            }
            return false;
        });
    },

    checkPosition: function () {
        s = this.settings;

        // on DESKTOP
        if (Modernizr.mq('(min-width: ' + tablet + 'px)')) {
            if ($('#banner').length) {
                s.headerH = $('#banner').offset().top + $('#banner').height();
            }

            if (winTop > s.headerH) {
                if (s.isFixed === false) ActionPanel.fixPosition();
                ActionPanel.updateLeftPosition();

            } else if (winTop < s.headerH) {
                ActionPanel.unfixPosition();
            }

            this.pushFooter();

        } else if (Modernizr.mq('(max-width: ' + tablet + 'px)')) {

            if (winTop > s.headerH) {
                ActionPanel.tabletFixPosition();

            } else if (winTop < s.headerH) {
                ActionPanel.tabletUnfixPosition();
            }

            $("#action-panel-wrapper input").blur();
            
        }
    },

    tabletFixPosition: function () {
        s.isFixed  = true;
        s.inFooter = false;

        if (s.isOpen) {
            $('#action-panel').css({
                position: 'fixed',
                top: '0px',
                left: 'auto'
            });
        } else {
            $('#action-panel').css({
                position: 'fixed',
                top: '0px',
                right: '-275px',
                left: 'auto'
            });
            this.actionPanelOpen = false;
        }

        
    },

    tabletUnfixPosition: function () {
        s.isFixed  = false;
        s.inFooter = false;

        if (s.isOpen) {
            $('#action-panel').css({
                position: 'absolute',
                top: s.topPos + 'px',
                left: 'auto'
            });
        } else {
            $('#action-panel').css({
                position: 'absolute',
                top: s.topPos + 'px',
                right: '-275px',
                left: 'auto'
            });
            this.actionPanelOpen = false;
        }
    },

    unfixPosition: function () {
        s.isFixed = false;

        $('#action-panel').css({
            position: 'absolute',
            top: s.topPos + 'px',
            right: '0px',
            left: 'auto'
        });
    },

    fixPosition: function () {
        s.isFixed = true;

        $('#action-panel').css({
            position: 'fixed',
            top: '0px',
            left: (
            ($(window).width() - $('#action-panel-wrapper').width())/2 +
            ($('#action-panel-wrapper').width() - $('#action-panel').width()) + 'px'
            )
        });
    },

    updateLeftPosition: function () {
        $('#action-panel').css({
            left: (
            ($(window).width() - $('#action-panel-wrapper').width())/2 +
            ($('#action-panel-wrapper').width() - $('#action-panel').width()) + 'px'
            )
        });
    },


    /* Stop the action panel from over lapping the footer by
     pushing the footer down or the action panel up.
     */
    pushFooter: function () {
        var footerOffset = $(".footer-container").offset().top - parseInt($(".footer-container").css("margin-top").replace("px", ""));
        var actionPanelHeight = $("#action-panel").outerHeight();
        var actionPanelBottom = $("#action-panel").offset().top + actionPanelHeight;
        var isOverlapping = (actionPanelBottom - footerOffset < -40) ? false : true;


        if ( !isOverlapping && s.isPushed !== true) {
            $(".footer-container").css({"margin-top": "0px"});
            $('#action-panel').css({
                top:  '0px'
            });
            return;
        }

        if ( s.isFixed === false ) {
            $(".footer-container").css({
                "margin-top": actionPanelBottom - footerOffset + 40 + "px"
            })

        } else if ( (s.isFixed === true && isOverlapping ) || s.isPushed ) {
            var fixedFooterOffset = $(".footer-container").offset().top - $(window).scrollTop();
            $('#action-panel').css({
                top:  (actionPanelHeight - fixedFooterOffset + 40) * -1 + 'px'
            });

            s.isPushed = true;
        }

        if ( s.isPushed && parseInt($("#action-panel").css("top").replace("px", "")) > -1 ) {
            s.isPushed = false;
        }
    },

    openIt: function () {
        $('#action-panel').animate({right: '0px'}, 300);
        this.actionPanelOpen = true;
        s.isOpen = true;
    },

    closeIt: function () {
        $('#action-panel').animate({right:'-275px'}, 300);
        this.actionPanelOpen = false;
    }
}; // end of Action Panel



// -----------------------------------------//
//    Discount blocks height                //
//------------------------------------------//

function resizeDiscountBlocks() {
    var blocks = $(".discount-block-inner");

    if ($(window).width() > 1024){
        blocks.css({"height": ""});
        var height = blocks.height();
        blocks.height(height);
    } else {
        blocks.css({"height": ""});
    }
}

function resizeMembershipBlocks() {
    var blocks = $(".membership-block-inner");
    
    if ($(window).width() > 1024){
        blocks.css({"height": ""});
        var height = blocks.height();
        blocks.height(height);
    } else {
        blocks.css({"height": ""});
    }
}



// -----------------------------------------//
//    Modal popup                           //
//------------------------------------------//

var modal = function (options) {
    var modal = {
            settings: {
            title: "Modal Title",
            content: "",
            successButton: null,
            failButton: null,
            onSuccess: function () {},
            onFail: function () {}
        },

        modalWindow: null,

        init: function () {
            //Create and append modal
            this.modalWindow = this.create();
            this.modalWindow = this.modalWindow.appendTo("body");

            //Append overlay
            this.createOverlay();

            //Show modal
            this.showModal();
        },

        create: function () {
            var modalWindow = $("<div class='modal-window' id='modal-window' />");
            modalWindow.append("<h3>" + this.settings.title + "</h3>");

            if (this.settings.content.indexOf("<div") !== 0) {
                modalWindow.append("<p>" + this.settings.content + "</p>");
            } else {
                modalWindow.append(this.settings.content);
            }

            if (this.settings.successButton === null) return modalWindow;

            if (this.settings.failButton) {
                var failButton = $("<a class='button'>" + this.settings.failButton + "</a>");
                failButton.on("click", this.fail);
                modalWindow.append(failButton);
            }

            var successButton = $("<a class='button'>" + this.settings.successButton + "</a>");
            successButton.on("click", this.success);
            modalWindow.append(successButton);

            return modalWindow;
        },

        createOverlay: function () {
            $("<div id='modal-window-overlay' class='modal-window-overlay' />").appendTo("body")
                .on("click", this.removeModal).fadeIn(400).on("click", this.fail);
        },

        showModal: function () {
            var modalWindow = this.modalWindow || $(this).closest("#modal-window");
            modalWindow.addClass("show");

            if ($("#modal-window-overlay").length < 1) 
                this.createOverlay();

            $("#modal-window-overlay").fadeIn();
            
            return modalWindow;
        },

        removeModal: function () {
            var modalWindow = this.modalWindow || $("#modal-window");
            modalWindow.removeClass("show");
            $("#modal-window-overlay").fadeOut(300, function () {
                $("#modal-window").remove();
                $("#modal-window-overlay").remove();
            });
        },

        success: function () {
            var target = $("#modal-window");
            target.data("settings" ).onSuccess.call();
            target.removeModal();
        },

        fail: function () {
            var target = $("#modal-window");
            target.data("settings" ).onFail.call();
            target.removeModal();
        }

    }

    _.extend(modal.settings, options);

    //build and show
    modal.init();

    modal.modalWindow.data("settings", modal.settings);

    //Create jquery methods
    $.fn.showModal = modal.showModal;
    $.fn.removeModal = modal.removeModal;


    // Return jquery object. Can call functions on it
    return modal.modalWindow;
}


// -----------------------------------------//
//    Tables controller                     //
//------------------------------------------//

var tableController = {
    
    init: function () {
        var that = this;

        this.control = $('.table-control');
        this.table = this.control.parent().find('.tablepress');

        if(this.table.length < 1) return;

        $(".table-control input").change(function () {
            var column = parseInt($('.table-control input:checked').val());
            that.showColumn(column + 1);
        });

        this.showColumn(2);
        this.control.find("input").eq(0).prop("checked", true)
    },

    showColumn: function (column) {
        $('.table-mobile-show').removeClass("table-mobile-show");
        this.table.find("td:nth-child(" + column + ")").addClass("table-mobile-show");
        this.table.find("th:nth-child(" + column + ")").addClass("table-mobile-show");
    }
};

var lifestyle = $(".membershiptable td:nth-child(4)"),
    standard = $(".membershiptable td:nth-child(3)"),
    plus = $(".membershiptable td:nth-child(2)");


window.onbeforeunload = function () {
    var bookingID = moviesCart.MovieCartID;
    if(bookingID) {
        $.ajax('/wp-admin/admin-ajax.php', {
            type: 'POST',
            data: {
                action: 'ajax_cancel_nu_metro_booking',
                bid: bookingID
            },
            dataType: 'json'
        }).done(function (data) {
            return true;
        }).fail(function (error){
            return true;
        });
    }
};
