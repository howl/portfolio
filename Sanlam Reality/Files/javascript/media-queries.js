// -----------------------------------------//
//     Media Queries                        //
//------------------------------------------//


//-----------------------------------------
// Return the break point above current size
//-----------------------------------------
var getCurrentBreakpoint = function () {
    if (Modernizr.mq('(max-width: 480px)')) {
        return 480;

    } else if (Modernizr.mq('(max-width: 640px)')) {
        return 640;

    } else if (Modernizr.mq('(max-width: 960px)')) {
        return 960;

    } else if (Modernizr.mq('(max-width: 1024px)')) {
        return 1024;

    } else if (Modernizr.mq('(max-width: 1150px)')) {
        return 1150;

    } else if (Modernizr.mq('(max-width: 1280px)')) {
        return 1280;

    } else if (Modernizr.mq('(max-width: 1440px)')) {
        return 1440;

    } else if (Modernizr.mq('(max-width: 1920px)')) {
        return 1920;

    }
}


//-----------------------------------------
// Trigger events at break points
//-----------------------------------------
var platformSize = null;
var mediaQuerySize = '';
var mediaQueriesEveryFrame = function (isInit) {
    var oldSize = mediaQuerySize;
    var oldPlatformSize = platformSize;

    if (Modernizr.mq('(max-width: 480px)')) {
        platformSize = "mobile";
        mediaQuerySize = '480px';

    } else if (Modernizr.mq('(max-width: 640px)')) {
        platformSize = "mobile";
        mediaQuerySize = '640px';

    } else if (Modernizr.mq('(max-width: 960px)')) {
        platformSize = "tablet";
        mediaQuerySize = '960px';

    } else if (Modernizr.mq('(max-width: 1024px)')) {
        platformSize = "desktop";
        mediaQuerySize = '1024px';

    } else if (Modernizr.mq('(max-width: 1150px)')) {
        platformSize = "desktop";
        mediaQuerySize = '1150px';

    } else if (Modernizr.mq('(max-width: 1280px)')) {
        platformSize = "desktop";
        mediaQuerySize = '1280px';

    } else if (Modernizr.mq('(max-width: 1440px)')) {
        platformSize = "desktop";
        mediaQuerySize = '1440px';

    } else if (Modernizr.mq('(max-width: 1920px)')) {
        platformSize = "desktop";
        mediaQuerySize = '1920px';
    }

    if (isInit === true) return;

    if ( platformSize !== oldPlatformSize ) {
        $(window).trigger(platformSize);
    }

    //Breakpoint reached
    if (oldSize !== mediaQuerySize) {
        $(window).trigger(mediaQuerySize);
        blockSliders.recreateAll();
    }
}

mediaQueriesEveryFrame(true);