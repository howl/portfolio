module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            options: {
                separator: ' ',
            },
            dist: {
                src: ['css/normalize.css', 'css/foundation.css', 'css/idangerous.swiper.css', 'css/forms.css', 'css/general.css', 'css/*.css', '!css/main.css'],
                dest: 'css/main.css',
            },
        },

        patternprimer: {
            my_target: {
                wwwroot: './',
                src: 'fragments/',
                files: ['nav.html', 'action-panel.html', 'html-structure.html','fonts.html','discounts-block.html', 'matrix.html', 'featured-slider.html', 'secondary-slider.html', 'single-benefit-matrix.html','article-blocks.html', 'article-slider.html', 'promotion-article.html', 'accordian.html', 'forms.html', 'table.html', 'opening-drawer.html', 'footer.html'],
                css: ['css/main.css'],
                dest: 'export',
                snapshot: true,
                index: "pattern-index.html"
            }
        },

        copy: {
            main: {
                files: [
                    {expand: true, src: ['js/**'], dest: 'export/'},
                    {expand: true, src: ['img/**'], dest: 'export/'},
                ],
            },
        },

        cssmin: {
          target: {
            files: [{
              expand: true,
              src: ['export/css/main.css'],
              dest: './',
              ext: '.min.css'
            }]
          }
        },

        imageEmbed: {
            dist: {
                src: [ "css/main.css" ],
                dest: "css/main.css",
                options: {
                    deleteAfterEncoding : false,
                }
            }
        },

        watch: {
            img: {
                files: ['img/*'],
                tasks: ['copy', 'concat', 'imageEmbed', 'patternprimer'],
                options: {
                  spawn: true,
                },
            },

            csscombine: {
                files: ['css/*.css', "!css/main.css"],
                tasks: ['concat', 'imageEmbed', 'cssmin', 'patternprimer'],
                options: {
                  spawn: true,
                },
            },

            javascript: {
                files: ['js/*.js'],
                tasks: ['copy'],
                options: {
                  spawn: true,
                },
            },

            html: {
                files: ['fragments/*.html'],
                tasks: ['patternprimer'],
                options: {
                  spawn: true,
                },
            }
        },

    });

    grunt.loadNpmTasks('grunt-patternprimer');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-image-embed');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('default', ['concat', 'imageEmbed', 'copy', 'cssmin', 'patternprimer']);
    grunt.registerTask('wait', ['watch']);

};